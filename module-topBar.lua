local nb = display.newGroup()


nb.new = function(group, sceneName)

    local group = display.newGroup()

    local topBarAssets = _G.IMAGES.topBar
    
    local background = display.loadImage(group, topBarAssets.background) --display.newImageRect(sceneGroup, "images/photos/background.png", 320,570)
    background.x, background.y = CENTER_X, background.contentHeight*.5
    group.background = background

    local btMenu = require("library-widgets").newTapButton{
        parent = group,
        left = 0,
        y = 201/3,
        width = topBarAssets.menu.width*2,
        height = topBarAssets.menu.height*3,
        fillColor = {0,0,0,0},
        imageObj = topBarAssets.menu,
        imageOverFillColor = {1,1,1,.3},
        onTap =  function() print("tapp"); _G.MENU.show() end
    }


    if string.lower(sceneName) == "outros artistas" then


        local lbSceneName = display.newText{parent=group, text=sceneName, font=APP_CONFIG.fontLight, fontSize=24, x = CENTER_X, y = btMenu.y + 1}

        local lbSceneSubTitle = display.newText{parent=group, text="Baixe outros aplicativos de nossos artistas", font=APP_CONFIG.fontLight, fontSize=12, x = CENTER_X, y = lbSceneName.y + lbSceneName.contentHeight*.5 + 2}        
        lbSceneSubTitle.anchorY = 0

        return group

    end


    local photoRadius = 169/3/2
    local photo = display.newCircle(group, btMenu.x + btMenu.contentWidth*.5 + photoRadius   + 10, btMenu.y - 5,  photoRadius )
    photo.fill = {.5,.5,.5 }
    --photo.fill = { type="image", filename = topBarAssets.foto.path}
    if PROFILE.image and PROFILE.image.url then
        local profilePhotoFilename = AUX.getFilenameFromURL(PROFILE.image.url)
        local imageExist = display.newImage(group, profilePhotoFilename, system.TemporaryDirectory, -500, -500 )
        print("imageExist=", imageExist)
        if imageExist then
            display.remove( imageExist )
            photo.fill = { type="image", filename = profilePhotoFilename, baseDir=system.TemporaryDirectory}    
        else
            print("image not here, let's download it")
            SERVER.downloadImage(PROFILE.image.url, function(result)
                print("on call abacl - photo=", photo, result)
                    if result and photo then
                        photo.fill = { type="image", filename = profilePhotoFilename, baseDir=system.TemporaryDirectory}    
                        if PROFILE.image.width > PROFILE.image.height then        
                            photo.fill.scaleX = PROFILE.image.width / PROFILE.image.height
                        else
                            photo.fill.scaleY = PROFILE.image.height / PROFILE.image.width
                        end
                    end
                end)
        end
        
    end
    

    group.photo = photo
    if PROFILE.image then
        if PROFILE.image.width > PROFILE.image.height then        
            photo.fill.scaleX = PROFILE.image.width / PROFILE.image.height
        else
            photo.fill.scaleY = PROFILE.image.height / PROFILE.image.width
        end
    end
    -- local photo = display.loadImage(group, topBarAssets.photo)
    -- photo.x = btMenu.x + btMenu.contentWidth*.5 + photo.contentWidth*.5 + 10
    -- photo.y = btMenu.y - 5
    -- group.photo = photo

    local photoBackground = display.newCircle( group, photo.x, photo.y + 2, photo.contentHeight*.5 + 2)
    photoBackground:setFillColor( 101/255,98/255,101/255 )
    group.photoBackground = photoBackground
    group:insert(photo) -- bringing photo to front


    local lbArtistName = display.newText{parent=group, text=APP_CONFIG.appName, font=APP_CONFIG.fontLight, fontSize=14}
    lbArtistName.y = photo.y - photo.contentHeight*.25 + 2
    lbArtistName.anchorX = 0
    lbArtistName.x = photoBackground.x + photoBackground.contentWidth*.5 + 10

    local lbSceneName = display.newText{parent=group, text=sceneName, font=APP_CONFIG.fontLight, fontSize=24, x = lbArtistName.x, y = photo.y - 2}
    lbSceneName.anchorX = 0
    lbSceneName.anchorY = 0



    if sceneName == "Perfil" then
        local newPhotoBackgroundW = 440/(1080/320)
        local newPhotoW = 420/(1080/320)
        photoBackground:scale(newPhotoBackgroundW/photoBackground.contentWidth, newPhotoBackgroundW/photoBackground.contentWidth)
        photo:scale(newPhotoW/photo.contentWidth,newPhotoW/photo.contentWidth)
        photoBackground.y = photoBackground.y + 73
        photo.y = photo.y + 70
        photo.x = photo.x - 14
        photoBackground.x = photo.x


        lbArtistName.size = 20
        lbArtistName.y = photo.y - photo.contentHeight*.25
        lbArtistName.x = photo.x + photo.contentWidth*.5 + 10

        lbSceneName.size = 12
        lbSceneName.x = lbArtistName.x
        lbSceneName.y = lbArtistName.y + lbArtistName.contentHeight*.5
        group.lbSceneName = lbSceneName

    end



    return group  -- uses top/left anchor

end


-- nb.new = function(options)


--     -- receiving params
--     local titleText = options.title or "Editar Dados"
--     local leftButtonHandler = options.leftButtonHandler
--     local parent = options.parent
--     local isHidden = options.isHidden or false


--     local navBar = display.newGroup()


--     local background = display.newRect(navBar, SCREEN_W*.5, 44*.5,SCREEN_W,44 )
--     background:setFillColor( 124/255,171/255,153/255 )


--     local title = display.newText{parent=navBar, text=titleText, font=myGlobals.fontTextLabel,fontSize=18, width=background.width*.8, align="center"}
--     title.x,title.y = background.x, background.y

    
--     local btLeftW = background.width*0.15
--     local b = require("custom-widget").newTapButton{
--         --label = "Test",
--         labelPadding = 0,
--         align="left",
--         --shape="rect",
--         width = btLeftW,
--         height = background.contentHeight,
--         fillColor={1, 0.2, 0.5, 0},
--         y = background.y,
--         x = 0,
--         imageFilename = "images/icon_back.png",
--         imageWidth = 24,
--         imageHeight = 19,
--         imageLeft = 8,
--         imageOverFillColor = {0,0,0},
--         onTap = function(e) if leftButtonHandler then leftButtonHandler(e) end end ,
--     }
--     b.x = b.contentWidth*.5
--     navBar:insert(b)

--     -- hiding the bar
--     if isHidden then
--         navBar.y = -navBar.contentHeight  --_G.TOP_Y_AFTER_STATUS_BAR
--     else
--         navBar.y = _G.TOP_Y_AFTER_STATUS_BAR
--     end
    

--     if parent then
--         parent:insert(navBar)
--     end

--     navBar.hide = function(animated)

--         if navBar.y == (-navBar.contentHeight) then
--             return 
--         end

--         animated = animated or false
        
--         local duration = 200
--         if animated == false then
--             duration = 0
--         end
--         transition.to(navBar, {y=-navBar.contentHeight, time=duration}) 


--     end

--     navBar.show = function(animated)

--         if navBar.y == _G.TOP_Y_AFTER_STATUS_BAR then
--             return 
--         end

--         animated = animated or false
        
--         local duration = 200
--         if animated == false then
--             duration = 0
--         end
--         transition.to(navBar, {y=_G.TOP_Y_AFTER_STATUS_BAR, time=duration})    

--     end

--     navBar.setTitle = function(newTitle)
--         title.text = newTitle

--     end


--     return navBar


-- end



return nb
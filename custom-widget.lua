local t = {}


t.newShareButton = function(options)
    
    -- receiving params
    local parent = options.parent
    local x = options.x
    local y = options.y
    local top = options.top
    local left = options.left
    local right = options.right

    local shareImageFilenameOrFunction = options.shareImageFilename   -- image filename
    local shareMessage = options.shareMessage
    local shareURL = options.shareURL

    local useBlackColor = options.useBlackColor or false

    local scrollViewObj = options.scrollViewParent

    local labelColor = {1,1,1,1}
    local labelOverColor = {0,162/255,1,.3}
    local imageColor = {1,1,1,1}
    local imageOverColor = {0,162/255,1,.3}

    if useBlackColor then
        labelColor =  {0,0,0,1}
        labelOverColor = {0,162/255,1,.3}
        
        imageColor = {0,0,0,1}
        imageOverColor = {0,162/255,1,.3}
    end


    if shareURL == nil then
        shareURL = APP_CONFIG.artistSite
    end

    local flurry = options.flurry
    
    local function btShareHandler(event)
        
        if type(shareImageFilenameOrFunction) == "function" then
            shareImageFilename = shareImageFilenameOrFunction()
        else
            shareImageFilename = shareImageFilenameOrFunction
        end

        if _G.DEVICE.isApple then

            local itemsToShare = { { type = "string", value = shareMessage }, }
            if url then
                itemsToShare[#itemsToShare+1] = { type = "url", value = shareURL }
            end
            if shareImageFilename then
                itemsToShare[#itemsToShare+1] = { type = "image", value = { filename = shareImageFilename, baseDir = system.TemporaryDirectory, } } 
            end


            local listener = {}
            function listener:popup( event )
                --print( "name(" .. event.name .. ") type(" .. event.type .. ") activity(" .. tostring(event.activity) .. ") action(" .. tostring(event.action) .. ")" )
            end

            -- Show the popup
            native.showPopup( "activity",
            {
                items = itemsToShare,
                -- excludedActivities = { "UIActivityTypeCopyToPasteboard", },
                listener = listener,
            })

        else

            local listener = {}
            function listener:popup( event )
                print( "name(" .. event.name .. ") type(" .. event.type .. ") action(" .. tostring(event.action) .. ") limitReached(" .. tostring(event.limitReached) .. ")" )          
            end

            local options = {}
            options.listener = listener
            options.message = shareMessage
            if shareURL then
                options.url = { shareURL }
            end
            
            if shareImageFilename then
                options.image = {
                    { filename = shareImageFilename, baseDir = system.TemporaryDirectory },
                }                
            end

            -- Show the popup
            native.showPopup( "social", options )

        end    
    end
    
    local btShare = require("library-widgets").newButton{
            right = right,
            top = top,
            y = y,
            width = 90, 
            height=30,
            
            label = "COMPARTILHAR",
            labelColor = labelColor,
            labelOverColor = labelOverColor,
            labelFont = APP_CONFIG["fontMedium"],
            labelFontSize = 8,
            
            imageFile = APP_CONFIG.images.icons.share.path,
            imageWidth = APP_CONFIG.images.icons.share.width,
            imageHeight = APP_CONFIG.images.icons.share.height,
            imagePosition = "right",
            imagePadding = {left=4, bottom=2},

            imageColor = imageColor,
            imageOverColor =imageOverColor,

            backgroundColor = { 1,0,0, 0 }, 
            backgroundOverColor ={ 0,0,0, 0 },

            onEvent = function(event)

                local phase = event.phase
                local target = event.target

                if ( "ended" == phase ) then

                    --btSeeMoreHandler(newsObj) 
                    print(flurry)
                    btShareHandler("buu")

                elseif "moved" == phase then

                     local dy = math.abs( event.y - event.yStart )

                     if dy > 12 then
                        if scrollViewObj then
                            --print("going to give focus to SV")
                            scrollViewObj:takeFocus( event )
                        end
                         
                     end
               end

            end
        }
        if parent then
            parent:insert(btShare)
        end
        
        return btShare

end


-- button that responds to tap. Don't have overlay status
t.newTapButton = function(options)
    local parent = options.parent
    local id = options.id
    local w = options.width or options.w
    local h = options.height or options.h
    local left = options.left --or (options.x - w*0.5)
    local right = options.right
    local x = options.x
    local top = options.top or (options.y - h*0.5)
    local fillColor = options.fillColor or {1,1,1}  -- background color

    local hasDisclosureIndicator = options.hasDisclosureIndicator or false
    local disclosureIndicatorFillColor = options.disclosureIndicatorFillColor or {180/255,180/255,180/255}
    local disclosureIndicatorOverFillColor = options.disclosureIndicatorOverFillColor or {180/255,180/255,180/255}
    
    local text = options.label    
    local textOffsetY = options.labelOffsetY or 0
    local textOffsetX = options.labelOffsetX or 0
    local textColor = options.labelColor or {0,0,0}
    local textOverColor = options.labelOverColor or {.3,.3,.3}
    local textPadding = options.labelPadding or 10  -- internal margin for truncating label purposes
    local align = options.align 
    local subText = options.subLabel -- optional labels that appears on the right side, before the indicator
    local font = options.font or APP_CONFIG.fontLight
    local fontSize = options.fontSize or 14
    local strokeColor = options.strokeColor or {0,0,0}
    local strokeWidth = options.strokeWidth or 0
    
    local icon = options.icon or {}
    local imageFilename = options.imageFilename or icon.filename or icon.path
    local imageWidth = options.imageWidth or icon.width
    local imageHeight = options.imageHeight or icon.height
    local imageTopPos = options.imageTop or icon.top  -- top position relative to the button
    local imageLeftPos = options.imageLeft or icon.left  -- top position relative to the button
    local imageOverFillColor = options.imageOverFillColor or options.iconOverFillColor
    
    local onTap = options.onTap
    

    local button = display.newGroup() -- this has a left, top reference
    button.id = id

    -- background
    local background = display.newRect(button, w*.5, h*.5, w, h)
    background:setFillColor(unpack(fillColor))
    background.isHitTestable = true
    button.background = background
    
    
    background.strokeWidth = strokeWidth
    background:setStrokeColor(unpack(strokeColor))    
    
    
    -- disclosure indicator
    if hasDisclosureIndicator then
        local disclosureIndicator = display.newImageRect(button, "images/icon_indicator.png",5,9)
        disclosureIndicator:setFillColor(unpack(disclosureIndicatorFillColor))
        disclosureIndicator.x = background.x + background.contentWidth*0.5 - disclosureIndicator.contentWidth*0.5 - 5
        disclosureIndicator.y = background.y
        button.disclosureIndicator = disclosureIndicator
    end

    -- text
    local labelX = background.x
    local sublLabelRightX = background.x + background.contentWidth*.5 - 5
    if hasDisclosureIndicator then
        labelX = labelX - 10*.5
    end
    if text then
        
        local textWidth = background.contentWidth - textPadding
        if hasDisclosureIndicator then
            textWidth = textWidth - 20             
            sublLabelRightX = button.disclosureIndicator.x - button.disclosureIndicator.contentWidth*.5 - 15
        end
        
        if subText then
            local subLabel = display.newText{parent=button, text=subText, x=sublLabelRightX, y=background.y + textOffsetY, font=font, fontSize=fontSize}            
            subLabel.x = sublLabelRightX - subLabel.contentWidth*.5
            subLabel:setFillColor( unpack(textColor) )
            button.subLabel = subLabel
        end

        local labelWidth = nil
        if align then
            labelWidth = textWidth
        end
        labelX = labelX + textOffsetX
        local label = display.newText{parent=button, text=text, x=labelX, y=background.y + textOffsetY, font=font, fontSize=fontSize, align = align, width=labelWidth}
        label:setFillColor( unpack(textColor) )
        button.label = label

        
        
        _G.AUX.adjustTextSize(label,textWidth)
        background.label = label
    end
    
    -- image
    if imageFilename then
        local image = display.newImageRect(button, imageFilename,imageWidth, imageHeight)
        image.x = background.x
        image.y = background.y
        if imageTopPos then
            image.y = imageTopPos + image.contentHeight*.5
        end
        if imageLeftPos then
            image.x= imageLeftPos + image.contentWidth*.5
        end
        button.image = image
    end

    local function onButtonTap(e)
        --print("sdkf")
        if e.numTaps ~= 1 then return end

        if imageOverFillColor and button.image then
            button.image:setFillColor(unpack(imageOverFillColor))
        end  
        
        if textOverColor then
            if button.label then
                button.label:setFillColor(unpack(textOverColor))
            end
            
            if button.subLabel then
                button.subLabel:setFillColor(unpack(textOverColor))
            end
        end  

        if disclosureIndicatorOverFillColor and button.disclosureIndicator then
            button.disclosureIndicator:setFillColor(unpack(disclosureIndicatorOverFillColor))
        end

        timer.performWithDelay( 50, function()
            if imageOverFillColor and button.image then
                button.image:setFillColor(1,1,1)
            end
            if button.label then
                button.label:setFillColor(unpack(textColor))
            end
            
            if button.subLabel then
                button.subLabel:setFillColor(unpack(textColor))
            end

            if disclosureIndicatorOverFillColor and button.disclosureIndicator then
                button.disclosureIndicator:setFillColor(unpack(disclosureIndicatorFillColor))
            end

            onTap(e)
        end
        )
        

    end
    
    if onTap then
        button:addEventListener("tap", onButtonTap)
    end

    button.anchorChildren = true
    
    -- sets the button position
    button.x = x or (left and (left + w*.5)) or (right and (right - w*.5))
    button.y = y or (top + h*.5)

    if parent then
        parent:insert(button)
    end
    
    return button

end

-- normal widget button already configured to app colors, font
t.newButton = function(label, x, y, width, height, onRelease, parent)
    

    
    local button = require("widget").newButton{
        x = x,
        y = y,
        width=width, height=height,
        label = label,
        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
        shape="rect",
        fillColor = { default={ 195/255, 99/255, 70/255, 1 }, over={ 195/255, 99/255, 70/255, 0.6 } },
        font="GillSans",
        onRelease=onRelease
    }
    if parent then
        parent:insert(button)
        
    end
    
    return button
    
end


-- collection of buttons where just 1 can be selected
t.newSegmentedControl = function(options)
    
    local groupSegmentControl = display.newGroup()
    
    local x = options.x
    local y = options.y
    local left = options.left
    local top  = options.top
    local segmentWidth = options.segmentWidth
    local segmentHeight = options.segmentHeight
    local segmentBackgroundColor = options.segmentBackgroundColor
    local segmentStrokeColor = options.segmentStrokeColor or {0,0,0,0}
    local segmentFontColor = options.segmentFontColor or {1,1,1}
    local segmentPressedBackgroundColor = options.segmentPressedBackgroundColor
    local segmentPressedStrokeColor = options.segmentPressedStrokeColor
    local segmentPressedFontColor = options.segmentPressedFontColor or {0,0,0}
    local segmentLabelFontSize = options.segmentLabelFontSize
    local segmentLabelOffsetY = options.segmentLabelOffsetY
    local segmentsLabel = options.segmentsLabel 
    local segments = options.segments or segmentsLabel  -- you can use segments as texts or images. To use a combo (image + text), use semgmentsLabel (for texts) and segments (to the images)
    local defaultSegment = options.defaultSegment
    local onPress = options.onPress
    local allowDeSelect = options.allowDeSelect or false
    local allowMultiSelect = options.allowMultiSelect or false
    local spacing = options.spacing or 1  -- space between buttons
    
    groupSegmentControl.segment = {}
    groupSegmentControl.segmentSelectedID = defaultSegment
    groupSegmentControl.totalSegmentsIDSelected = {}
    local function onSegmentTap(e)
        
        -- if tapped someone already selected, let's deselect if it is allowed
        local wasDeSelected = false
        if e.target.isSelected and allowDeSelect then
            groupSegmentControl.segmentSelectedID = nil
            e.target.isSelected = false
            --e.target.background:setFillColor(0,0,0,.1)
            e.target.background:setFillColor(unpack(segmentBackgroundColor))
            e.target.background:setStrokeColor(unpack(segmentStrokeColor))
            e.target.label:setTextColor(unpack(segmentFontColor))
            wasDeSelected = true
            
            local index = table.indexOf( groupSegmentControl.totalSegmentsIDSelected, e.target.id )
            table.remove(groupSegmentControl.totalSegmentsIDSelected, index)
            
        else
            
            
            if allowMultiSelect == false then
                -- let's deselect the last selected if exists
                if groupSegmentControl.segmentSelectedID then
                    groupSegmentControl.segment[groupSegmentControl.segmentSelectedID].background:setFillColor(unpack(segmentBackgroundColor))
                    groupSegmentControl.segment[groupSegmentControl.segmentSelectedID].background:setStrokeColor(unpack(segmentStrokeColor))
                    groupSegmentControl.segment[groupSegmentControl.segmentSelectedID].label:setTextColor(unpack(segmentFontColor))
                    groupSegmentControl.segment[groupSegmentControl.segmentSelectedID].isSelected = false
                end
            end
            
            -- select the new one
            --e.target.background:setFillColor(195/255, 99/255, 70/255, 1)       
            e.target.background:setFillColor(unpack(segmentPressedBackgroundColor))
            e.target.background:setStrokeColor(unpack(segmentPressedStrokeColor))
            e.target.label:setTextColor(unpack(segmentPressedFontColor))
            e.target.isSelected = true
            groupSegmentControl.segmentSelectedID = e.target.id
            table.insert(groupSegmentControl.totalSegmentsIDSelected, e.target.id)
 
            
        end
        
        if onPress then
            onPress({target={segmentLabel=e.target.label.text, segmentNumber=e.target.id, wasDeSelected = wasDeSelected, totalSegmentsNumberSelected = groupSegmentControl.totalSegmentsIDSelected}})
        end
        
    end
    
    
    
    for i=1,#segments do
        local label = segments[i]
        local icon = nil
        
        if type(segments[i]) == "table" then
            label = (segmentsLabel and segmentsLabel[i]) or ""
            icon = segments[i]
        end
        
        -- creates the buttons as unselected state
        local button = t.newTapButton{
            id = i,
            x = segmentWidth*.5 + (i-1)*(segmentWidth + spacing) , y = segmentHeight*.5,
            width=segmentWidth, height=segmentHeight,
            label= label,
            labelOffsetY = segmentLabelOffsetY,
            fillColor = segmentBackgroundColor,
            labelColor = segmentFontColor,
            fontSize = segmentLabelFontSize,
            font=APP_CONFIG.fontMedium,
            strokeWidth=1,
            strokeColor=segmentStrokeColor, 
            icon = icon,   -- table as {filename=, width=, height=}
            onTap = onSegmentTap
        }

        -- highlight the selected button
        if i == defaultSegment then
            button.background:setFillColor(unpack(segmentPressedBackgroundColor))
            button.background:setStrokeColor(unpack(segmentPressedStrokeColor))
            button.label:setTextColor(unpack(segmentPressedFontColor))
            button.isSelected = true
            table.insert(groupSegmentControl.totalSegmentsIDSelected, i)
        end
        groupSegmentControl:insert(button)
        groupSegmentControl.segment[i] = button
        
        
    end



    groupSegmentControl.selectSegmentNumber = function(segmentNumber)
        groupSegmentControl.segment[segmentNumber]:dispatchEvent(  { name="tap", numTaps=1, target=groupSegmentControl.segment[segmentNumber] } )
    end
    
    -- sets the position
    left = left or (x - groupSegmentControl.contentWidth*.5)
    top = top or (y - groupSegmentControl.contentHeight*.5)
    groupSegmentControl.x = left
    groupSegmentControl.y = top
    
    
    return groupSegmentControl
end





t.newLyricButton = function(options)
    
    -- receiving options
    local id = options.id
    local x = options.x
    local y = options.y
    local left = options.left
    local right = options.right
    local top = options.top
    local bottom = options.bottom
    local onRelease = options.onRelease
    local onEvent = options.onEvent


        
    local height = options.height or 16
    local widthTotal = options.width or 46 -- total width, including the triangle arrow
    local widthArrow = 3
    local heightArrow = 6


    local hW = widthTotal*.5  -- half width
    local hH = height*.5      -- half height
    local hAh = heightArrow*.5

    local vertices = {
        -hW, 0,
        -hW + widthArrow, -hAh,
        -hW + widthArrow, -hH,
        hW, -hH,

        hW, hH,
        -hW + widthArrow, hH,
        -hW + widthArrow, hAh,

    }


    local button = require("widget").newButton{       
            id = id,     
            x = x,
            y = y,
            label = "LETRA",            
            labelColor = {default={1,1,1,1}},
            labelXOffset = widthArrow*.5,            
            fontSize = 8,
            fillColor = {default={236/255,169/255,23/255,1}, over={236/255,169/255,23/255,.3}},
            strokeColor = {default={236/255,169/255,23/255,1}, over={236/255,169/255,23/255,0}},
            strokeWidth=1,
            shape="polygon",
            vertices = vertices,
            onRelease = onRelease,
            onEvent = onEvent,
    }


    -- positioning the button
    button.x = x or (left and (left + button.contentWidth*.5)) or (right and (right - button.contentWidth*.5))
    button.y = y or (top and (top + button.contentHeight*.5)) or (bottom and (bottom - button.contentHeight*.5))

    return button

end



t.newDateLabel = function(params)
    
    local parent = params.parent
    local day = params.day
    local month = params.month
    local x = params.x
    local top = params.top
    
    if type(month) == "number" then
       month =  require("module-calendar").dicMonthName[month]
    end
    
    month = string.sub(month,1,3)
    
    local groupDateLabel = display.newGroup()
    
    local labelDay = display.newText{parent=groupDateLabel, text=day, font=myGlobals.fontTextLabel, fontSize=34}
    labelDay:setFillColor(1,1,1)
    labelDay.anchorY = 0
    labelDay.x = labelDay.contentWidth*.5
    
    local labelMonth = display.newText{parent=groupDateLabel, text=month, x =labelDay.x, font=myGlobals.fontTextLabel, fontSize=24}
    labelMonth:setFillColor(1,1,1)
    labelMonth.anchorY = 0
    labelMonth.y = labelDay.y + labelDay.contentHeight - 5
    
    if parent then
        parent:insert(groupDateLabel)
    end
    
    -- positioning
    groupDateLabel.x = x + groupDateLabel.contentWidth*.5
    groupDateLabel.y = top - 5  -- text label appears to have a 5 internal margin, so le's remove it manually
    
    
    return groupDateLabel
end




t.newTextField = function(options)

    -- receiving params
    local x = options.x
    local y = options.y
    local w = options.w or options.width
    local h = options.h or options.height

    local listener = options.listener
    local placeholder = options.placeholder
    local text = options.text    
    local textColor = options.textColor or {195/255, 99/255, 70/255}
    local font = options.font or myGlobals.fontTextLabel
    local fontSize = options.fontSize or 15
    local returnKey = options.returnKey

    local forceUpperCase = options.forceUpperCase
    local maxChars = options.maxChars
    local invalidChars = options.invalidChars   -- string ex:  "<>{}%&*"

    local parent = options.parent
    local id = options.id
    local index = options.index


    local function textListener( event )

        if ( event.phase == "began" ) then
            -- user begins editing defaultField
            --print( event.text )


        elseif ( event.phase == "ended" or event.phase == "submitted" ) then
            -- do something with defaultField text
            --print( "finished", event.target.text )
            
        elseif ( event.phase == "editing" ) then
    
            if maxChars then
                event.target.text = string.sub(event.text,0,maxChars)
            end
           
            if invalidChars and invalidChars:find(event.newCharacters) then
                event.target.text = string.sub(event.text,0,#event.text - 1)
            end

            if forceUpperCase then
                event.target.text = string.upper(event.target.text)
            end
            
        end

        if listener then listener(event) end 

    end

    local input = native.newTextField( x, y, w, h )        
    input.hasBackground = false
    input:setTextColor( unpack(textColor) )
    input.font = native.newFont( font, fontSize )
    input.placeholder = placeholder
    input:setReturnKey( returnKey )
    input:addEventListener( "userInput", textListener )
    
    input.id = id
    input.index = index

    if parent then
        parent:insert(input)
    end


    return input            

end



return t


local album = {}


	album.new = function(params)

		local obj = {}

		obj.date = params.date
		obj.image = params.image  -- image={url=, width, height=}
		obj.title = params.title
		obj.artist = params.artist
		obj.musics = params.musics  -- {id, title, previewURL, duration={min=00,sec=00} }
		obj.radios = params.radios -- {deezer = "www.link_to_deezer.com/album", spotify = "www.link_to_spotify.com/album", ...}

		obj.previewURL = params.previewURL  -- previewURL for the album (not for a specific music)
		obj.buyDiscURL = params.buyDiscURL
		obj.buyDigitalAlbumURL = params.buyDigitalAlbumURL


		return obj

	end


return album
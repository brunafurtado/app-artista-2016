local ads = require"ads"
local timer = require"timer"

local adsHelper = {}
local appConfig = APP_CONFIG or require"appConfig"
local setup = appConfig.ads

local hitCount = 0

local function updateHitCount()
    hitCount = hitCount + 1
    hitCount = hitCount % setup.showSkip
end


local function canShowAd()
    return hitCount == 0
end


local AD_UNIT_IDS =
{
    ["Android"]   = setup.ANDROID_AD_UNIT_ID,
    ["iPhone OS"] = setup.IOS_AD_UNIT_ID,
    -- ["WinPhone"] = setup.WINDOWS_PHONE_AD_UNIT_ID,
}


local function selectUnitId()
    return AD_UNIT_IDS[system.getInfo("platformName")]
end


local function isPlatformIPhone()
    return system.getInfo("platformName") == "iPhone OS"
end


local function loadInterstitial()
    ads.load("interstitial")
end


local function adListener( event )
    -- The 'event' table includes:
    -- event.name: string value of "adsRequest"
    -- event.response: message from the ad provider about the status of this request
    -- event.phase: string value of "loaded", "shown", or "refresh"
    -- event.type: string value of "banner" or "interstitial"
    -- event.isError: boolean true or false

    local msg = event.response
    local provider = tostring(ads:getCurrentProvider())

    if ( event.isError ) then
        print("Error, no ad received", msg )
    else
        if event.phase == "loaded" then
            print("Ad loaded. Provider: " .. provider)
        elseif event.phase == "shown" then
            print("Ad shown. Provider: " .. provider)
            if not isPlatformIPhone() then
                loadInterstitial()
            end
        end
    end
end


function adsHelper.showInterstitial(delay)
    delay = delay or 0
    if canShowAd() then
        if isPlatformIPhone() then
            -- Warning: ads.load não está funcionando bem com o iOS 9. Por isso está sendo desabilidado para o iOS. (19/01/2016)
            -- TODO: Verificar se alguma atualização do Corona ou do iOS resolveu o problema.
            ads.show("interstitial")
        else
            timer.performWithDelay(delay,
                function ()
                    if ads.isLoaded("interstitial") then
                        ads.show("interstitial")
                    end
                end)
        end
    end
    updateHitCount()
end


function adsHelper.initialize()
    local unitID = selectUnitId()
    ads.init(setup.provider, unitID, adListener)
    if not isPlatformIPhone() then
        loadInterstitial()
    end
end


adsHelper.initialize()
return adsHelper

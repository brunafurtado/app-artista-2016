local eventClass = {}

eventClass.new = function(params)
	
	local obj = {}
	obj.name = params.name
	obj.date = params.date  -- {day, month, year, hour, min}
	
	obj.address = {}
	obj.address.name = params.address.name
	obj.address.street = params.address.street
	obj.address.neighborhood = params.address.neighborhood
	obj.address.city = params.address.city
	obj.address.state = params.address.state

	return obj

end


return eventClass
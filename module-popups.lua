
-- use the show() function  of this module to show the popup


local p ={}


local activePopup = nil


local popups = {}



popups.lyric = function(lyricObj)

    local frame = display.newGroup()


    local frameWidth = (280/320)*SCREEN_W
    local padding = {left=12, right=12, top = 10, bottom=0}
    local lbTitleWidth = frameWidth - padding.left - padding.right*3 - 2
    local lbTitle = display.newText2{parent=frame, text=lyricObj.title or "", font=APP_CONFIG.fontMedium, fontSize=18, width = lbTitleWidth, color={1,1,1}}
    lbTitle.anchorX, lbTitle.anchorY = 0, 0
    --lbTitle:setTextColor(1,1,1) 
    lbTitle.x = padding.left
    lbTitle.y = padding.top

    if lyricObj.author ~= nil and lyricObj.author ~= "" and lyricObj.author ~= false then
    
        local lbAuthor = display.newText{parent=frame, text="AUTOR:", font=APP_CONFIG.fontMedium, fontSize=14}
        lbAuthor.anchorX, lbAuthor.anchorY = 0, 0
        lbAuthor:setTextColor(1,1,1) 
        lbAuthor.x = lbTitle.x
        lbAuthor.y = lbTitle.y + lbTitle.contentHeight + 2


        local lbAuthorNameW = frameWidth - (lbAuthor.x + lbAuthor.contentWidth + 1) - padding.right*3 - 2
        local lbAuthorName = display.newText2{parent=frame, text=lyricObj.author, font=APP_CONFIG.fontLight, fontSize=14, width = lbAuthorNameW, color={1,1,1}}
        lbAuthorName.anchorX, lbAuthorName.anchorY = 0, 0
        --lbAuthorName:setTextColor(1,1,1) 
        lbAuthorName.x = lbAuthor.x + lbAuthor.contentWidth + 1
        lbAuthorName.y = lbAuthor.y --+ lbAuthor.contentHeight
    --if lyricObj.author == nil or lyricObj.author == "" or lyricObj.author == false then
        --lbAuthor.isVisible = false
    end


    local headerBackgroundH = frame.contentHeight + padding.top*2 --lbAuthorName.y + lbAuthorName.contentHeight + 10
    local headerBackground = display.newRect(0,0,frameWidth, headerBackgroundH)
    headerBackground.anchorX, headerBackground.anchorY = 0, 0 
    headerBackground:setFillColor(238/255, 169/255, 18/255)
    frame:insert(1,headerBackground)


    local btCloseHandler = function()        

        p.hide()
        

    end

    local btClose = require("library-widgets").newButtonOLD{
        right = frameWidth,
        y = headerBackground.y + headerBackground.contentHeight*.5,
        label="X",
        labelColor = { default={ 80/255, 60/255, 10/255, 1 }, over={  80/255, 60/255, 10/255, .3  } },
        width = padding.right*3,
        height = headerBackground.contentHeight,
        fillColor = { default={ 80/255, 60/255, 10/255, 0 }, over={ 1, 0.2, 0.5, 0 } },
        shape="rect",
        onRelease = btCloseHandler

    }
    frame:insert(btClose)

    local groupScrollView = display.newGroup()    


    local lbLyric = display.newText2{parent=frame, text=lyricObj.lyric, spaceBetweenLines = 3, font=APP_CONFIG.fontLight, fontSize=14, width=frameWidth - padding.left - padding.right}
    lbLyric.anchorX, lbLyric.anchorY = 0, 0
    lbLyric:setTextColor(0,0,0) 
    lbLyric.x = lbTitle.x
    lbLyric.y = headerBackground.y + headerBackground.contentHeight + 12


    local lyricBackgroundH = lbLyric.y + lbLyric.contentHeight + 12 - (headerBackground.y + headerBackground.contentHeight)
    local lyricBackground = display.newRect(0,headerBackground.y + headerBackground.contentHeight,frameWidth, lyricBackgroundH)
    lyricBackground.anchorX, lyricBackground.anchorY = 0, 0 
    lyricBackground:setFillColor(1,1,1)
    frame:insert(1,lyricBackground)


    -- -- positionating below screen so we  can move it up
    -- frame.y = SCREEN_H
    -- frame.x = CENTER_X - frame.contentWidth*.5

    -- -- moving the lyric frame up
    -- transition.to(frame, {y = 80, time=600, transition=easing.inOutExpo})


    
    
    --local shouldDisableSrollView = ( scrollViewHeight < frame.contentHeight)


    local scrollView = require("widget").newScrollView{
       x = CENTER_X,
       y = 0,
       width = SCREEN_W,
       height = SCREEN_H - _G.TOP_Y_AFTER_STATUS_BAR,    
       hideBackground = true,   
       backgroundColor = { 0.8, 0.8, 0},
       horizontalScrollDisabled = true,
       --verticalScrollDisabled = shouldDisableSrollView,
       topPadding = 50,
       bottomPadding = 40,
       hideScrollBar = true,
       --isBounceEnabled = false
    }          
    scrollView:insert(frame)
    frame.x = CENTER_X - frame.contentWidth*.5
    scrollView.anchorY = 0

    -- positionating below screen so we  can move it up
    scrollView.y = SCREEN_H

    -- moving the lyric frame up
    transition.to(scrollView, {y = _G.TOP_Y_AFTER_STATUS_BAR, time=600, transition=easing.inOutExpo})




    

    frame.close = btCloseHandler

    return scrollView
    

end


popups.buttons = function(buttonsList, onRelease)
    
    local btH = 30
    local btW = 1000/1080*SCREEN_W
    local lineStrokeW = 1
    local spaceToBtCancel = 4

    local frame = display.newGroup()
    frame.anchorChildren = true
    frame.anchorX = 0.5
    frame.anchorY = 0

    for i=1, #buttonsList do

        local bt = require("widget").newButton{
            x = btW*.5,
            y = btH*.5 + (btH + lineStrokeW)*(i-1),
            width = btW,
            height = btH,
            label = buttonsList[i],
            labelColor = {default={1,1,1}, over={1,1,1,.3}},
            font = APP_CONFIG.fontMedium,
            fontSize = 14,
            fillColor = { default={ 0, 0, 0, 0 }, over={ 0,0,0, .5 } },
            onRelease = function(e) 
                e.target.id = i
                e.target.label = buttonsList[i]
                if onRelease then
                    onRelease(e)
                end
                p.hide()
            end,
            shape="rect",
            }
        frame:insert(bt)
        

        if i < #buttonsList then
            local lineY = bt.y + bt.contentHeight*.5 + 1
            local line = display.newLine(bt.x -  bt.contentWidth*.48,lineY, bt.x +  bt.contentWidth*.48,lineY)
            line.strokeWidth = 1
            line:setStrokeColor( 1,1,1 , .7)
            frame:insert(line)

        end

    end

    local background = display.newRect(frame.contentWidth*.5, frame.contentHeight*.5, frame.contentWidth, frame.contentHeight)
    background:setFillColor( 0,0,0,.5 )
    background:setStrokeColor( 1,1,1,.7 )
    background.strokeWidth = 1
    frame:insert(1,background)


    local btCancel = require("widget").newButton{
            x = btW*.5,
            y = frame.y + frame.contentHeight + btH*.5 + spaceToBtCancel,
            width = btW,
            height = btH,
            label = "CANCELAR",
            labelColor = {default={1,1,1}, over={1,1,1,.3}},
            font = APP_CONFIG.fontMedium,
            fontSize = 14,
            fillColor = { default={ 1, 0, 0, .7 }, over={ 0,0,0, .5 } },
            strokeColor = { default={ 235/255, 200/255, 200/255, .7 }, over={ 0.4, 0.1, 0.2 } },
            strokeWidth = 1,
            onRelease=function(e) 
                e.target.id = 0 
                e.target.label = "CANCELAR"
                print(e.target.label)
                if onRelease then
                    onRelease(e)
                end
                p.hide()
            end,
            shape="rect",
            }
        frame:insert(btCancel)


        -- positioning out of screen
        frame.x = CENTER_X
        frame.y = SCREEN_H + 2    

    local bottomY = (SCREEN_H - frame.contentHeight - 55)


    transition.to(frame, {y = bottomY, time=600, transition=easing.inOutExpo})

    return frame

end




-----------------------------------
-- PUBLIC FUNCTIONS
-----------------------------------


p.show = function(popupName, popupObj, listener)

    if activePopup then return end  -- a popup is already being shown on screen

    -- creating background (will overlay scene)
	local background = display.newRect(0,0,SCREEN_W, SCREEN_H)
	background.anchorX, background.anchorY = 0,0
	background.fill={0,0,0}
	background.alpha = 0	
	p._background = background
	background:addEventListener( "tap", function() return true end )
	background:addEventListener( "touch", function() return true end )

--    if popupName ~= "buttons" then
    
--    end

    transition.to(background, {alpha=.5, time=600, transition=easing.inOutExpo})

    -- creating the popup frame
	activePopup = popups[popupName](popupObj, listener)

    -- making the back button hide the popup
    _G.rbBack.externalBackFuction = p.hide

end

p.hide = function()

    transition.to(activePopup,{y = SCREEN_H + 2, time=600, transition=easing.inOutExpo, onComplete=onClose})
	transition.to(p._background, {alpha=0, time=600, transition=easing.inOutExpo, onComplete=function() display.remove(p._background) end})

	activePopup = nil

    _G.rbBack.externalBackFuction = nil

end



return p


local t = {}

--local twitter = require("plugin.twitter")
local twitter = require("library-twitter")
 



-- twitter user info (will be filled by the getUser function, but let's start using some info just in case...)
t.artistInfo = { 
			profileName = APP_CONFIG.artistName.first .. " " .. APP_CONFIG.artistName.last,
		   	profileHandler = APP_CONFIG.twitterHandler,
		   	profileAvatarURL = nil,
		   	followersCount = nil,
}





-- get information about the artist
local getUser = function()

	local function myCallback(response)
			--print("Twitter JSON response: " .. response)

			local userInfo = require("json").decode(response) -- for all properties avilable on the reponse, see https://dev.twitter.com/rest/reference/get/users/show
			
			t.artistInfo.profileName = userInfo.name
			t.artistInfo.profileHandler = userInfo.screen_name
			t.artistInfo.profileAvatarURL = userInfo.profile_image_url	
			t.artistInfo.followersCount = userInfo.followers_count
			
	end		
	
	twitter.request({
		endpoint = "users/show",
		method = "GET",
		parameters = {
			screen_name = APP_CONFIG.twitterHandler,
			include_entities = false					
		},
		callback = myCallback,
	})

end









local tweetsFetchQty = 3 -- numer of tweets retrieved on each call

local tweetsList = {}			-- total list of tweets downloaded
local lastTweetsFecthedList = {}	-- list of tweets downloaded on the last request

local dicMonthInitialsToNumber = {Jan=1, Feb=2, Mar=3, Apr=4, May=5, Jun=6, Jul=7, Aug=8, Sep=9, Oct=10, Nov=11, Dec=12}
local extraFetchQty

-- maps the Tweets JSON response to the postObj table
local parseResponse = function(response)

	local listOfTweets = {}
	
	local tweets = require("json").decode(response) 

	for i=1+extraFetchQty, #tweets do

		local twt = tweets[i] 			-- for all properties of the twt object, see JSON response at https://dev.twitter.com/rest/reference/get/statuses/user_timeline

		-- converting the datetime info
		local datetimeString = twt.created_at
		local dateParts = datetimeString:split(" ")
		local hourParts = dateParts[4]:split(":")
		print("dateParts[2]=", dateParts[2])
		local datetime = {day=dateParts[3], month=dicMonthInitialsToNumber[dateParts[2]], year=dateParts[6], hour=hourParts[1],  min=hourParts[2],  sec=hourParts[3],  }

		-- getting the image info
		local image = nil 
		if twt.entities and twt.entities.media and twt.entities.media[1] then
			local imageURL = twt.entities.media[1].media_url
			local imageWidth = twt.entities.media[1].sizes.large.w
			local imageHeight= twt.entities.media[1].sizes.large.h
			image = {
				url = imageURL,
				width = imageWidth,
				height = imageHeight
			}				
		end
		
		-- created the tweet object that will be used on the class-socialMedia.lua
		local postObj = {  -- based on the class-socialMedia.lua
			
			name = "twitter",
			profileName = t.artistInfo.profileName,
	        profileHandler = t.artistInfo.profileHandler,
	        profileAvatarURL = t.artistInfo.profileAvatarURL,


			id = twt.id_str,
			content = twt.text,
			datetime = datetime,
			image = nil,
		}

		listOfTweets[#listOfTweets+1] = postObj			
	end				
	
	return listOfTweets

end



local getTweets = function(reset, callBack)

	if reset then
		tweetsList = nil
		tweetsList = {}
		lastTweetsFecthedList = nil
		lastTweetsFecthedList = {}
	end

	local startID = nil
	extraFetchQty = 0

	if tweetsList[#tweetsList] then
		startID = tweetsList[#tweetsList].id
		extraFetchQty = 1
	end
	

	local function myCallback(response)
		print("Twitter JSON response: " .. response)

		local listOfTweets = parseResponse(response)

		table.appendTable(tweetsList, listOfTweets)
		lastTweetsFecthedList = listOfTweets

		if callBack then
			callBack(lastTweetsFecthedList)
		end

	end		
	
	twitter.request({
		endpoint = "statuses/user_timeline",
		method = "GET",
		parameters = {
			screen_name = APP_CONFIG.twitterHandler,
			include_rts = 1,
			count = tweetsFetchQty + extraFetchQty,
			trim_user = 1,
			max_id = startID, -- Returns results with an ID equal or less than (that is, older than) the specified ID.
		},
		callback = myCallback,
	})

end



t.getFeedTwitter = getTweets




-- EXECUTION CODE

twitter.init(APP_CONFIG["twitterKEY"], APP_CONFIG["twitterPASS"], function(result)
	if result then
		getUser()
	end

end)

	-- timer.performWithDelay(1000, function() getTweets() end)
	
	-- timer.performWithDelay(3000, function() getTweets() end)






return t
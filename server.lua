local server = {}

local host = APP_CONFIG.serverURL


local lastSuccessRequestTime = nil

local lastFailedRequest = {endpoint=nil, params=nil, onCompleteDownload=nil} -- table that will store the last failed request for retry purposes




server.hasInternet = function (onComplete, showNoInternetScene, forceTest)
    --print("testing connection")

    -- uncomment line below to simulate no internet
    --if true then return onComplete(false) end


        -- let's make internet connection test only with a minimum of 5 seconds of difference
        if forceTest ~= true and lastSuccessRequestTime ~= nil and (os.time() - lastSuccessRequestTime) < 5 then
            return onComplete(true)
        end
    
        local function testResult( event )
            --print("connection result")
            if ( event.isError ) then
              --  print("error connecting " .. tostring(event.response))
                
                onComplete(false)
                
                if showNoInternetScene ==  nil or showNoInternetScene == true then            
                    -- showing no internet screen
                end
            else
                lastSuccessRequestTime = os.time()
                onComplete(true)
            end
        end
        
        network.request("https://www.google.com","GET",testResult)
end




-- function that gets a JSON from a server
local function downloadJSON(endpoint, params, onCompleteDownload )

    local method = method or "GET"
    local url = host
    --url = "https://s3-sa-east-1.amazonaws.com/redbeach-mk/".. endpoint .. "/data.json"    
    params = params or {}

    params["type"] = endpoint
    params["user"] = APP_CONFIG.serverKEY
    params["password"] = APP_CONFIG.serverPASS
    --params["timestamp"] = os.time()  -- this is used to avoid any http cache
    params["store"] = DEVICE.store   -- store to which the app was built for
    if DEVICE.isSimulator then
        params["store"] = (DEVICE.isApple and "apple") or (DEVICE.isGoogle and "google") or (DEVICE.isKindleFire and "amazon") or (DEVICE.isWindows and "windows")
    end
    params["os"] = DEVICE.os
    --params["model"] = require("mime").b64(DEVICE.model)
    --params["appVersion"] = require("mime").b64(system.getInfo("appVersionString"))


    -- converts a table from to a string format
    local paramToString = function(paramsTable)
        local str = ""
        local i = 1
        
        for paramName,paramValue in pairs(paramsTable) do 
            --print(paramName .. ": " .. paramValue)
            if i == 1 then
                str = paramName .. "=" .. paramValue
            else
                str = str .. "&" .. paramName .. "=" .. paramValue
            end
            i=i+1
        end
        
        return str
    end 
    
    local paramsString = paramToString(params)



    local function networkListener( event )

        local result, data, errorMessage = false, nil, nil
        print("event.isError=", event.isError, "event.status=", event.status)
        if ( event.isError or event.status ~= 200) then                            
                print( "Network error! - status=", event.status, " - response:", event.response)
                print("a  -  ", event.status == "-1" )
                print("scene=", require("composer").getSceneName("current"))
                print("lastsceneName=", _G.rbBack.lastGoToSceneArgs.sceneName)
                print("options=", _G.rbBack.lastGoToSceneArgs.options)
                if event.status == -1 then -- no internet connection

                    -- storing this failed request
                    lastFailedRequest.endpoint = endpoint
                    lastFailedRequest.params = params
                    lastFailedRequest.onCompleteDownload = onCompleteDownload



                    local sceneName = _G.rbBack.lastGoToSceneArgs.sceneName
                    local sceneParams = _G.rbBack.lastGoToSceneArgs.options
                    if sceneName == "scene-loading" then
                        _G.CANCEL_LOADING = true
                        require("composer").gotoScene("scene-no-internet", {effect="slideUp", time=400, params={sceneName=sceneName, sceneParams=sceneParams}})                    
                    else
                        require("composer").showOverlay("scene-no-internet", {effect="slideUp", time=400, params={sceneName=sceneName, sceneParams=sceneParams, isOverlay=true, isModal=true}})                    
                    end
                    

                end
                

                
                errorMessage = "Houve um problema ao tentar se comunicar com o servidor"

        else
            --print("Network ok. Now let's decode the JSON")
            local response = event.response:gsub("&#8211;", "-")  -- manually replacing a HTML code for its chair
            print("response=", response)
            local jsonData = require("json").decode(response)

            if jsonData == nil or type(jsonData) ~= "table" then
                print("Dados enviados pelo servidor estão com formato JSON inválido")

                -- Handler that gets notified when the alert closes
                local function onComplete( event )
                   if event.action == "clicked" then
                        local i = event.index
                        if i == 1 then
                            -- Retrying....
                            downloadJSON(endpoint, params, onCompleteDownload)
                            return
                        elseif i == 2 then
                            return native.requestExit()
                        end
                    end
                end
                local alert = native.showAlert( "Oopps", "Houve um problema na comunicação. O que deseja fazer?" , { "Tentar novamente", "Tentarei mais tarde" }, onComplete )

                return
            else
                if jsonData[endpoint] then
                    data = jsonData[endpoint]
                    result = true
                else
                    errorMessage = "Servidor não conseguiu realizar a operação desejada"
                end
            end
            --print("result=", result)
            onCompleteDownload({result=result, data=data, errorMessage=errorMessage})
        end
        
    end
    

    local headers = {}
    local params = {}
    
    if method == "POST" then
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        params.body = paramsString
    else
        headers["Content-Type"] = "application/json"
        url = url .. "?" .. paramsString
    end
    
    params.headers = headers
    params.timeout = 30
    print("url=", url)
    network.request( url, method, networkListener, params)


end


server.retryLastFailedRequest = function()
    downloadJSON(lastFailedRequest.endpoint, lastFailedRequest.params, lastFailedRequest.onCompleteDownload)

end


server.getNews = function(fetchQty, pageNumber, callBack)

    local params = {}    

    params["num"] = fetchQty
    params["page"] = pageNumber

    local function onReturn(event)

        local dataList = event.data

        callBack({result=event.result, data=dataList}) 
        
    end

    downloadJSON("getNews", params, onReturn)

end




server.getVideos = function(fetchQty, pageNumber, callBack)

    local params = {}    

    params["num"] = fetchQty
    params["page"] = pageNumber

    local function onReturn(event)

        local dataList = event.data

        callBack({result=event.result, data=dataList}) 
        
    end

    downloadJSON("getVideos", params, onReturn)

end



local dataByMonth -- will store the result data
server.getEvents = function(monthSelected, callBack)


    -- we already have the data, let's return it
    if dataByMonth then
        callBack({result=true, data=dataByMonth[monthSelected]}) 
        return
    end

    -- we don't have the data, let's get from the server
    local params = {}    
    params["num"] = -1  -- returning all events


    local function onReturn(event)



        local dataList = event.data or {}

        dataByMonth = {}
        local today = os.date("*t")
        
        -- let's separate the events by month
        for k, v in ipairs(dataList) do

            if (v.date.month >= today.month and v.date.year == today.year) or (v.date.year == today.year) then

                if dataByMonth[v.date.month] == nil then
                    dataByMonth[v.date.month] = {}
                end
                dataByMonth[v.date.month][#dataByMonth[v.date.month]+1] = v

            end
        end

         local compare = function( a, b )
                return a.date.day < b.date.day  -- if true, return a before b; if false, returns b before a
            end
             

        -- sorting data for each month
        for k, v in pairs(dataByMonth) do
            --print("mes ", k, " #=", #v)
            table.sort(v, compare)
        end

        callBack({result=event.result, data=dataByMonth[monthSelected]}) 
        
    end

    downloadJSON("getEvents", params, onReturn)


end


server.getPhotos = function(fetchQty, pageNumber, callBack)

    local params = {}    

    params["num"] = fetchQty
    params["page"] = pageNumber

    local function onReturn(event)

        local dataList = event.data

        callBack({result=true, data=dataList}) 
        
    end

    downloadJSON("getPhotos", params, onReturn)

end



--server.getDiscography = function(callBack)
server.getDiscography = function(media, fetchQty, pageNumber, callBack)

    local params = {}    
    --params.num = -1
    params["media"] = media
    params["num"] = fetchQty
    params["page"] = pageNumber


    local function onReturn(event)

        local dataList = event.data
        local dataListAlbum = dataList.album
        local dataListDVD = dataList.dvd
        local dataListSingles = dataList.single
        
        
        callBack({result=true, data={album=dataListAlbum, dvd=dataListDVD, single=dataListSingles}}) 
        --callBack({result=true, data={cd=dataListAlbum, dvd=dataListDVD, single=dataListSingles}}) 
        
    end

    downloadJSON("getDiscography", params, onReturn)

end

local dataRadiosOnline = nil
server.getRadiosOnline = function(fetchQty, pageNumber, callBack)
    
    if dataRadiosOnline and pageNumber > 1 then
        local data = unpack(dataRadiosOnline, (pageNumber-1)*fetchQty + 1, pageNumber*fetchQty)
        callBack({result=true, data=data}) 
        return 
    end

    local params = {}    
    params.num = -1    
    params.media = "album"
    
    local function onReturn(event)

        local dataList = event.data.album

        -- removing albums that don't have Radios
        for i=#dataList, 1, -1 do
            if dataList[i].HasRadio ~= true then
                table.remove(dataList,i)
            end
        end

        -- storing data for future requests
        local dataRadiosOnline = dataList  

        -- retorning the data      
        local data = {unpack(dataRadiosOnline, (pageNumber-1)*fetchQty + 1, pageNumber*fetchQty)}
        callBack({result=true, data=data}) 

    end


    downloadJSON("getDiscography", params, onReturn)

end

server.getLyric = function(musicID, callBack)

    local params = {}
    params.musicid = musicID

    local function onReturn(event)       
        callBack({result=event.result, data=event.data})
    end

    downloadJSON("getLyric", params, onReturn)
end




server.getSocialMediaNumber = function(socialMediaName)

    local count = 0
    if socialMediaName == "Twitter" then
        count = TWITTER.artistInfo.followersCount    
    elseif socialMediaName == "Facebook" then
        count = PROFILE.likesFacebook

    end

    if count == nil then
        count = 0
    end

    return count

end


server.getPreviewAlbums = function(fetchQty, pageNumber, callBack)

    local params = {}    
    params["num"] = fetchQty
    params["page"] = pageNumber

    local function onReturn(event)

        local data = event.data

        for i = #data,1,-1 do
            if data[i].musics == nil or #data[i].musics == 0 then
                print("the album has no music to preview. Let's remove it from the data.")
                table.remove(data,i)
            elseif data[i].musics[1].previewURL == nil or data[i].musics[1].previewURL == "" then
                print("the music has no previewURL. Let's remove it from the data.")
                table.remove(data,i)
            end

        end

        callBack({result=event.result, data=data}) 
        
    end

    downloadJSON("getPreviewAlbums", params, onReturn)

end



server.getSocialNetworksFeed = function(fetchQty, pageNumber, callBack)

    local params = {}    

    params["num"] = fetchQty
    params["page"] = pageNumber

    local function onReturn(e)
        if e.result ~= true then print("error getting json") return end

        local feed = e.data.posts
        local networkInfo = e.data.networkInfo

        -- creating the socialMedia class object from the feed
        local data = require("class-socialMedia").formatFeed(feed, networkInfo)


        callBack({result=true, data=data })

    end

    downloadJSON("getSocialFeed", params, onReturn)

end



server.getOtherArtists = function(callBack)

    local params = {}    

    local function onReturn(event)

        local dataList = event.data

        callBack({result=event.result, data=dataList}) 
        
    end

    downloadJSON("getOtherArtists", params, onReturn)

end




server.getProfile = function(callBack)

    local params = {}    

    local function onReturn(event)

        local data = event.data
        --print("event.result=", event.result)
        --print("event.data=", event.data)
        --for f,v in pairs(event.data) do
          --  print(f, ":", v)
        --end
        --print("event.data.description=", event.data.description)
        if event.result then
                        
            PROFILE.description = data.description
            PROFILE.awards = data.awards
            PROFILE.image = data.image
            PROFILE.likesFacebook = data.likesFacebook

            if PROFILE.image and PROFILE.image.url then
                server.downloadImage(PROFILE.image.url)
            end                        

        end
        
    end

    downloadJSON("getProfile", params, onReturn)

end


server.getFeaturedAlbum = function(callBack)

    local params = {}    

    local function onReturn(event)

        local dataList = event.data

        callBack({result=event.result, data=dataList}) 
        
    end

    downloadJSON("getFeaturedAlbum", params, onReturn)


end


-- downloads (async) an image for posterior use
server.downloadImage = function(url, onComplete, baseDir)
    if url == nil then
        print("no URL specified to download. Canceling download.")
        return
    end

    baseDir = baseDir or system.TemporaryDirectory

    local img = display.newImage(AUX.getFilenameFromURL(url), baseDir)
    if img then
        -- print("img already downloaded")
        display.remove(img)
        if onComplete then
            onComplete(true)   
        end
        return
    end

    -- we cannot save an image on the ResourcesDirectory
    if baseDir == system.ResourceDirectory then
        baseDir = system.TemporaryDirectory
    end

    local function listener(event)
        
        if ( event.isError ) then
            --print( "Network error - download failed" )
            if onComplete then
                onComplete(false, url, baseDir)
            end
        elseif ( event.phase == "began" ) then
            --print( "Progress Phase: began" )
        elseif ( event.phase == "ended" ) then
            --print( "Download complete" )            
            if onComplete then
                onComplete(true)   
            end
        end

    end

    network.download( url, "GET", listener, nil, AUX.getFilenameFromURL(url), baseDir )

end


return server


local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------
local tableView = nil
local fetchQty = 10
local pageIndex = 0
local hasMoreDataToLoad = nil



-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.

    local sceneAssets = _G.IMAGES.news

    local topBar = require("module-topBar").new(sceneGroup, "Vídeos")
    sceneGroup:insert(topBar)

    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}


    --------------------------------------------------
    -- REFRESHING FUNTIONS
    --------------------------------------------------


    local groupRefreshing
    local function showScrollViewRefreshing()

        if groupRefreshing then return end  -- if already being shown, don't show again

        groupRefreshing = require("module-frames").newRefreshingGroup()
        sceneGroup:insert(groupRefreshing)
        groupRefreshing.y = tableView.y - tableView.contentHeight*.5
        groupRefreshing.x = tableView.x - tableView.contentWidth*.5

        return groupRefreshing

    end

    local function removeScrollViewRefreshing()
        if groupRefreshing then
            display.remove(groupRefreshing)
            groupRefreshing = nil

            timer.performWithDelay( 10, function()
                tableView:scrollToY( { y=tableView._topPadding, time=800, onComplete=scrollComplete } )
            end)
        end
    end

    --------------------------------------------------
    -- FOOTER FUNTIONS
    --------------------------------------------------

    local function addScrollViewFooter(group)

        local view = group

        local groupFooter = display.newGroup()
        groupFooter.y = view.contentHeight - 15
        view:insert(groupFooter)
        tableView.groupFooter = groupFooter

        local btLoadMore = require("library-widgets").newButton{
                top=14,
                x = CENTER_X,
                --width = 160,
                height = 30,
                backgroundColor={1,0,0,0},
                backgroundOverColor={1,0,1,0},
                label="CARREGAR MAIS",
                labelFontSize = 8,
                labelFont = APP_CONFIG.fontMedium,
                labelColor = {26/255,118/255,173/255},
                labelOverColor = {26/255,118/255,173/255,.3},

                imageFile = APP_CONFIG.images.icons.plus.path,
                imageWidth = APP_CONFIG.images.icons.plus.width,
                imageHeight = APP_CONFIG.images.icons.plus.height,
                imageColor = {26/255,118/255,173/255},
                imageOverColor = {26/255,118/255,173/255,.3},
                imagePosition="left",
                imagePadding = {right = 4},

                onTap = function(e)
                print("bt e.phase=", e.phase)
                    sceneGroup.loadMoreSpinner = AUX.showSpinner(e.target)
                    e.target.isVisible = false
                    sceneGroup.getContent(false)

                    return true

                end,
        }
        groupFooter:insert(btLoadMore)

        local lbNoMoreToLoad = display.newText{text="NÃO HÁ MAIS VÍDEOS", font=APP_CONFIG.fontMedium, fontSize=8}
        lbNoMoreToLoad.x = CENTER_X
        lbNoMoreToLoad.y = 29
        lbNoMoreToLoad:setTextColor( 26/255,118/255,173/255)

        groupFooter:insert(lbNoMoreToLoad)


        -- showing / hiding the buttons
        btLoadMore.isVisible = hasMoreDataToLoad
        lbNoMoreToLoad.isVisible = not hasMoreDataToLoad

        -- function to show the No More To Load label
        groupFooter.showNoMoreToLoad = function()
            btLoadMore.isVisible = false
            lbNoMoreToLoad.isVisible = true
        end

        return groupFooter
    end


    local function removeScrollViewFooter()
        display.remove(tableView.groupFooter)
    end




    ------------------------------------------------------------------------------------
    -- FUNCTION DEFINITIONS
    ------------------------------------------------------------------------------------

    local tableViewData = {}  -- table with the current data of the tableView


    local function onRowRender( event )

        -- Get reference to the row group
        local row = event.row
        local index = event.row.index

        -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
        local rowHeight = row.contentHeight
        local rowWidth = row.contentWidth


        local rowEntry = require("module-frames").videoEntry(tableViewData[index],false, btSeeMoreHandler)
        if tableViewData[index].hasShown ~= true then
            transition.from(rowEntry,{alpha=0, time=400})
            tableViewData[index].hasShown = true
        end

        row:insert(rowEntry)
        rowEntry.x = CENTER_X - rowEntry.contentWidth*.5


        if index == #tableViewData then
            row.groupFooter = addScrollViewFooter(row)
        end

    end

    -- inserts 1 row inside the tableView with a specified delay (ms)
    local function insertRow(newsObj, delay)

        local rowFrame = require("module-frames").videoEntry(newsObj, true)
        local rowHeight = rowFrame.contentHeight

        display.remove(rowFrame)
        rowFrame = nil

        -- Insert a row into the tableView
        timer.performWithDelay( delay,function()
            tableView:insertRow({
                    rowHeight = rowHeight,
                    rowColor = { default={ 0, 1, 1,0 }, over={ 1, 0.5, 0, 0 } },
                }
            )
        end)

    end

    -- add data (list of objects) to the tableView. It can do a clean insert (deleteBeforeAdding = true) or just append new data
    local function addDataToTableView(data, deleteBeforeAdding)

        if deleteBeforeAdding then
            tableView:deleteAllRows()
            tableViewData = {}

            tableViewData = {}
            pageIndex = 0
        end

        for i=1,#data do
            if data[i] == nil then
                --print("photoOBj nil")
            else
                insertRow(data[i], 10)
                tableViewData[#tableViewData+1] = data[i]
            end
        end
    end


    -- TableView listener
    local function tableViewListener( event )


        local phase = event.phase

        if ( phase == "began" ) then print( "Scroll view was touched" )
        --elseif ( phase == "moved" ) then
        elseif ( phase == "ended" ) then print( "Scroll view was released" )
        end
        --print("event.limitReached=", event.limitReached)
        -- In the event a scroll limit is reached...
        if ( event.limitReached ) then
            if ( event.direction == "up" ) then print( "Reached top limit" )
            elseif ( event.direction == "down" ) then print( "Reached bottom limit" )

                if groupRefreshing or sceneGroup.loading then
                    print("there is already a refreshing in progress")
                    return
                end

                local y = tableView:getContentPosition()

                if y > 50 then
                    local function onScrollComplete()
                        print( "Scroll complete!" )
                        tableView:setIsLocked( true )
                    end

                    groupRefreshing = showScrollViewRefreshing()
                    tableView:scrollToY{
                        y = groupRefreshing.contentHeight + 10,
                        time = 800,
                        onComplete = onScrollComplete
                    }


                    timer.performWithDelay( 10, function() sceneGroup.getContent(true) end )
                end

            end
        end

        return true
    end

    -- Create the widget
    local tableViewTop = topBar.y + topBar.contentHeight
    local tableViewH = SCREEN_H - tableViewTop
    local tableViewTopPadding = 20
    tableView = require("widget").newTableView{
        x = CENTER_X,
        top = tableViewTop,
        height = tableViewH,
        width = SCREEN_W,
        hideBackground = true,
        noLines = true,
        topPadding = tableViewTopPadding,
        bottomPadding = 40,
        onRowRender = onRowRender,
        onRowTouch = function(e)

            if e.phase ~= "release" and e.phase ~= "tap" then return end

            tableView.isVisible = false

            local webView
            local btBack

            local btBackRelease = function()

                display.remove(webView)
                webView = nil


                timer.performWithDelay(10, function()
                    _G.rbBack.externalBackFuction = nil;
                    display.remove(btBack)
                    tableView.isVisible = true
                    sceneGroup.onSystemEvent = nil
                    Runtime:removeEventListener( "system", sceneGroup.onSystemEvent )
                end)

                return true

            end

            btBack = require("library-widgets").newButton{
                left = 8,
                top = tableViewTop,
                --width = 380*(320/1080),
                height=30,

                label = "VOLTAR",
                labelColor = {0,162/255,1},
                labelOverColor = {0,162/255,1,.3},
                labelFont = APP_CONFIG["fontMedium"],
                labelFontSize = 10,

                imageFile = APP_CONFIG.images.icons["arrowLeft"].path,
                imageWidth = APP_CONFIG.images.icons["arrowLeft"].width,
                imageHeight = APP_CONFIG.images.icons["arrowLeft"].height,
                imagePosition = "left",
                imagePadding = {right=4, bottom=2},

                imageColor = {0,162/255,1},
                imageOverColor = {0,162/255,1,.3},

                backgroundColor = { 31/255, 148/255, 241/255, 0 },
                backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

                strokeWidth = 1,
                strokeColor = { 31/255, 148/255, 241/255, 1 },
                strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

                onRelease = btBackRelease,
            }
            sceneGroup:insert(btBack)


            _G.rbBack.externalBackFuction = function()
                btBackRelease()
            end

            local rowIndex = e.target.index
            local videoURL = tableViewData[rowIndex].videoURL
            local videoTitle = tableViewData[rowIndex].title

            --print(tableViewData[rowIndex].title)
            local webViewTop = btBack.y + btBack.contentHeight*.5
            local webViewH = SCREEN_H - webViewTop

            --local aa  = native.showWebPopup( videoURL, CENTER_X, CENTER_Y, SCREEN_W, tableViewH, options )
            webView = native.newWebView( tableView.x, webViewTop + webViewH*.5, tableView.contentWidth, webViewH )
            webView:request( videoURL )
            sceneGroup:insert(webView)
            --aa.y = CENTER_Y + 50
            _G.MENU.objToTranslate = webView

            -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
            local eventFlurry = "view;Vídeo;Vídeo;" .. videoTitle .. ";" .. videoURL .. ";"
            print(eventFlurry)
            _G.analytics.logEvent(eventFlurry)
            -- fim


            sceneGroup.onSystemEvent = function( event )
                --print("running: main.onSystemEvent." .. tostring(event.type))

                if ( event.type == "applicationSuspend") then
                    -- Stops transitions and timers
                    print("suspending application")
                    btBackRelease()

                end

            end
            Runtime:addEventListener( "system", sceneGroup.onSystemEvent )

        end,


        listener = tableViewListener
    }
    tableView._topPadding = tableViewTopPadding
    sceneGroup:insert(tableView)


    -- get content to show
    sceneGroup.getContent = function(resetData)

        local function onCallBack(returnData)
            display.remove(sceneGroup.loading)
            sceneGroup.loading = nil
            display.remove(sceneGroup.loadMoreSpinner)

            tableView:setIsLocked( false )
            if returnData.result then

                local data = returnData.data
                if data == nil or #data == 0 then
                    hasMoreDataToLoad = false
                    local lastRow = tableView:getRowAtIndex( tableView:getNumRows() )
                    if ( lastRow ) then
                        lastRow.groupFooter.showNoMoreToLoad()
                    else
                        print("last row not visible at the moment")
                    end

                else
                    removeScrollViewFooter()
                    hasMoreDataToLoad = true
                end

                removeScrollViewRefreshing()
                timer.performWithDelay( 10, function()
                     addDataToTableView(data, resetData)
                end)


            end
        end

        pageIndex = pageIndex + 1
        SERVER.getVideos(fetchQty, pageIndex, onCallBack)
    end


    -- show loading icon
    sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y)
    sceneGroup:insert(sceneGroup.loading)

    -- get data from server
    sceneGroup.getContent(true)


    -- bringing topBar to front
    sceneGroup:insert(topBar)

end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
        adsHelper.showInterstitial()

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        _G.rbBack.addBack()

        local detailsTopBar = composer.getVariable( "temp_topBar" )
        display.remove(detailsTopBar)

        _G.MENU.highlightMenuWithLabel("Vídeos")

        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Vídeos;Vídeos;"
        print(eventFlurry)
        _G.analytics.logEvent(eventFlurry)
        -- fim

    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
        if sceneGroup.onSystemEvent then
            Runtime:removeEventListener( "system", sceneGroup.onSystemEvent )
            sceneGroup.onSystemEvent = nil
        end
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene

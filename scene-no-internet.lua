local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    local sceneAssets = _G.IMAGES.noInternet

    local background = display.newRect(sceneGroup, SCREEN_W*.5, SCREEN_H*.5, SCREEN_W, SCREEN_H)
    background.fill = {27/255, 116/255, 141/255}

    local group = display.newGroup( )
    sceneGroup:insert(group)

    local icon = display.loadImage(sceneAssets.cloud)
    icon.x, icon.y = icon.contentWidth*.5, icon.contentHeight*.5
    group:insert(icon)


    local lbConnectionFailOrConnected = display.newText{parent=group, text="FALHA NA CONEXÃO", font=APP_CONFIG.fontMedium, fontSize=14}
    lbConnectionFailOrConnected:setTextColor( 1,1,1 )
    sceneGroup.lbConnectionFailOrConnected = lbConnectionFailOrConnected

    local lbAppNotConnected = display.newText{parent=group,text="SEU APLICATIVO NÃO ESTÁ CONECTADO", font=APP_CONFIG.fontLight, fontSize=12}
    lbAppNotConnected:setTextColor( 1,1,1 )

    local function onBtRetryRelease(event)

        local bt = event.target

        lbConnectionFailOrConnected.isVisible = false
        lbAppNotConnected.isVisible = false
        bt.isVisible = false

        --local loading = require("rb-aux").showLoadingAnimation(lbConnectionFailOrConnected.x, lbConnectionFailOrConnected.y + (lbAppNotConnected.y + lbAppNotConnected.contentHeight*.5 - lbConnectionFailOrConnected.y)*.5 )
        local loading = require("rb-aux").showLoadingAnimation(lbConnectionFailOrConnected.x, bt.y )
        group:insert(loading)
        sceneGroup.loading = loading

        local function onCallBack(hasInternet)
            
            display.remove(loading)
            
            if hasInternet then
                lbConnectionFailOrConnected.text = "CONEXÃO ENCONTRADA"
                lbConnectionFailOrConnected.isVisible = true

                
                if sceneGroup.isOverlay then
                    timer.performWithDelay( 10, function()
                        require("server").retryLastFailedRequest()
                    end)
                    require("composer").hideOverlay("slideDown", 800)
                else
                    _G.CANCEL_LOADING = false
                    print("setou _G.CANCEL_LOADING = false")
                    local params = sceneGroup.comingFrom.sceneParams or {}
                    params.effect = "slideDown"
                    params.time = 400
                    require("composer").gotoScene(sceneGroup.comingFrom.sceneName, params)
                end
        
                
            
            else
                sceneGroup.resetScreen()
            end

        end
        timer.performWithDelay( 200, function()
            require("server").hasInternet(onCallBack, false, true)
        end)

    end

    local btRetry = require("library-widgets").newButton{
            right = right,
            top = top,
            y = y,
            height=30,
            --width = 120,
            
            label = "TENTAR NOVAMENTE",
            labelColor = labelColor,
            labelOverColor = labelOverColor,
            labelFont = APP_CONFIG["fontMedium"],
            labelFontSize = 8,
            labelPadding = {left = 8, right = 20},
            
            imageFile = sceneAssets.refresh.path,
            imageWidth = sceneAssets.refresh.width,
            imageHeight = sceneAssets.refresh.height,
            imagePosition = "left",
            imagePadding = {left=0},

            imageColor = imageColor,
            imageOverColor =imageOverColor,

            backgroundColor = { 15/255,65/255,79/255, 1 }, 
            backgroundOverColor ={ 15/255,65/255,79/255, 0.3 },

            onRelease = onBtRetryRelease,
        }

        group:insert(btRetry)


        local maxW = math.max(icon.contentWidth, lbConnectionFailOrConnected.contentWidth, lbAppNotConnected.contentWidth, btRetry.contentWidth)

        -- centering objects
        icon.x = maxW*.5
        lbConnectionFailOrConnected.x = maxW*.5
        lbAppNotConnected.x = maxW*.5
        btRetry.x = maxW*.5

        icon.y  = icon.contentHeight*.5
        lbConnectionFailOrConnected.y = icon.y + icon.contentHeight*.5 + lbConnectionFailOrConnected.contentHeight*.5 + 8
        lbAppNotConnected.y = lbConnectionFailOrConnected.y + lbConnectionFailOrConnected.contentHeight*.5 + lbAppNotConnected.contentHeight*.5 + 5
        btRetry.y = lbAppNotConnected.y + lbAppNotConnected.contentHeight*.5 + btRetry.contentHeight*.5 + 10



        group.x = display.contentCenterX - group.contentWidth*.5
        group.y = display.contentCenterY - group.contentHeight*.5



        sceneGroup.resetScreen = function()
            lbConnectionFailOrConnected.text = "FALHA NA CONEXÃO"
            lbAppNotConnected.isVisible = true
            lbConnectionFailOrConnected.isVisible = true
            btRetry.isVisible = true

            display.remove(sceneGroup.loading)
            sceneGroup.loading = nil

        end
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then    
        -- Called when the scene is still off screen (but is about to come on screen).        

        
        sceneGroup.resetScreen()

        sceneGroup.comingFrom = {sceneName=event.params.sceneName, sceneParams=event.params.sceneParams}
        sceneGroup.isOverlay = event.params.isOverlay


    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.


    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
        
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
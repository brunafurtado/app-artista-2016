local n = {}


local notifPreferences = {}


notifPreferences.news = {id="news", label="Notícias", value=true}
notifPreferences.agenda = {id="agenda", label="Agenda", value=true}
notifPreferences.videos = {id="videos", label="Vídeos", value=true}
notifPreferences.photos = {id="photos", label="Fotos", value=false}
notifPreferences.discography = {id="discography", label="Discografia", value=false}



n.getNotificationsPreferences = function()

	local listOfNotifPreferences = {}
	
	listOfNotifPreferences[#listOfNotifPreferences+1] = notifPreferences.news
	listOfNotifPreferences[#listOfNotifPreferences+1] = notifPreferences.agenda
	listOfNotifPreferences[#listOfNotifPreferences+1] = notifPreferences.videos
	listOfNotifPreferences[#listOfNotifPreferences+1] = notifPreferences.photos
	listOfNotifPreferences[#listOfNotifPreferences+1] = notifPreferences.discography
	
	return listOfNotifPreferences
end


n.updateNotificationPreference = function(id, value, onComplete)
print("on updateNotificationPreference, [" .. id .."]=" .. tostring(value))
	notifPreferences[id].value = value

	print("ONESIGNAL=", ONESIGNAL)
	if ONESIGNAL == nil then return end


	SERVER.hasInternet(function(result) 

		if result then
			if value then
				print("sending tag id=", id)
				ONESIGNAL.SendTag(id, true)
			else
				ONESIGNAL.DeleteTag(id)
			end
		end

		onComplete(result)

	end)
	
end



n.downloadNotificationSettings = function(onCallBack)
	print("on downlaodNotificationSettings")

	function onOneSignalCallBack(tags)
		print("onCallBack - tags", tags)
		if tags then
			for key,value in pairs(tags) do
		      print( key, value )
		      print("notifPreferences[key]=", notifPreferences[key] )
		      if notifPreferences[key] then
		      	print("setou" )
		      	notifPreferences[key].value = value
		      else
		      	print("tag is not a notification preference one. let's ignore it")
		      end	
		    end

		end
	   

	   onCallBack()

	end	

	-- end)
	print("ONESIGNAL=", ONESIGNAL)
	if ONESIGNAL then
		print("calling getTags")
		if DEVICE.isSimulator then
			onOneSignalCallBack()
		else
			ONESIGNAL.GetTags(onOneSignalCallBack)
		end
		
	end
	

end


-- RUN CODE

if AUX.isFirstRun then

	local tagsTable = {}
	for k,v in pairs(notifPreferences) do
		tagsTable[k] = v.value
	end

	ONESIGNAL.SendTags(tagsTable)

end



return n
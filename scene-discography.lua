local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


local dicBarcodeToFrame = {}

local isLoading = false

local menuLabels = {"ÁLBUNS", "DVDs", "SINGLES"}
local dicLabelToIDs = {[menuLabels[1]]="album", [menuLabels[2]]="dvd", [menuLabels[3]]="single"}
--local dicLabelToIDs = {[menuLabels[1]]="CD", [menuLabels[2]]="DVD", [menuLabels[3]]="SINGLE"}


local fetchQty = 10
local pageIndex = 0


-- scrollView widgets
local scrollViewAlbums = nil
local scrollViewDVDs = nil
local scrollViewSingles = nil

local scrollViewSelected = nil



-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.

    local sceneAssets = _G.IMAGES.discography

    local topBar = require("module-topBar").new(sceneGroup, "Discografia")
    sceneGroup:insert(topBar)

    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}



    --------------------------------------------------
    -- NAV BAR
    --------------------------------------------------

    local function onSegmentPress( event )
        local target = event.target
        --print( "Segment Label is:", target.segmentLabel )
        --print( "Segment Number is:", target.segmentNumber )

        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Discografia;" .. target.segmentLabel .. ";"
        print(eventFlurry)
        _G.analytics.logEvent(eventFlurry)
        --print("Discografia;".. target.segmentLabel .. ";")
        --_G.analytics.logEvent("Discografia;".. target.segmentLabel .. ";")
        -- fim

        scrollViewAlbums.isVisible = false
        scrollViewDVDs.isVisible = false
        scrollViewSingles.isVisible = false

        if target.segmentNumber == 1 then
            scrollViewSelected = scrollViewAlbums

        elseif target.segmentNumber == 2 then
            scrollViewSelected = scrollViewDVDs

        elseif target.segmentNumber == 3 then
            scrollViewSelected = scrollViewSingles
        end

        pageIndex = scrollViewSelected._pageIndex
        scrollViewSelected.isVisible = true
        transition.from(scrollViewSelected,{alpha=0, time=400})

        if scrollViewSelected._pageIndex == 0 then

            if sceneGroup.loading == nil then
                sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y)
                sceneGroup:insert(sceneGroup.loading)

            end
            sceneGroup.getContent()

        end


    end
    sceneGroup.onSegmentPress = onSegmentPress

    local navBar = require("custom-widget").newSegmentedControl{
        x = CENTER_X,
        top = topBar.y + topBar.contentHeight + 20,
        segmentWidth = 0.96*SCREEN_W / #menuLabels,
        segmentHeight = 26,
        segmentBackgroundColor = {0,0,0,0},
        segmentStrokeColor = {112/255, 112/255, 112/255},
        segmentFontColor={0,0,0},
        segmentPressedBackgroundColor = {37/255,170/255,255/255},
        segmentPressedStrokeColor = {37/255,170/255,255/255},
        segmentPressedFontColor={1,1,1},
        segments = menuLabels,
        segmentLabelFontSize=12,
        defaultSegment = 1,
        onPress = onSegmentPress,
        spacing=10*(320/1080),
    }
    sceneGroup:insert(navBar)
    sceneGroup.navBar = navBar



    --------------------------------------------------
    -- REFRESHING FUNTIONS
    --------------------------------------------------


    local groupRefreshing
    local function showScrollViewRefreshing()

        if groupRefreshing or sceneGroup.loading ~= nil then return end  -- if already being shown or there is a loading on screen, don't show again

        groupRefreshing = require("module-frames").newRefreshingGroup()
        sceneGroup:insert(groupRefreshing)
        groupRefreshing.y = scrollViewSelected.y - scrollViewSelected.contentHeight*.5
        groupRefreshing.x = scrollViewSelected.x - scrollViewSelected.contentWidth*.5

        return groupRefreshing

    end

    local function removeScrollViewRefreshing()
        if groupRefreshing then
            display.remove(groupRefreshing)
            groupRefreshing = nil
            timer.performWithDelay( 10, function()
                scrollViewSelected:scrollTo( "top", {
                        time=800,
                        --onComplete=function() print("finshied scrolling to top") end
                        })
            end)
        end
    end


    --------------------------------------------------
    -- FOOTER FUNTIONS
    --------------------------------------------------

    local function addScrollViewFooter(hasMoreToLoad)

        local view = scrollViewSelected:getView()

        local groupFooter = display.newGroup()
        groupFooter.y = (#scrollViewSelected._data == 0 and 0) or view.contentHeight
        scrollViewSelected:insert(groupFooter)
        scrollViewSelected.groupFooter = groupFooter


        if hasMoreToLoad then

            local btLoadMore = require("library-widgets").newButton{
                    top=14,
                    x = CENTER_X,
                    --width = 160,
                    height = 30,
                    backgroundColor={1,0,0,0},
                    backgroundOverColor={1,0,1,0},
                    label="CARREGAR MAIS",
                    labelFontSize = 8,
                    labelFont = APP_CONFIG.fontMedium,
                    labelColor = {26/255,118/255,173/255},
                    labelOverColor = {26/255,118/255,173/255,.3},

                    imageFile = APP_CONFIG.images.icons.plus.path,
                    imageWidth = APP_CONFIG.images.icons.plus.width,
                    imageHeight = APP_CONFIG.images.icons.plus.height,
                    imageColor = {26/255,118/255,173/255},
                    imageOverColor = {26/255,118/255,173/255,.3},
                    imagePosition="left",
                    imagePadding = {right = 4},

                    onRelease = function(e)
                        sceneGroup.loadMoreSpinner = AUX.showSpinner(e.target)
                        e.target.isVisible = false
                        sceneGroup.getContent()
                    end,
            }
            groupFooter:insert(btLoadMore)

        else

            local lbNoMoreToLoad = display.newText{text="NÃO HÁ MAIS POSTS", font=APP_CONFIG.fontMedium, fontSize=8}
            lbNoMoreToLoad.x = CENTER_X
            lbNoMoreToLoad.y = 29
            lbNoMoreToLoad:setTextColor( 26/255,118/255,173/255)

            groupFooter:insert(lbNoMoreToLoad)

        end

    end


    local function removeScrollViewFooter()
        display.remove(scrollViewSelected.groupFooter)
    end


    --------------------------------------------------
    -- SCROLLVIEWS
    --------------------------------------------------


    local scrollViewTop = navBar.y + navBar.contentHeight + 14*(320/1080)
    local scrollViewHeight = SCREEN_H - scrollViewTop

    -- ScrollView listener
    local function scrollListener( event )

            local phase = event.phase
            if event.target and event.target._view then
                event.target._view.x = 0 -- manually disabling the horizontal scroll
            end
            if ( phase == "began" ) then --print( "Scroll view was touched" )
            --elseif ( phase == "moved" ) then
            --elseif ( phase == "ended" ) then print( "Scroll view was released" )
            end

            -- In the event a scroll limit is reached...
            if ( event.limitReached ) then
                if ( event.direction == "up" ) then --print( "Reached top limit" )
                elseif ( event.direction == "down" ) then --print( "Reached bottom limit" )

                    if groupRefreshing or sceneGroup.loading then
                        print("there is already a refreshing in progress")
                        return
                    end

                    local x, y = scrollViewSelected:getContentPosition()

                    if y > 50 then
                        local function onScrollComplete()
                            print( "Scroll complete!" )
                            scrollViewSelected:setIsLocked( true )
                        end

                        local groupRefreshing = showScrollViewRefreshing()

                        scrollViewSelected:scrollToPosition{
                            --y = 50,
                            y = (groupRefreshing and groupRefreshing.contentHeight + 10) or 60,
                            time = 800,
                            onComplete = onScrollComplete
                        }

                        timer.performWithDelay( 10, function() sceneGroup.getContent(nil, true) end )
                    end

                end
            end

            return true
        end

    local scrollViewOptions = {
       x = CENTER_X,
       y = scrollViewTop + scrollViewHeight*.5,
       width = SCREEN_W,
       height = scrollViewHeight,
       hideBackground = true,
       hideScrollBar = true,
       backgroundColor = { 0.8, 0.8, 0},
       horizontalScrollDisabled = true,
       topPadding = 10,
       bottomPadding = 20,
       listener = scrollListener,
    }


    -- creates the scrollview for Albums (1st button)
    scrollViewAlbums = require("widget").newScrollView(scrollViewOptions)
    sceneGroup:insert(scrollViewAlbums)
    scrollViewAlbums._data = {}
    scrollViewAlbums._content = display.newGroup()
    scrollViewAlbums._media = dicLabelToIDs[menuLabels[1]]
    scrollViewAlbums._pageIndex = 0

    scrollViewSelected = scrollViewAlbums


    -- creates the scrollview (2nd button)
    scrollViewDVDs = require("widget").newScrollView(scrollViewOptions)
    sceneGroup:insert(scrollViewDVDs)
    scrollViewDVDs.isVisible = false
    scrollViewDVDs._data = {}
    scrollViewDVDs._content = display.newGroup()
    scrollViewDVDs._media = dicLabelToIDs[menuLabels[2]]
    scrollViewDVDs._pageIndex = 0

    -- creates the scrollview (3rd button)
    scrollViewSingles = require("widget").newScrollView(scrollViewOptions)
    sceneGroup:insert(scrollViewSingles)
    scrollViewSingles.isVisible = false
    scrollViewSingles._data = {}
    scrollViewSingles._content = display.newGroup()
    scrollViewSingles._media = dicLabelToIDs[menuLabels[3]]
    scrollViewSingles._pageIndex = 0


    -- adds/appends content to scrollView
    local function addContentToScrollView(group)

        local view = scrollViewSelected:getView()

        local groupTop = (#scrollViewSelected._data == 0 and 0) or (view.contentHeight + 14)
        group.x = CENTER_X - group.contentWidth*.5
        group.y = groupTop

        scrollViewSelected:insert(group)

    end


    -- get content from server
    sceneGroup.getContent = function(onFinish, resetData)
        isLoading = true

        if resetData then
            pageIndex = 0
        end


        local function onCallBack(event)

            scrollViewSelected:setIsLocked( false )
            if event.result ~= true then
                return print("something wrong happened with the data of discography")

            end

            if resetData then
                display.remove(scrollViewSelected._content)
                scrollViewSelected._content = nil
                scrollViewSelected._content = display.newGroup()
                scrollViewSelected._data = {}
            end


            --local data = event.data[media]
            local media = string.lower(scrollViewSelected._media)
            local data = event.data[(media)]

            removeScrollViewRefreshing()

            removeScrollViewFooter()


            local function createScrollViewContentGroup(data, scrollViewObj, isDVD, isSingle)

                local groupContent = scrollViewSelected._content

                --incluido por Bruna em 09/11/2015
                if data == nil then
                    data = ""
                end
                --incluido por Bruna em 09/11/2015

                --incluido por Bruna em 09/11/2015
                if (data ~= "") then
                -- fim
                    for i=1, #data do

                        scrollViewSelected._data[#scrollViewSelected._data+1] = data[i]

                        local frameY = groupContent.contentHeight
                        if groupContent.numChildren == 0 then frameY = 0 end

                        local frame = require("module-frames").albumEntry(data[i], true, nil,scrollViewObj, isDVD, isSingle)
                        frame.y = frameY + 50/3
                        frame.x = CENTER_X - frame.contentWidth*.5
                        groupContent:insert(frame)

                        dicBarcodeToFrame[data[i].barcode] = frame
                        frame._scrollView = scrollViewObj
                        frame.isDVD = isDVD

                         if i<#data then

                            local lineY = groupContent.contentHeight + 20
                            local line = display.newLine(groupContent, frame.x,lineY, frame.x + frame.contentWidth ,lineY)
                            line.strokeWidth = 1
                            line:setStrokeColor( 207/255, 207/255, 207/255 )

                            -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
                            local invisibleRect = display.newRect(groupContent, frame.x,lineY, frame.contentWidth, 20)
                            invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
                            invisibleRect.alpha = 0
                        end

                    end

                    local scrollViewBackground = groupContent._scrollViewBackground

                    if scrollViewBackground == nil then
                        scrollViewBackground = display.newRect(CENTER_X, groupContent.y + groupContent.contentHeight*.5, groupContent.contentWidth, groupContent.contentHeight+20)
                        scrollViewBackground.anchorY = 0
                        scrollViewBackground.y = groupContent.y
                        scrollViewBackground:setFillColor(1,1,1)
                        scrollViewBackground.strokeWidth = 1
                        scrollViewBackground:setStrokeColor( 207/255, 207/255, 207/255 )
                        groupContent:insert(1, scrollViewBackground)
                        groupContent._scrollViewBackground = scrollViewBackground
                    else
                        scrollViewBackground.height = groupContent.contentHeight
                        scrollViewBackground.y = groupContent.y

                    end
                -- incluido por Bruna em 09/11/2015
                end
                -- fim
                return groupContent

            end

            local svContentSelected = createScrollViewContentGroup(data, scrollViewSelected, media == "dvd", media == "single")
            scrollViewSelected:insert(svContentSelected)
            scrollViewSelected._content = svContentSelected

            --incluido por Bruna em 09/11/2015
            if (data == nil) then
                data = ""
            end
            --fim
            addScrollViewFooter(#data > 0)


            transition.from(svContentSelected,{alpha=0, time=400})

            display.remove(sceneGroup.loading)
            sceneGroup.loading = nil

            if onFinish then
                onFinish()
            end

        end

        pageIndex = pageIndex + 1
        scrollViewSelected._pageIndex = pageIndex
        SERVER.getDiscography(scrollViewSelected._media, fetchQty, pageIndex, onCallBack)

    end


    sceneGroup.goToAlbum = function(goToBarcode)

        if dicBarcodeToFrame[goToBarcode] then

            local frame = dicBarcodeToFrame[goToBarcode]
            local scrollView = frame._scrollView

             local segmentNumber = 1
             if frame.isDVD then segmentNumber = 2
             elseif frame.isSingle then segmentNumber = 3 end

             if sceneGroup.navBar.segment[segmentNumber].isSelected ~= true then
                    sceneGroup.navBar.selectSegmentNumber(segmentNumber)
            end

            scrollView:scrollToPosition
            {
                y = -frame.y,
                time = 800,
                onComplete = function() end
            }
            sceneGroup:insert(scrollView)
        else
            print("album does not exist or was not load yet")
        end
    end
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
        adsHelper.showInterstitial()

        goToBarcode = event.params and event.params.scrollToBarcode

        if scrollViewSelected._pageIndex == 0 then
            sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y)
            sceneGroup:insert(sceneGroup.loading)
        end

        if goToBarcode and scrollViewSelected._pageIndex ~= 0 then
            sceneGroup.goToAlbum(goToBarcode)

        elseif scrollViewSelected._pageIndex == 0  then
            sceneGroup.getContent(
                function()
                    if goToBarcode == nil then return end
                    sceneGroup.goToAlbum(goToBarcode)
                end)
        end



    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
            _G.rbBack.addBack()

            _G.MENU.highlightMenuWithLabel("Discografia")

            -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
            local eventFlurry = "view;Discografia;ÁLBUNS;"
            print(eventFlurry)
            _G.analytics.logEvent(eventFlurry)
            -- fim

    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
        PLAYER.stop(true)

    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene

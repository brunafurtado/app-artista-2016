



-- PRECISA FAZER FACEBOOK API no SERVER

-- https://developers.facebook.com/docs/facebook-login/access-tokens

-- App tokens are considered insecure if your app is set to Native/Desktop in the Advanced settings of your App Dashboard and therefore will not work with API calls. This is because we assume that native or desktop apps will have the app secret embedded somewhere (and therefore the app token generated using that secret is not secure).

-- This call will return an app access token which can be used in place of a user access token to make API calls as noted above. Again, for security, app access token should never be hard-coded into client-side code, doing so would give everyone who loaded your webpage or decompiled your app full access to your app secret, and therefore the ability to modify your app. This implies that most of the time, you will be using app access tokens only in server to server calls.



-- "https://graph.facebook.com/v2.3/56381779049?fields=likes&access_token=<access_token>"

-- returns: {
--   "likes": 12053746,
--   "id": "209775982456374"
-- }


-- https://developers.facebook.com/tools/explorer/391660947598993/?method=GET&path=BrunaKarlaOficial&version=v2.4
-- "209775982456374?fields=likes"
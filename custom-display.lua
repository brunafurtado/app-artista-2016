
---------------------------
-- NEWTEXT2
---------------------------

local function newWord(options)

    local id = options.id
    local text = options.text
    local left = options.left
    local top = options.top
    local listener = options.listener
    local font = options.font
    local fontSize = options.fontSize
    local color = options.color or {1,1,1}
    local width = options.width -- maximum width for the word

    local group = display.newGroup()
    local wasSpit = false
    local remainingWord = nil

    local word = display.newText{parent=group, text=text, font=font, fontSize=fontSize}    
    while width and word.contentWidth > width do
        wasSpit = true
        local currText = word.text
        local ratio = width / word.contentWidth         
        local finalIndex = math.max(math.floor(currText:len()*ratio),1)        
        word.text = text:sub(1,finalIndex)
        remainingWord = text:sub(finalIndex+1, text:len())
    end
    


    word:setTextColor(unpack(color))
    word.x, word.y = word.contentWidth*.5, word.contentHeight*.5
    group.word = word
    word.id = id



    local wordBackground = display.newRect(word.x, word.y, word.contentWidth, word.contentHeight)
    wordBackground.alpha = .0
    --wordBackground.fill = {1,0,0}
    wordBackground.isHitTestable = true
    group:insert(1, wordBackground)
    group.background = wordBackground

    if listener then
        wordBackground:addEventListener( "tap", function()
            listener{target=word}
        end )

    end
    

    group.x = left
    group.y = top

    return group, wasSpit, remainingWord

end


-- this split is not working for pattern "."
function string:split( inSplitPattern, outResults )

   if not outResults then
      outResults = { }
   end
   local theStart = 1
   local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   while theSplitStart do
      table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
      theStart = theSplitEnd + 1
      theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   end
   table.insert( outResults, string.sub( self, theStart ) )
   return outResults
end



--local function newText(options). The returned object is left-top positioned. 
display.newText2 = function(options)

    local parent = options.parent
    local text = options.text
    local font = options.font
    local fontSize = options.fontSize
    local color = options.color
    local listener = options.listener
    local spaceBetweenWords = options.spaceBetweenWords or 2
    local spaceBetweenLines = options.spaceBetweenLines or 0
    local width = options.width or SCREEN_W*0.9
    local align = options.align

    if text == nil or type(text) ~= "string" then
        return print("error newText2 - text argument is nil or not a string")
    end

    local wordListener = function(e)
        if listener then
            listener(e)
        end

    end


    local groupText = display.newGroup()


    -- let's break the text into words
    local dicWordIndexesToCreateNewLines = {}
    local wordsPerExplicitNewLine = text:split("\n") -- splitting first the new lines

    local wordsTable = {}
    for i=1, #wordsPerExplicitNewLine do
        local wordsOnALine = wordsPerExplicitNewLine[i]:split(" ") -- splitting the spaces

        for j=1, #wordsOnALine do
            wordsTable[#wordsTable+1] = wordsOnALine[j]
            if i > 1 and j == 1 then
                dicWordIndexesToCreateNewLines[#wordsTable] = true            
            end
        end
    end

    

    -- create the words obj
    local words = {}
    -- for i=1, #wordsTable do
    --     words[i] = newWord{id=i, text=wordsTable[i], left=0, top=0, font=font, fontSize=fontSize, color=color, listener=wordListener}
    -- end

    local i = 1
    while i <= #wordsTable do
        local word, wasSpit, remainingWord = newWord{id=i, text=wordsTable[i], left=0, top=0, font=font, fontSize=fontSize, color=color, listener=wordListener, width=width}      
        words[i] = word
        i = i + 1
        if wasSpit then
            table.insert(wordsTable, i , remainingWord)
        end

    end


    
    local groupLines = {}
    groupLines[1] = display.newGroup()
    groupLines[1].x = 0
    groupLines[1].y = 0
    groupText:insert(groupLines[1])


    local function positionateWord(wordObj, index)

        local i = index or #words
        local currentLineWidth = (groupLines[#groupLines].numChildren == 0 and 0) or groupLines[#groupLines].contentWidth

        if dicWordIndexesToCreateNewLines[i] ~= true and currentLineWidth + spaceBetweenWords + wordObj.contentWidth <= width then            
            wordObj.x = currentLineWidth            
            if i > 1 then
                wordObj.x = currentLineWidth + spaceBetweenWords
            end

             groupLines[#groupLines]:insert(wordObj)             
             groupLines[#groupLines].x = 0

        else            
            groupLines[#groupLines+1] = display.newGroup()
            groupText:insert(groupLines[#groupLines])            
            groupLines[#groupLines].x = 0        
            groupLines[#groupLines].y = groupLines[#groupLines-1].y + groupLines[#groupLines-1].contentHeight + spaceBetweenLines
            groupLines[#groupLines]:insert(wordObj)
            
        end

    end

     -- positionate the words
     for i=1, #words do
        positionateWord(words[i], i)
     end      


     -- aligning the lines          
     if align then
        for i=1, #groupLines do
            if align == "center" then                
                groupLines[i].x = groupText.contentWidth*.5 - groupLines[i].contentWidth*.5                        
            elseif align == "right" then
                groupLines[i].x = groupText.contentWidth - groupLines[i].contentWidth
            end
         end
     end
     


    if parent then
        parent:insert(groupText)
    end

     ------------------------------------------
     -- PUBLIC FUNCTIONS
     ------------------------------------------

    groupText.getWordCount = function()
        return #words
    end


    groupText.addWord = function(newWordString)

        local nextIndex = #words+1
        words[nextIndex] = newWord{id=nextIndex, text=newWordString, left=0, top=0,  font=font, fontSize=fontSize, color=color, listener=wordListener}

        local heightBefore = groupText.contentHeight

        positionateWord(words[#words], #words)

        local heightAfter = groupText.contentHeight

        return (heightAfter - heightBefore)
    end

    groupText.setTextColor = function(obj, r,g,b,a)
        -- positionate the words
         for i=1, #words do
            words[i].word:setTextColor(r,g,b,a or 1)
         end    

    end

    groupText.getText = function()

        local text = words[1].word.text or ""
        for i=2, #words do
            text = text .. " " .. words[i].word.text
        end

        return text
        
    end





    return groupText

end





---------------------------
-- LOAD IMAGE
---------------------------




display.loadImage = function(group, imageInfo)
    

    if imageInfo == nil then
        imageInfo = group
        group = nil
        return display.newImageRect(imageInfo.path, imageInfo.width, imageInfo.height)
    end

    return display.newImageRect(group, imageInfo.path, imageInfo.width, imageInfo.height)
end


---------------------------
-- LOAD IMAGE FROM INTERNET
---------------------------


local hasAlreadyRetried = {} -- table that will store the download retries made, in order to allow only 1 retry 
display.loadImageFromInternet = function(group, imageObj, showOnlyPlaceholder, doNotShowPlaceholder, showSpinner)

    local composer = require("composer");
    local currentScene = composer.getSceneName( "current" )


    if imageObj == nil then
        imageObj = group
        group = nil
    end

    local url = imageObj.url
    local destFilename
    
    if url == nil then
        showOnlyPlaceholder = true
        print("ATTENTION: NO IMAGE URL SPECIFIED!!")
    else
        destFilename = url:match( "([^/]+)$" )

        local imgFilename = destFilename                            
        local imgExtension = imgFilename:sub(#imgFilename - 3)
        if imgExtension:sub(1,1) == "." then imgExtension = imgExtension:sub(2) end  

        -- let's use MD5 to create a unique filename based on the URL
        destFilename = require( "crypto" ).digest( crypto.md5, url ) .. "." .. imgExtension
        
    end
    
    local imageWidth = imageObj.width
    local imageHeight = imageObj.height

    if imageWidth == nil then
        print("ATTENTION: NO IMAGE WIDTH SPECIFIED!!")
        imageWidth = 40
    end
    if imageHeight == nil then
        print("ATTENTION: NO IMAGE HEIGHT SPECIFIED!!")
        imageHeight = 40
    end

    -- let's try to load the image locally
    local image 
    if destFilename then
        image = display.newImage(destFilename, system.TemporaryDirectory)    
    end
    if image then
        --print("imaged already available locally")
        image:scale(imageWidth/image.contentWidth,imageHeight/image.contentHeight)
        if group then
            group:insert(image)
        end        
        return image
    end


    -- image is not available locally, let's create a temporary placeholder for the image
    local groupPlaceholder = display.newGroup()
    groupPlaceholder.anchorChildren = true
    groupPlaceholder.anchorX, groupPlaceholder.anchorY = .5, .5
    local placeholder = display.newRect(groupPlaceholder, imageWidth*.5,imageHeight*.5, imageWidth, imageHeight )    
    placeholder:setFillColor( .5,.5,.5,.5 )
    if doNotShowPlaceholder then
        placeholder.alpha = 0
    end

    if showOnlyPlaceholder then
        if group then
            group:insert(groupPlaceholder)
        end
        return groupPlaceholder
    end

    if showSpinner then
        local loading = AUX.showLoadingAnimation(placeholder.x, placeholder.y)   
        groupPlaceholder:insert(loading)
    end


    local function networkListener( event )
        display.remove(loading)
        if ( event.isError ) then
            print ( "Network error - download failed" )
            -- retrying the download
            if hasAlreadyRetried[destFilename] == nil then
                hasAlreadyRetried[destFilename] = true
                timer.performWithDelay( 100, function()
                    display.loadRemoteImage( url, "GET", networkListener, destFilename, system.TemporaryDirectory, placeholder.x, placeholder.y )
                end)
            end
        else

            local downloadImage = event.target
            downloadImage.alpha = 0
            if groupPlaceholder == nil or groupPlaceholder.contentWidth == nil or currentScene ~= composer.getSceneName( "current" ) then
                -- image should not be in screen anymore
                display.remove(downloadImage)
                return
            end

            local finalW, finalH = groupPlaceholder.contentWidth, groupPlaceholder.contentHeight
            local scaleFactorW = finalW / downloadImage.contentWidth
            local scaleFactorH = finalH/downloadImage.contentHeight
            local isPlaceholderHorizontal = (groupPlaceholder.contentWidth / groupPlaceholder.contentHeight) > 1
            local isImageHorizontal = (downloadImage.contentWidth / downloadImage.contentHeight) > 1
            if isPlaceholderHorizontal ~= isImageHorizontal then
                print("ATTENTION: image width/height informed appears to be inverted!")            
                scaleFactorH = math.min(groupPlaceholder.contentWidth, groupPlaceholder.contentHeight) / math.max(downloadImage.contentWidth, downloadImage.contentHeight)
                scaleFactorW = scaleFactorH
            end

            downloadImage:scale(scaleFactorW, scaleFactorH)
            downloadImage.anchorX, downloadImage.anchorY = groupPlaceholder.anchorX, groupPlaceholder.anchorY
            downloadImage.x, downloadImage.y = groupPlaceholder.x, groupPlaceholder.y            
            
            group = group or (groupPlaceholder and groupPlaceholder.parent)
            if group then
                group:insert(downloadImage)
            end
            transition.to( downloadImage, { alpha = 1.0, onComplete=function() display.remove(groupPlaceholder) end } )
        end

        --print ( "event.response.fullPath: ", event.response.fullPath )
        --print ( "event.response.filename: ", event.response.filename )
        --print ( "event.response.baseDirectory: ", event.response.baseDirectory )
    end


    --display.loadRemoteImage( url, "GET", listener [, params], destFilename [, baseDir] [, x, y] )
    display.loadRemoteImage( url, "GET", networkListener, destFilename, system.TemporaryDirectory, placeholder.x, placeholder.y )
    
    if group then
        group:insert(groupPlaceholder)
    end

    return groupPlaceholder

end

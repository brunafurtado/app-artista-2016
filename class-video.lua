local video = {}


	video.new = function(params)

		local obj = {}

		obj.date = params.date
		obj.image = params.image  -- image={url=, width, height=}
		obj.title = params.title
		obj.subTitle = params.subTitle
		obj.videoURL = params.videoURL

		return obj

	end


return video
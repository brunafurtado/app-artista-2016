local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------
local tableView = nil
local libSlideView


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.icons

    local background = display.newRect(sceneGroup, CENTER_X, CENTER_Y, SCREEN_W, SCREEN_H)
    background.fill = {0,0,0}

    local function createFooter()

        local group = display.newGroup()
        sceneGroup:insert(group)

        local footer = display.newRect(group, CENTER_X, SCREEN_H - 25, SCREEN_W, 50)
        footer:setFillColor( 5/255, 15/255, 20/255, .9 )


        local btMore = require("library-widgets").newButton{

            left = 0,
            y = footer.y,

            height=footer.contentHeight,
                width = APP_CONFIG.images.icons["more"].width*2,
                
            imageFile = APP_CONFIG.images.icons["more"].path,
            imageWidth = APP_CONFIG.images.icons["more"].width,
            imageHeight = APP_CONFIG.images.icons["more"].height,
            --imagePosition = "left",
            --imagePadding = {right=4, bottom=2},

            imageColor = {1,1,1},
            imageOverColor = {1,1,1,.7},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },
            
            onRelease = function() 
                
                    local buttonsLabels = {
                        "SALVAR IMAGEM",
                        --"ENVIAR POR EMAIL", 
                        --"ENVIAR POR MENSAGEM", 
                        --"SALVAR COMO FUNDO DE TELA",
                    }

                    local popup = require("module-popups").show("buttons", 
                            buttonsLabels,
                            function(e) 
                                print(e.target.id, e.target.label)
                                local label = e.target.label                                

                                if label == buttonsLabels[1] then
                                    media.save(sceneGroup.imageFilename, system.TemporaryDirectory)
                                    timer.performWithDelay( 100, function()
                                        native.showAlert("Aplicativo " .. APP_CONFIG.appName, "A foto foi salva!", { "Ok" } )
                                        end)

                                elseif label == buttonsLabels[2] then                                   

                                    local options =
                                    {
                                       to = "",
                                       subject = "",
                                       body = "",
                                       attachment = { baseDir=system.TemporaryDirectory, filename=sceneGroup.imageFilename, type="image/"..imgExtension },
                                    }
                                    native.showPopup( "mail", options  )                            

                                end

                            end )            

            end
        }
        group:insert(btMore)

        -- Adicionado por Bruna em 23/10/2015 
        -- nome do app cujo item será compartilhado
        local shareTo = "Compartilhar"
        local photoIndex = event.params.photoIndex
        local btShare = require("custom-widget").newShareButton{
            parent = group,
            right = SCREEN_W - 9,
            y = footer.y,
            shareImageFilename = function() print("on func=",sceneGroup.imageFilename); return sceneGroup.imageFilename; end,
            shareMessage = "Olha que foto legal!",
            flurry = "outClick;Fotos;Fotos;Compartilhar;" .. shareTo .. ";" .. event.params.albumObj.title .. ";foto_" .. photoIndex .. ";",
        }

    end


    createFooter()


    local btW = 60
    local btClose = require("widget").newButton{
        x = SCREEN_W - 10 - btW*.5,
        y = 40,
        width = btW,
        height = 20,
        label = "FECHAR",
        fillColor = {default={25/255,35/255,40/255,1}, over={25/255,35/255,40/255,1}},
        labelColor = {default={1,1,1,1}, over={1,1,1,.7}},
        font=APP_CONFIG.fontMedium,
        fontSize = 10,
        shape="rect",
        strokeWidth = 1,
        strokeColor = {default={1,1,1,1}, over={1,1,1,.7}},
        onRelease = event.params.closeOverlay

    }
    sceneGroup:insert(btClose)


    libSlideView = require "lib-slideViewImages"




end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

        display.setStatusBar( display.HiddenStatusBar )


        display.remove(sceneGroup.slideView)

        local photos 
        local photoIndex
        print(event.params)
        print(event.params and event.params.albumObj)
        print(event.params and event.params.albumObj and event.params.albumObj.images)
        if event.params and event.params.albumObj and event.params.albumObj.images then
            photos = event.params.albumObj.images
            photoIndex = event.params.photoIndex

            local function createGroup(e)
                --local imageFilename = myImages[e.index]
                --return display.newImage(imageFilename)
                print("on createGroup, index=", e.index)
                local imageObj = photos[e.index].full
                local photo = display.loadImageFromInternet(imageObj, nil, nil, nil, true)
                local scaleF = math.min(SCREEN_W / photo.contentWidth, SCREEN_H / photo.contentHeight)
                photo:scale(scaleF, scaleF)
                --photo.x, photo.y = CENTER_X, CENTER_Y
                
                return photo

            end
            
            -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
            local eventFlurry = "view;Fotos;Fotos;" .. event.params.albumObj.title .. ";foto_" .. photoIndex .. ";"
            print(eventFlurry)
            _G.analytics.logEvent(eventFlurry)
            -- fim

            sceneGroup.slideView = libSlideView.new{  
                imageSet = photos,
                columnRender = createGroup,
                numColumns = #photos,
                initialIndex = photoIndex,
                onShow = function(e)
                    -- saving the image filename for use when sharing this photo
                    local imageURL = photos[e.index].full.url
                    local index =   e.index                        
                    sceneGroup.imageFilename = AUX.getFilenameFromURL(imageURL)
                end
            }
            sceneGroup:insert(2, sceneGroup.slideView )
            print("added slideview")
            
        end

        



        -- -- showing the photo
        -- local photo = display.loadImageFromInternet(event.params.image, nil, nil, nil, true)
        -- local scaleF = math.min(SCREEN_W / photo.contentWidth, SCREEN_H / photo.contentHeight)
        -- photo:scale(scaleF, scaleF)
        -- photo.x, photo.y = CENTER_X, CENTER_Y
        -- sceneGroup:insert(2, photo)

        -- saving the image filename for use when sharing this photo
        --local imageURL = event.params.image.url                
        --sceneGroup.imageFilename = AUX.getFilenameFromURL(imageURL)





    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.

        display.setStatusBar( display.TranslucentStatusBar )     
        sceneGroup.slideView:destroy()   
        
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
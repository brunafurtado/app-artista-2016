
local storage = {}


-------------
storage.save = function(data)
    if isEncoded == nil then
        isEncoded = storage.useEncode 
    end
    
    local path = system.pathForFile("sobre.ini", system.DocumentsDirectory)
    local fh = io.open(path, "w")
    
    fh:write(data)
    fh:close()
  --print("closing file")
end



storage.load = function() 

    local path = system.pathForFile("sobre.ini", system.DocumentsDirectory)
    
    local fh, reason = io.open(path, "r")
    
    local returnData = nil

    if fh then
        -- read all contents of file into a string
        returnData = fh:read("*a")        
            
        fh:close()
    end

    return returnData
end



return storage

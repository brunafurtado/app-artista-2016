local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

-- Inserido por Bruna em 26/10/2015 para enviar o evento para o Flurry
local platform = system.getInfo( "platformName" )
--print(platform)


local parceiro

if _G.DEVICE.isApple then

    parceiro = "iTunes"

elseif _G.DEVICE.isGoogle then

    parceiro = "Google Play"       

elseif _G.DEVICE.isKindleFire then

    parceiro = "Amazon"           

elseif platform == "WinPhone" then

    parceiro = "Windows Phone"

else

    -- any other DEVICE (windows, html,..) will end up here

end

-- fim


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.first


    local background = display.newImageRect(sceneGroup, "images/photos/background.png", 320,570)
    --local background = display.loadImageFromInternet(sceneGroup, _G.FEATURED_ALBUM.getImageObj())
    local scaleFactor = math.max(SCREEN_W/background.contentWidth, SCREEN_H/background.contentHeight)
    background:scale(scaleFactor, scaleFactor)
    background.x, background.y = CENTER_X, CENTER_Y
    

    -- group that will hold the main info of the screen (artist/album name + buttons)
    local groupInfo = display.newGroup()
    sceneGroup:insert(groupInfo)
    groupInfo.y = CENTER_Y
    groupInfo.x = 40


    local lbFirstName = display.newText{parent=groupInfo, text=APP_CONFIG.artistName.first, font=APP_CONFIG.fontLarge, fontSize=60}
    lbFirstName.anchorX,lbFirstName.anchorY = 0,0
    lbFirstName.y = 0 --CENTER_Y
    lbFirstName.x = 0 --40

    local lbLastName = display.newText{parent=groupInfo, text=APP_CONFIG.artistName.last, font=APP_CONFIG.fontLarge, fontSize=60}
    lbLastName.anchorX, lbLastName.anchorY = 0, 0
    lbLastName.x = lbFirstName.x
    lbLastName.y = lbFirstName.y + lbFirstName.contentHeight*0.8

    local widthLbAlbum = SCREEN_W - 10
    local lbAlbumTitle = display.newText{parent=groupInfo, text=_G.FEATURED_ALBUM.title, font=APP_CONFIG.fontLight, fontSize=20, width=widthLbAlbum}
    --local lbAlbumTitle = display.newText{parent=groupInfo, text=_G.FEATURED_ALBUM.title, font=APP_CONFIG.fontLight, fontSize=20}
    lbAlbumTitle.anchorX, lbAlbumTitle.anchorY = 0, 0
    lbAlbumTitle.x = lbFirstName.x
    lbAlbumTitle.y = lbLastName.y + lbLastName.contentHeight*0.85


    local function onButtonRelease(e)
        
        if e.target.id == 1 then
            local url = _G.FEATURED_ALBUM.buyDigitalAlbumURL            
            if url == nil or url == "" then
                url = _G.FEATURED_ALBUM.buyDiscURL
                parceiro = "MK Shopping"
            end
            -- incluido por Bruna em 19/10/2015 
            local eventFlurry = "outClick;Tela Inicial;Tela Inicial;Comprar Álbum;" .. parceiro .. ";" .. _G.FEATURED_ALBUM.title .. ";"
            print(eventFlurry)
            _G.analytics.logEvent(eventFlurry)
            -- fim
            system.openURL(url)

        elseif e.target.id == 2 then            

            composer.gotoScene( "scene-discography", {effect="fade",
                time=400,
                params={scrollToBarcode=_G.FEATURED_ALBUM.barcode}
                })
            -- incluido por Bruna em 19/10/2015 
            local eventFlurry = "view;Tela Inicial;Tela Inicial;Músicas;" .. _G.FEATURED_ALBUM.title .. ";"
            print(eventFlurry)
            _G.analytics.logEvent(eventFlurry)
            -- fim

        end

    end
    
 
    local btBuyAlbum = require("library-widgets").newButton{
        id=1,
        left = lbFirstName.x + 5,
        top = lbAlbumTitle.y + lbAlbumTitle.contentHeight + 20, --1436*(320/1080)* _G.GROW_WITH_SCREEN_H,
        width = 380*(320/1080), height=90*(320/1080),
        
        label = "COMPRAR ÁLBUM",
        labelColor = { 1, 1, 1 }, 
        labelOverColor = { 0, 0, 0, 0.5 },
        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 10,

        backgroundColor = { 31/255, 148/255, 241/255, 1 }, 
        backgroundOverColor ={ 31/255, 148/255, 241/255, 0.6 },

        strokeWidth = 1,
        strokeColor = { 31/255, 148/255, 241/255, 1 },
        strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },
        
        onRelease=onButtonRelease
    }
    
    groupInfo:insert(btBuyAlbum)    
    if (_G.FEATURED_ALBUM.buyDigitalAlbumURL == nil and G.FEATURED_ALBUM.buyDiscURL == nil) or (_G.FEATURED_ALBUM.buyDigitalAlbumURL == "" and _G.FEATURED_ALBUM.buyDiscURL == "") then
        btBuyAlbum.isVisible = false
    end


    local btMoreMusic = require("library-widgets").newButton{
        id=2,
        x = btBuyAlbum.x + btBuyAlbum.contentWidth + 6,
        y = btBuyAlbum.y,
        width=btBuyAlbum.width, height=btBuyAlbum.height,
        
        label = "MÚSICAS",
        labelColor = { 1, 1, 1 }, 
        labelOverColor = { 0, 0, 0, 0.5 },

        
        backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
        backgroundOverColor = { 31/255, 148/255, 241/255, 0 },        
        backgroundStrokeWidth = 1,
        backgroundStrokeColor = { 1, 1, 1 }, 
        backgroundStrokeOverColor={ 0, 0, 0, 0.5 },

        labelFont=APP_CONFIG["fontMedium"],
        labelFontSize=10,
        
        onRelease=onButtonRelease
    }
    groupInfo:insert(btMoreMusic)


    local iconExclusiveArtist = display.loadImage(sceneGroup, sceneAssets.iconExclusive)
    iconExclusiveArtist.x = 142/3
    iconExclusiveArtist.y = _G.SCREEN_H - 130/3
    

    local iconLogoMK = display.loadImage(sceneGroup, sceneAssets.logo)
    iconLogoMK.x = _G.SCREEN_W - 142/3
    iconLogoMK.y = _G.SCREEN_H - 130/3


    -- ensuring that the groupInfo is not above the icons
    local iconTopY = math.min(iconExclusiveArtist.y - iconExclusiveArtist.contentHeight*.5, iconLogoMK.y - iconLogoMK.contentHeight*.5)
    local groupInfoBottomY = groupInfo.y + groupInfo.contentHeight
    if groupInfoBottomY + 5 > iconTopY then
        groupInfo.y = groupInfo.y - (groupInfoBottomY - iconTopY) - 20

    end



    -- creating the button menu 
    local btMenu = require("library-widgets").newButton{
        left = 0,
        y = SCREEN_H*0.1, --_G.TOP_Y_AFTER_STATUS_BAR,
        width=sceneAssets.btMenu.width,
        height=sceneAssets.btMenu.height*7,
        
        label = "",
        labelColor = { 1, 1, 1 }, 
        labelOverColor = { 0, 0, 0, 0.5 },

        imageFile = sceneAssets.btMenu.path,
        imageWidth = sceneAssets.btMenu.width,
        imageHeight = sceneAssets.btMenu.height,

        imageColor = {0,0,0},
        imageOverColor = {0,0,0,.5},

        imagePadding = {left=12},

        backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
        backgroundOverColor = { 31/255, 148/255, 241/255, 0 },        
        --backgroundStrokeWidth = 1,
        --backgroundStrokeColor = { 1, 1, 1 }, 
        --backgroundStrokeOverColor={ 0, 0, 0, 0.5 },

        labelFont=APP_CONFIG["fontMedium"],
        labelFontSize=10,
        
        onRelease= function(e) 

            if e.phase == "ended" then
                print("open lateral menu")
                -- incluido por Bruna em 19/10/2015 
                local eventFlurry = "view;Menu;Menu;"
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                _G.MENU.show()

            end
        
        end
    }
    sceneGroup:insert(btMenu)

end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        --display.setStatusBar( display.TranslucentStatusBar )
        --_G.statusBarBackground.isVisible = true
        display.getCurrentStage():insert(_G.statusBarBackground)


        _G.MENU.highlightMenuWithLabel(nil)
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
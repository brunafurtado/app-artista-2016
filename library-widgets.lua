local cw = {}


cw.newProgressBar = function(options)

	-- receiving params
	local parent = options.parent
	local x = options.x
	local y = options.y
	local top = options.top
	local left = options.left
	local imageBackground = options.imageBackground  -- {path, width, height}
	local imageFill = options.imageFill 		 -- {path, width, height}
	local duration = options.duration or 1000 -- duration (in ms) to fill

	local group = display.newGroup()
	group.anchorChildren = true


	-- creates the background / outter image of the progress view	
	local background = display.loadImage(group, imageBackground)
	background.x, background.y = background.contentWidth*.5, background.contentHeight*.5	
	local fill = display.loadImage(imageFill)

	local widthMax = fill.contentWidth

 	-- Create a container with the fill image
    local containerFill = display.newContainer( widthMax, fill.contentHeight )
    containerFill.anchorX = 0
    containerFill.anchorY = 0.5
    containerFill.anchorChildren = false
    containerFill:insert(fill, true)  
    containerFill.x = background.x - background.contentWidth*.5 + (background.contentWidth - fill.contentWidth)*.5
    containerFill.y = background.y
    group:insert(containerFill)
    fill.x = fill.contentWidth*.5
    
    -- starting the fill with 0 width ("empty")
    containerFill.width = 0

	
	group.y = y or (top + group.contentHeight*.5)
	group.x = x or (left + group.contentWidth*.5)


    if parent then
    	parent:insert(group)
    end



    group.setProgress = function(percentValue)

		if percentValue > 1 then percentValue = 1 end

    	local finalWidth = percentValue * widthMax

    	-- canceling any previous transition
    	transition.cancel(containerFill)

    	-- filling the progress bar
		transition.to(containerFill, {width=finalWidth, time=duration})

	end

    
    
    group.getProgress = function(percentValue)

    	return containerFill.width / maxWidth
    	
	end


	return group

end


cw.newArrowButton = function(options)

    local left = options.left
    local right = options.right
    local top = options.top
    local y = options.y
    local height = options.height
    local text = options.label or "VOLTAR"
    local textColor = options.labelColor or {0,0,0}
    local textOverColor = options.labelOverColor or {.3,.3,.3}
    local font = options.font or APP_CONFIG.fontLight
    local fontSize = options.fontSize or 14
    local fillColor = options.fillColor
    local arrowFillColor = options.arrowFillColor or {0,0,0}
    local arrowOverColor = options.arrowOverColor or {.3,.3,.3}
    local onTap = options.onTap

    local button = display.newGroup() -- this has a left, top reference
    button.id = id




    local arrow = display.newImageRect(button, "images/icon_arrow@3x.png",74/5,55/5)
    arrow:setFillColor(unpack(arrowFillColor))
    arrow.x = arrow.contentWidth*.5
    arrow.y = arrow.contentHeight*.5
    button.arrow = arrow

    local label = display.newText{parent=button, text=text, x=arrow.x + arrow.contentWidth*0.5 + 4, y=arrow.y, font=font, fontSize=fontSize}
    label.anchorX = 0
    label.y=math.max(arrow.y, label.contentHeight*.5)
    label:setFillColor( unpack(textColor) )
    button.label = label
    arrow.y = label.y


    if text ~= "VOLTAR" then
        arrow:scale(-1,1)
        label.x = 0
        arrow.x = label.x + label.contentWidth + arrow.contentWidth*0.5 + 4    
    end
    
    


    -- background
    local w,h = button.contentWidth, math.max(button.contentHeight, height)
    local background = display.newRect(w*.5, h*.5, w, h)
    background:setFillColor(unpack(fillColor))
    background.isHitTestable = true
    button:insert(background)
    button.background = background

    -- repositioning everyone
    label.y = background.y
    arrow.y = background.y



    local function onButtonTap(e)
        if e.numTaps ~= 1 then return end


            label:setFillColor(unpack(textOverColor))
            arrow:setFillColor(unpack(arrowOverColor))


        timer.performWithDelay( 50, function()
            label:setFillColor(unpack(textColor))
            arrow:setFillColor(unpack(arrowFillColor))
         

            onTap(e)
        end
        )
        

    end
    
    if onTap then
        button:addEventListener("tap", onButtonTap)
    end

    button.anchorChildren = true

    if left then
        button.x = left + button.contentWidth*.5
    else
        button.x = right - button.contentWidth*.5
    end   
        button.y = y or (top + button.contentHeight*.5)

    return button


end

-- button that responds to tap. Don't have overlay status
cw.newTapButton = function(options)
    local parent = options.parent
    local id = options.id
    local w = options.width or options.w
    local h = options.height or options.h
    local left = options.left
    local x = options.x
    local right = options.right
    local top = options.top 
    local y = options.y
    local bottom = options.bottom


    local fillColor = options.fillColor or {1,1,1}  -- background color

    local hasDisclosureIndicator = options.hasDisclosureIndicator or false
    local disclosureIndicatorFillColor = options.disclosureIndicatorFillColor or {180/255,180/255,180/255}
    local disclosureIndicatorOverFillColor = options.disclosureIndicatorOverFillColor or {180/255,180/255,180/255}
    local disclosureIndicatorScaleX = options.disclosureIndicatorScaleX or 1
    local disclosureIndicatorScaleY = options.disclosureIndicatorScaleY or 1

    local text = options.label    
    local textOffsetY = options.labelOffsetY or 0
    local textOffsetX = options.labelOffsetX or 0
    local textColor = options.labelColor or {0,0,0}
    local textOverColor = options.labelOverColor or {.3,.3,.3}
    local textPadding = options.labelPadding or 10
    local align = options.align 
    local subText = options.subLabel -- optional labels that appears on the right side, before the indicator
    local font = options.font or APP_CONFIG.fontLight
    local fontSize = options.fontSize or 14
    local strokeColor = options.strokeColor or {0,0,0}
    local strokeWidth = options.strokeWidth or 0
    
    local imageObj = options.imageObj
    local icon = options.icon or imageObj or {}    
    local imageFilename = options.imageFilename or icon.filename or icon.path
    local imageWidth = options.imageWidth or icon.width
    local imageHeight = options.imageHeight or icon.height
    local imageTopPos = options.imageTop or icon.top  -- top position relative to the button
    local imageLeftPos = options.imageLeft or icon.left  -- top position relative to the button
    local imageFillColor = options.imageFillColor or {1,1,1}
    local imageOverFillColor = options.imageOverFillColor
    
    local onTap = options.onTap
    
    
    local button = display.newGroup() -- this has a left, top reference
    button.id = id

    -- background
    local background = display.newRect(button, w*.5, h*.5, w, h)
    background:setFillColor(unpack(fillColor))
    background.isHitTestable = true
    button.background = background
    
    
    background.strokeWidth = strokeWidth
    background:setStrokeColor(unpack(strokeColor))    
    
    
    -- disclosure indicator
    if hasDisclosureIndicator then
        local disclosureIndicator = display.newImageRect(button, "images/icon_arrow@3x.png",74/5,55/5)
        disclosureIndicator:setFillColor(unpack(disclosureIndicatorFillColor))
        disclosureIndicator:scale(disclosureIndicatorScaleX, disclosureIndicatorScaleY)
        disclosureIndicator.x = background.x + background.contentWidth*0.5 - disclosureIndicator.contentWidth*0.5 - 5
        disclosureIndicator.y = background.y
        button.disclosureIndicator = disclosureIndicator
    end

    -- text
    local labelX = background.x + textOffsetX
    local sublLabelRightX = background.x + background.contentWidth*.5 - 5
    if hasDisclosureIndicator then
        labelX = labelX - 10*.5
    end
    if text then
        
        local textWidth = background.contentWidth - textPadding
        if hasDisclosureIndicator then
            textWidth = textWidth - 20             
            sublLabelRightX = button.disclosureIndicator.x - button.disclosureIndicator.contentWidth*.5 - 15
        end
        
        if subText then
            local subLabel = display.newText{parent=button, text=subText, x=sublLabelRightX, y=background.y + textOffsetY, font=font, fontSize=fontSize}            
            subLabel.x = sublLabelRightX - subLabel.contentWidth*.5
            subLabel:setFillColor( unpack(textColor) )
            button.subLabel = subLabel
        end

        local labelWidth = nil
        if align then
            labelWidth = textWidth
        end

        local label = display.newText{parent=button, text=text, x=labelX, y=background.y + textOffsetY, font=font, fontSize=fontSize, align = align, width=labelWidth}
        label:setFillColor( unpack(textColor) )
        button.label = label

        
        
        _G.AUX.adjustTextSize(label,textWidth)
        background.label = label
    end
    
    -- image
    if imageFilename then
        local image = display.newImageRect(button, imageFilename,imageWidth, imageHeight)
        if imageFillColor then
            image:setFillColor(unpack(imageFillColor))
        end
        image.x = background.x
        image.y = background.y
        if imageTopPos then
            image.y = imageTopPos + image.contentHeight*.5
        end
        if imageLeftPos then
            image.x= imageLeftPos + image.contentWidth*.5
        end
        button.image = image
    end

    local function onButtonTap(e)
        if e.numTaps ~= 1 then return end

        if imageOverFillColor and button.image then
            button.image:setFillColor(unpack(imageOverFillColor))
        end  
        
        if textOverColor then
            if button.label then
                button.label:setFillColor(unpack(textOverColor))
            end
            
            if button.subLabel then
                button.subLabel:setFillColor(unpack(textOverColor))
            end
        end  

        if disclosureIndicatorOverFillColor and button.disclosureIndicator then
            button.disclosureIndicator:setFillColor(unpack(disclosureIndicatorOverFillColor))
        end

        timer.performWithDelay( 50, function()
            if imageOverFillColor and button.image then
                button.image:setFillColor(unpack(imageFillColor))
            end
            if button.label then
                button.label:setFillColor(unpack(textColor))
            end
            
            if button.subLabel then
                button.subLabel:setFillColor(unpack(textColor))
            end

            if disclosureIndicatorOverFillColor and button.disclosureIndicator then
                button.disclosureIndicator:setFillColor(unpack(disclosureIndicatorFillColor))
            end

            onTap(e)
        end
        )
        

    end
    
    if onTap then
        button:addEventListener("tap", onButtonTap)
    end

    button.anchorChildren = true

    -- sets the button position
    button.x = x or (left and (left + w*.5)) or (right and (right - w*.5))
    button.y = y or (top and (top + h*.5)) or (bottom and (bottom - h*.5))

    if parent then
        parent:insert(button)
    end
    
    return button

end



-- this should be the ultimate button function - v6
cw.newButton = function(options)

    -- receiving options

    local id = options.id

    -- button position    
    local top = options.top
    local y = options.y
    local bottom = options.bottom
    local left = options.left
    local x = options.x
    local right = options.right


    -- button background
    local width = options.width or 0
    local height = options.height or 0
    local backgroundColor = options.backgroundColor or {1,1,1}
    local backgroundOverColor = options.backgroundOverColor
    local backgroundStrokeWidth = options.backgroundStrokeWidth or 0
    local backgroundStrokeColor = options.backgroundStrokeColor or {0,0,0,0}
    local backgroundStrokeOverColor = options.backgroundStrokeOverColor or {0,0,0,0}

    -- button label
    local labelString = options.label
    local labelFont = options.labelFont or native.systemFont
    local labelFontSize = options.labelFontSize or 12
    local labelColor = options.labelColor or { 1,1,1 }
    local labelOverColor = options.labelOverColor or { 1,1,1 }
    local labelPadding = options.labelPadding or {left=0, top=0, bottom=0, right=0}
    local labelTruncate = options.labelTruncate or false
    local labelWrap = options.labelWrap or false
    local labelAlign = options.labelAlign or "left"
    local labelLineSpacing = options.labelLineSpacing


    -- button image
    local imageFile = options.imageFile
    local imageOverFile = options.imageOverFile
    local imageColor = options.imageColor 
    local imageOverColor = options.imageOverColor
    local imageWidth = options.imageWidth
    local imageHeight = options.imageHeight
    local imagePadding = options.imagePadding or {left=0, top=0, bottom=0, right=0}
    local imagePosition = options.imagePosition or options.imagePos or "left"  -- "left", "right" "center", "top", "bottom"
    local imageScaleX = options.imageScaleX or 1
    local imageScaleY = options.imageScaleY or 1

    -- listener
    local onRelease = options.onRelease
    local onEvent = options.onEvent
    local onTap = options.onTap

    

    -- making sure table values have all values filled out
    labelPadding.left = labelPadding.left or 0
    labelPadding.right = labelPadding.right or 0
    labelPadding.top = labelPadding.top or 0
    labelPadding.bottom = labelPadding.bottom or 0

    imagePadding.left = imagePadding.left or 0
    imagePadding.right = imagePadding.right or 0
    imagePadding.top = imagePadding.top or 0
    imagePadding.bottom = imagePadding.bottom or 0




    local button = display.newGroup()   -- button group
    button.anchorChildren = true

    
    button.id = id



    local image = nil
    local imageOver = nil
    local label = nil


    local view = display.newGroup()     -- group that hold the label and image

    -- sets the appropriatecolor accordingly to the button status
    local function setStatus(isPressed)
        if isPressed == button.isPressed then return end

        local newBackgroundColor = backgroundColor
        local newBackgroundStrokeColor = backgroundStrokeColor
        local newLabelColor = labelColor
        local newImageColor = imageColor


        if isPressed then
            newBackgroundColor = backgroundOverColor
            newBackgroundStrokeColor = backgroundStrokeOverColor
            newLabelColor = labelOverColor
            newImageColor = imageOverColor            
        end

        if newBackgroundColor then
            button.background:setFillColor( unpack(newBackgroundColor) )
        end
        if newLabelColor and button.label then
            button.label:setTextColor( unpack(newLabelColor) )
        end
        if newImageColor and button.image then
            button.image:setFillColor( unpack(newImageColor) )
        end        
        if backgroundStrokeOverColor then
            button.background:setStrokeColor( unpack(newBackgroundStrokeColor) )
        end
        if button.imageOver then
            button.image.isVisible = not isPressed
            button.imageOver.isVisible = isPressed
        end
        

        button.isPressed = isPressed
    end



    -- creating the objects

    -- loads the image
    if imageFile then
        image = display.newImageRect(view, imageFile, imageWidth, imageHeight)
        image:scale(imageScaleX, imageScaleY)
        if imageColor then
            image:setFillColor( unpack(imageColor) )
        end        
        button.image = image
    end
    if imageOverFile then
        imageOver = display.newImageRect(view, imageOverFile, imageWidth, imageHeight)
        imageOver:scale(imageScaleX, imageScaleY)
        if imageOverColor then
            imageOver:setFillColor( unpack(imageOverColor) )
        end        
        button.imageOver = imageOver
        imageOver.isVisible = false

    end
    

    -- creates the label
    if labelString then
        local labelMaxWidth = nil
        if width then
            labelMaxWidth = width - labelPadding.left - labelPadding.right
            
            if imageWidth then
                if imagePosition == "left" or imagePosition == "right" then
                    labelMaxWidth = labelMaxWidth - image.width - imagePadding.left
                end
            end        

        end
        local labelWidth = nil 
        if labelWrap and not labelTruncate then
            labelWidth = labelMaxWidth
        end
        if labelLineSpacing then
            label = display.newText2{parent=view, text=labelString, font=labelFont, fontSize=labelFontSize, width=labelWidth, align=labelAlign, color=labelColor}
            label.anchorChildren = true
            label.anchorX = .5
            label.anchorY = .5
        else
            label = display.newText{parent=view, text=labelString, font=labelFont, fontSize=labelFontSize, width=labelWidth, align=labelAlign}
            label:setTextColor( unpack(labelColor) )
        end                
        button.label = label
        
        if labelTruncate then
            if _G.AUX == nil or _G.AUX.adjustTextSize == nil then
                error("Function newButton: Please load aux library with adjustTextSize") 
            end
            if labelLineSpacing then
                print("Function newButton: labelTruncate cannot be used when labelLineSpacing is set load aux library with adjustTextSize")
            else
                _G.AUX.adjustTextSize(label, labelMaxWidth)
            end
            
        end    
    end

    -- positionate the objects
    local buttonW = nil
    local buttonH = nil

    if label == nil then

        buttonW = imagePadding.left + image.contentWidth + imagePadding.right       
        buttonH = math.max(height, image.contentHeight)

        image.x = image.contentWidth*.5 + imagePadding.left - imagePadding.right
        image.y = buttonH*.5 + (imagePadding.top or 0 ) - (imagePadding.bottom or 0)

    elseif image == nil then
        label.x = labelPadding.left + label.contentWidth*.5            
        buttonW = labelPadding.left + label.contentWidth + labelPadding.right
        
        buttonH = math.max(height, label.contentHeight)

        label.y = buttonH*.5 + labelPadding.top - labelPadding.bottom
    

    else

        if imagePosition == "left"  then
            image.x = image.contentWidth*.5 + imagePadding.left - imagePadding.right
            label.x = image.x + image.contentWidth*.5 + imagePadding.right + labelPadding.left + label.contentWidth*.5            
            buttonW = imagePadding.left + image.contentWidth + imagePadding.right + labelPadding.left + label.contentWidth + labelPadding.right
            
            buttonH = math.max(height, image.contentHeight, label.contentHeight)

            image.y = buttonH*.5 + (imagePadding.top or 0 ) - (imagePadding.bottom or 0)
            label.y = buttonH*.5 + labelPadding.top - labelPadding.bottom


        elseif imagePosition == "right" then                    
            label.x = label.contentWidth*.5 + labelPadding.left 
            image.x = label.x + label.contentWidth*.5 + labelPadding.right + imagePadding.left + image.contentWidth*.5 - imagePadding.right
            buttonW = imagePadding.left + image.contentWidth + imagePadding.right + labelPadding.left + label.contentWidth + labelPadding.right
            
            buttonH = math.max(height, image.contentHeight, label.contentHeight)

            image.y = buttonH*.5 + (imagePadding.top or 0 ) - (imagePadding.bottom or 0)
            label.y = buttonH*.5 + labelPadding.top - labelPadding.bottom

        elseif imagePosition == "top" then

        elseif imagePosition == "bottom" then

        end
    end

    if imageOver then
        imageOver.x, imageOver.y = image.x, image.y
    end


    -- creates the button background
    buttonW = math.max(buttonW, width)
    local background = display.newRect(button, buttonW*.5, buttonH*.5, buttonW, buttonH)
    background:setFillColor( unpack(backgroundColor) )
    background.isHitTestable = true
    button.background = background
    background.strokeWidth = backgroundStrokeWidth
    background:setStrokeColor( unpack(backgroundStrokeColor) )

    -- adds the view to the button group
    button:insert(view)
    view.x = background.contentWidth*.5 - view.contentWidth*.5
    --view.y = view.contentHeight*.5



    -- positioning the button
    button.x = x or (left and (left + button.contentWidth*.5)) or (right and (right - button.contentWidth*.5))
    button.y = y or (top and (top + button.contentHeight*.5)) or (bottom and (bottom - button.contentHeight*.5))


    -- check if an event (x,y) is within the bounds of an object
    local function isWithinBounds( object, event )
        local bounds = object.contentBounds
        local x, y = event.x, event.y
        local isWithinBounds = true
            
        if "table" == type( bounds ) then
            if "number" == type( x ) and "number" == type( y ) then
                isWithinBounds = bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
            end
        end
        
        return isWithinBounds
    end
    
    
    -- touch listener of the button
    local function touchListener(event)
        
        local phase = event.phase

        if phase == "began" then
            setStatus( true )

            -- Set focus on the button
            button._isFocus = true
            display.getCurrentStage():setFocus( button, event.id )

        elseif button._isFocus then

            if phase == "moved" then

                if isWithinBounds( button.background, event ) then
                    setStatus( true )
                else
                    setStatus( false )
                end


            elseif phase == "ended" or phase == "cancelled" then
                
                setStatus( false )
                
                if isWithinBounds( button.background, event ) then
                    if onRelease then
                        onRelease(event)
                    end
                end

                -- Remove focus from the button
                button._isFocus = false
                display.getCurrentStage():setFocus( nil )

            end

        end

        if onEvent then
            onEvent(event)
        end

        return true -- don't allowing touch event to propagate

    end
    button:addEventListener( "touch", touchListener )
    if onTap then
        button:addEventListener( "tap", onTap )
    end
    

    --------------------------------------------------------------
    -- The function and variable below are just to make this widget compatible with takeFocus function of the other Corona widgets - like scrollview
    --------------------------------------------------------------
    function button:_loseFocus()
        setStatus( false )
        
    end
    button._widgetType = "buttonRB"



    return button

end




cw.newGrid = function(params)


    -- receiving the params
    local objectWidth = params.objectWidth
    local objectHeight = params.objectHeight
    local gridWidth = params.gridWidth  -- (optional) total width of the grid. It is used to wrap the elements in a new line
    local qty = params.qty 
    local spacing = params.spacing or 1
    local spacingVertical = params.spacingVertical or spacing
    local spacingHorizontal = params.spacingHorizontal or spacing
    local onRelease = params.onRelease

    local imagesURLs = params.imagesURLs
    
    local onRender = params.onRender
    local onFinish = params.onFinish


    -- creating group that will be the grid
    local group = display.newGroup() 


    local objList = {}  -- pointer to the objects



    local function addImageToGrid(imageURL)

        local index = #objList + 1

        -- creating the container
        local container = display.newContainer(group, objectWidth, objectHeight )
        container.index = index

        -- adding the object to the container
        local img = display.loadImageFromInternet(container, imageURL)
        local scaleF = math.max(container.contentWidth/img.contentWidth, container.contentHeight/img.contentHeight)
        img:scale(scaleF, scaleF)        
        container:insert( img )        
        container.img = img

        -- positionating the container
        local x = objectWidth *.5 + (spacingVertical + objectWidth*.5) * (index - 1)
        local y = objectHeight *.5 

        if index > 1 then
            x = objList[#objList].x + spacingHorizontal + objectWidth
            y = objList[#objList].y
        end
        local rightPos = x + objectWidth*.5

        -- if surpassed the width limit, let's create a new line
        if gridWidth and rightPos > gridWidth then
            x = objList[1].x
            y = y + spacingVertical + objectHeight
        end

        container.x = x  -- center x
        container.y = y  -- center y

        objList[index] = container

        if onRelease then
            container:addEventListener( "tap", onRelease )
        end


        return container


    end

    for i=1, #imagesURLs do        
        timer.performWithDelay( 300 + 100*i, function()
            addImageToGrid(imagesURLs[i])

            if onRender then
                onRender({target=group})
            elseif onFinish and i == #imagesURLs  then                                        
                onFinish({target=group})
            end
        end)
    end


    return group

end





-- creates off/on switch
cw.newSwitch = function(options)

    local parent = options.parent
    local onSwitchTap = options.onSwitchTap
    local w = options.width or options.w or APP_CONFIG.images.settings["switchBgOff"].width
    local h = options.height or options.h or APP_CONFIG.images.settings["switchBgOff"].height    
    local top = options.top
    local left = options.left    
    local x = options.x
    local y = options.y
    local right = options.right
    local onColor = options.onColor    or {195/255,99/255,70/255,1}
    local offColor = options.offColor  or {120/255,120/255,120/255,1}
    local initialSwitchState = options.initialSwitchState or options.initialValue
    local onChange = options.onChange 
    local font = options.font or native.systemFont  
    local id = options.id





    
    local background
    local block

    local group = display.newGroup()

    group.currentStatus = initialSwitchState

    local isTransitioning = false

    local containerLeft
    local containerRight
    local block  -- the object (usually a circle or rectangle) that moves to the sides

    -- switch tap handler
    local function onTap(event)

        if isTransitioning then return end
        
        local newStatus = event.newStatus

        if newStatus == nil then
            newStatus = not group.currentStatus
        end
                print("newStatus=", newStatus)
        -- sets position and color of origin and destination        
        local newContainerRightW = w
        local newBlockX = block.contentWidth*.5 - w*.5
        

        if newStatus then
            newContainerRightW = 0
            newBlockX = - block.contentWidth*.5 + w*.5
        end

        -- on finish transition
        local function onFinish(e)
            isTransitioning = false

            if newStatus then
                containerRight.isVisible = false
            end
            group.currentStatus = newStatus
            if onChange and event.ignoreCallBack ~= true then
                onChange({newStatus=newStatus, id=id, target=group})
            end        
            
        end

        -- starts transition
        isTransitioning = true
        --transition.to(block,{x=newX, time=200, onComplete=onFinish})            
        containerRight.isVisible = true

        local duration = 1500
        if event.disableAnimation then
            duration = 0
        end
        
        transition.to(containerRight, {time = duration*.25, width = newContainerRightW})
        transition.to(block, {delay=duration*0.25/3, time = duration*.25/2, x = newBlockX, onComplete=onFinish})


    end

    -- public function to allow the user change the status manually
    group.setStatus = function(newStatus, disableAnimation)
    print("on setStatus=", newStatus)
        onTap({newStatus=newStatus, ignoreCallBack = true, disableAnimation = disableAnimation} )
    end


    --block = display.newImage(group, "images/settings/swtich_circle@3x.png")
    block = display.loadImage(group, APP_CONFIG.images.settings["switchBgCircle"])
    block.x = block.contentWidth*.5 - w*.5
    block.y = 0


    -- container behaves as regular newRect (by default it uses anchorX, anchorY = .5, .5.)
    containerLeft = display.newContainer(group, w, h)
    containerLeft.anchorX = 0
    containerLeft.anchorChildren = false -- this is to affect the container children, not the container itself
    containerLeft.x = - w*.5
    containerLeft.y = 0    
    
    --local backgroundOn = display.newImage(containerLeft, "images/settings/switch_bg_on@3x.png")
    local backgroundOn = display.loadImage(containerLeft, APP_CONFIG.images.settings["switchBgOn"])
    --local backgroundOn = display.newImageRect(APP_CONFIG.images.settings["switchBgOn"].path, w, h)
    backgroundOn.anchorX = 0
    backgroundOn.x = 0


    containerRight = display.newContainer(group, w, h)
    containerRight.anchorX = 1
    containerRight.anchorChildren = false -- this is to affect the container children, not the container itself
    containerRight.x = w*.5
    containerRight.y = 0    

    --local backgroundOff = display.newImage(containerRight, "images/settings/switch_bg_off@3x.png")
    local backgroundOff = display.loadImage(containerRight, APP_CONFIG.images.settings["switchBgOff"])
    backgroundOff.anchorX = 1
 

    group:insert(block)
    

    --group.setStatus(true)
    --timer.performWithDelay( 5000, function() group.setStatus(false) end)



    backgroundOn:addEventListener( "tap", onTap )

    

    -- block that moves    
    if initialSwitchState then
         containerRight.width = 0
         block.x = - block.contentWidth*.5 + w*.5
    end

    -- makes the group obey the anchor of the default system
    group.anchorChildren = true

    -- sets the position of the switch
    group.x = x or (right - w*.5)
    group.y = y

    if parent then
        parent:insert(group)
    end


    return group

end









-- this function is a wrapper for the tableView widget. It adds a 0 row-index as the Loading More row, which is activated the user slides down the tableView
-- it receives the same params as the normal tableview with more 2 extra params (see beloe)
cw.newTableView = function(options)
    
    local tableView = nil
    
    local listener = options.listener
    local rowRender = options.onRowRender
    local topPadding = options.topPadding or 0    

    -- custom params
    local functionThatCreatesRefreshingGroup = options.functionThatCreatesRefreshingGroup
    local functionThatCreatesLoadingMoreGroup = options.functionThatCreatesLoadingMoreGroup
    local refreshFunction = options.refreshFunction   -- function that is used to reload all tableview data
    local loadMoreFunction = options.loadMoreFunction


    -- control variables
    local refreshingRowHeight = nil
    local isRefreshingVisible = functionThatCreatesRefreshingGroup ~= nil

    local loadingMoreRowHeight = nil
    local isLoadingMoreVisible = false --functionThatCreatesLoadingMoreGroup ~= nil


    -- shows the Loading More Row as the 1st Row of the TableView
    local function showRefreshing()
        print("on showRefreshing")
        if isRefreshingVisible then return end
        isRefreshingVisible = true

        -- let's show the Loading More row
        local row = tableView:__getRowAtIndex(1)
        transition.to(row, {alpha=1, time=400})
        if row.spinner then
            row.spinner:start()
            row.spinner.alpha = 1
        end
        

        -- adjust the padding to make the Loading More row visible
        tableView._view._topPadding = tableView._view._topPadding + refreshingRowHeight                
        
    end

    -- hides the Loading More Row
    local function hideRefreshing(doNotScroll)
        print("on hideRefreshing")
        if isRefreshingVisible == false then return end
        isRefreshingVisible = false

        -- let's hide the Loading More row
        local row = tableView:__getRowAtIndex(1)
        --row.alpha = 0
        if row.spinner then
            row.spinner:stop()
        end
        

        -- adjust the padding to make the Loading More space row not visible
        tableView._view._topPadding = tableView._view._topPadding - refreshingRowHeight
        print("going to scroll? = ", doNotScroll)
        -- let's move the tableView to top
        if doNotScroll ~= true then        
            print("yes")
            tableView:scrollToY( { y=-topPadding, time=600 } )
        end
        
    end


    local function addLoadingMore()        
        local rowFrame = functionThatCreatesLoadingMoreGroup()
        loadingMoreRowHeight = rowFrame.contentHeight            

        display.remove(rowFrame)
        rowFrame = nil

        -- Insert a row into the tableView
        tableView:insertRow({
                rowHeight = loadingMoreRowHeight,
                rowColor = { default={ 0, 1, 1,0 }, over={ 1, 0.5, 0, 0 } },
                params = {isLoadingMoreRow = true}
            }
        )

            
    end
        -- shows the Loading More Row as the 1st Row of the TableView
    local function showLoadingMore()
        --print("on showLoadingMore")
        if isLoadingMoreVisible then return end
        isLoadingMoreVisible = true
        
        addLoadingMore()         
        
    end

    local function hideLoadingMore()
        print("on hideLoadingMore")
 
        local row = tableView:__getRowAtIndex(tableView:__getNumRows())
        if row then
            row.alpha = 0
            if row.spinner then
                row.spinner:stop()
            end
        end
        tableView:__deleteRows({tableView:__getNumRows()}, {slideLeftTransitionTime=1,slideUpTransitionTime=1})
        isLoadingMoreVisible = false
    end
   


    local function onRowRender( event )

        -- Get reference to the row group
        local row = event.row
        local index = event.row.index
         --print("index=", index, "row.parmas=", event.params, "row.params.isLoadingMoreRow=",row.params and row.params.isLoadingMoreRow) 
        -- let's add the Loading More row if the it is the first row being added and the user provided the function that creates the Loading More group  
        if index == 1 and functionThatCreatesRefreshingGroup then
            print("creating group")
            local rg = functionThatCreatesRefreshingGroup()            
            row:insert(rg)
            row.spinner = rg.spinner
            rg.x = CENTER_X            
            row.alpha = 0
            

            -- let's make the Loading More row start hided
            hideRefreshing(row.notFirstRender)

            row.notFirstRender = true
            
        elseif row.params and row.params.isLoadingMoreRow then
            

            local lmg = functionThatCreatesLoadingMoreGroup()            
            row:insert(lmg)
            lmg.x = CENTER_X            
            row.spinner = lmg.spinner
            transition.from(row,{alpha=0, time=600})
            if row.spinner then
                row.spinner:start()
                row.spinner.alpha = 1
            end

            -- we need to force the scroll to the bottom after adding the row because the tableView was already moving before the new row
            tableView:__scrollToIndex(index)

        else
            if functionThatCreatesRefreshingGroup then                        
                event.row.index = event.row.index - 1
            end
            rowRender(event)                        
        end
            
    end

    

    
    local needToReload = false    
    local function scrollListener(event)
        local phase = event.phase
        local y = tableView:getContentPosition()
        --print("x,y=", tableView:getContentPosition(), "  -  phase", phase, "  -  limitReached=", event.limitReached, " -- dir=", event.direction)
        --if y > 70 and phase == "ended" then
        if y > 70 and phase == "moved" then
            needToReload = true
            --print("need to reloead!")
            if functionThatCreatesRefreshingGroup then                        
                showRefreshing()
            end            
        end
        

        -- If we have reached one of the scrollViews limits
        if event.limitReached then
            local direction = event.direction
            
            if "up" == direction then -- arrived the at the bottom of the tableview
                print("let's show the loadingMore group")
                if functionThatCreatesRefreshingGroup then                        
                    showLoadingMore()
                    if loadMoreFunction then                
                        loadMoreFunction()                    
                    end
                end                

            elseif "down" == direction and needToReload then    
                if refreshFunction then                
                    refreshFunction()                    
                end
                needToReload = false
            end
                        
        end
        
        if listener then
            listener()            
        end
    end
    
    
    options.listener = scrollListener
    options.onRowRender = onRowRender
    tableView = require("widget").newTableView(options)
    

    local function addRefreshingRow()
        local rowFrame = functionThatCreatesRefreshingGroup()
        refreshingRowHeight = rowFrame.contentHeight            
        
        print("refreshingRowHeight=", refreshingRowHeight)
        display.remove(rowFrame)
        rowFrame = nil

        -- Insert a row into the tableView
            tableView:insertRow({
                    rowHeight = refreshingRowHeight,
                    rowColor = { default={ 0, 1, 1,0 }, over={ 1, 0.5, 0, 0 } },
                }
            )

            
    end

   
    
        -- making extra public functions
        tableView.showRefreshing = showRefreshing
        tableView.hideRefreshing = hideRefreshing
    
        tableView.showLoadingMore = showLoadingMore
        tableView.hideLoadingMore = hideLoadingMore

    
        -- saving default functions (that will be overwrited below)
        tableView.__getRowAtIndex = tableView.getRowAtIndex
        tableView.__getNumRows = tableView.getNumRows
        tableView.__deleteAllRows = tableView.deleteAllRows
        tableView.__deleteRows = tableView.deleteRows
        tableView.__scrollToIndex = tableView.scrollToIndex
        

        tableView.getRowAtIndex = function(obj, index)
            if functionThatCreatesRefreshingGroup then
                index = index + 1
            end
            print(index)
            return tableView:__getRowAtIndex(index)
        end

        tableView.getNumRows = function(obj)

            local numRows = tableView:__getNumRows()
            if functionThatCreatesRefreshingGroup then
                numRows = numRows - 1
            end
            if isLoadingMoreVisible then
                numRows = numRows - 1
            end
            return numRows

        end

        

        tableView.deleteAllRows = function(obj)
        print("deleting all rows")
        if functionThatCreatesRefreshingGroup then
            tableView.hideLoadingMore()
        end

            tableView:__deleteAllRows()

            if functionThatCreatesRefreshingGroup then
                addRefreshingRow()
            end            

        end

        tableView.deleteRows = function(obj, rowArray, animationOptions)
            print("deleting rows")
            for i=1, #rowArray do
                rowArray[i] = rowArray[i] - 1
            end

            tableView:__deleteRows(rowArray, animationOptions)
            
        end

        tableView.scrollToIndex = function(obj, index, time, onComplete)
            if functionThatCreatesRefreshingGroup then
                index = index + 1
            end
            
            tableView:__scrollToIndex(index, time, onComplete)

        end
    




    if functionThatCreatesRefreshingGroup then
        
        addRefreshingRow()

    end


    return tableView
    
    
end

cw.newButtonOLD = function(options)

    options.x = options.x or (options.left and (options.left + options.width*0.5)) or (options.right and (options.right - options.width*0.5))
    options.y = options.y or (options.top + options.height*0.5)

    local bt = require("widget").newButton(options)

    bt._view.isHitTestable = true

    return bt

end


return cw
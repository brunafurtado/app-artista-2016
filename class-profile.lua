local profile = {}

profile.name = APP_CONFIG.artistName
profile.image = nil   -- image table {url, width, height}
profile.description = nil

profile.awards = {}
profile.doublePlatinum = nil
profile.gold = nil
profile.platinum = nil



-- let's download the info
SERVER.getProfile()



return profile
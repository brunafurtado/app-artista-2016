local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        

    ------------------------------------------------------------------------------------
    -- LOCAL VARIABLES
    ------------------------------------------------------------------------------------

    local sceneAssets = _G.IMAGES.otherArtists

    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}

    local topBar = require("module-topBar").new(sceneGroup, "Configurações")
    sceneGroup:insert(topBar)
    
    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}



    local margin={left=35/3, right=35/3}

    local frameWidth = SCREEN_W - (35/3)*2


    local frame = display.newGroup()
    sceneGroup:insert(frame)

    frame.y = topBar.y + topBar.contentHeight + 12
    frame.x = CENTER_X - frameWidth*.5

    local lbTitle = display.newText2{text="Selecione quais seções você gostaria de receber notificações", font=APP_CONFIG.fontLight, fontSize=18, width=frameWidth - margin.right - margin.left, align="center", color={129/255,129/255,129/255}}
    lbTitle.anchorChildren = true
    lbTitle.anchorX = 0.5
    lbTitle.anchorY = 0
    lbTitle.y = 12
    lbTitle.x = frameWidth*.5
    frame:insert(lbTitle)

    local lineY = lbTitle.y + lbTitle.contentHeight + 15
    local line = display.newLine(margin.left,lineY, frameWidth - margin.right,lineY)
    frame:insert(line)
    line.strokeWidth = 1
    line:setStrokeColor( 129/255,129/255,129/255 )
        

    local onChange = function(e)
        print("new=", e.newStatus)
        print("id=", e.id)

        require("class-notifications").updateNotificationPreference(e.id, e.newStatus, function(result)
            if result then
                --native.showAlert( "App Bruna Karla", "Atualização salva", {"ok"} )
            else
                print("falhou, voltando para ", not e.newStatus)
                e.target.setStatus(not e.newStatus)
            end

        end)

    end

    
    local function createLabelSwitchPair(id, text, value, onChange)

        local pairGroup = display.newGroup()
        
        local label = display.newText{parent=pairGroup, text=text, x=0, font=APP_CONFIG.fontLight, fontSize=14}
        label.anchorX = 0        
        label:setTextColor( 30/255, 30/255, 30/255 )
        label.y = label.contentHeight*.5

        pairGroup.switch = require("library-widgets").newSwitch{parent=pairGroup, id=id, initialValue = value, right=frameWidth - margin.right - margin.left, y=label.y, onChange= onChange}        

        return pairGroup

    end

    -- local notifOptions = { 
    --     {id=1, label="Notícias", value=false},
    --     {id=2, label="Agenda", value=false},
    --     {id=3, label="Vídeos", value=false},
    --     {id=3, label="Fotos", value=false},
    --     {id=3, label="Discografia", value=false},
    -- }

    local notifOptions = require("class-notifications").getNotificationsPreferences()
    
    for i=1,#notifOptions do
            print("notifOptions[i].value=", notifOptions[i].value)
            notifOptions[i].group = createLabelSwitchPair(notifOptions[i].id, notifOptions[i].label, false, onChange)
            notifOptions[i].group.x = line.x
            notifOptions[i].group.y = lineY + 15
            frame:insert(notifOptions[i].group)


            lineY = notifOptions[i].group.y + notifOptions[i].group.contentHeight*.5 + 15
            local line = display.newLine(line.x,lineY, line.x + line.contentWidth,lineY)
            frame:insert(line)
            line.strokeWidth = 1
            line:setStrokeColor( 129/255,129/255,129/255 )

            if i == #notifOptions then
                line.alpha = 0
            end
    end
 
     
    local backgroundH = frame.contentHeight + frame[1].y
    local backgroundW = frameWidth
    local background = display.newRect(backgroundW*.5, backgroundH*.5, backgroundW, backgroundH)
    background:setFillColor( 1,1,1 )
    frame:insert(1, background)
   
    frame.alpha = 0


    sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, SCREEN_H*.35) 
    sceneGroup:insert(sceneGroup.loading)
    
    require("class-notifications").downloadNotificationSettings(function()

        local notifOptions = require("class-notifications").getNotificationsPreferences()
         for i=1,#notifOptions do
            print("notifOptions[i].value=", notifOptions[i].value)
            notifOptions[i].group.switch.setStatus(notifOptions[i].value, true)            
            --notifOptions[i].group.switch.setStatus(true, true)            
        end

        display.remove(sceneGroup.loading)

        frame.alpha = 1

    end)

    
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).        
        

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

       _G.rbBack.addBack()

       _G.MENU.highlightMenuWithLabel("Configurações")
       
        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Configurações;Configurações;"
        print(eventFlurry)
        _G.analytics.logEvent(eventFlurry)
        -- fim

        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
-- stores the reusable frames (group of objects) 

local f = {}

local widget = require "widget"




f.newsEntry = function(newsObj, isSizeEstimateCalculation, btSeeMoreHandler, scrollViewObj)

        local frame = display.newGroup( )
        --frame.anchorChildren = true


        local frameWidth = SCREEN_W - (35/3)*2
        local padding = {top=10, left=10, right=10, bottom=10}
        local frameContentWidth = frameWidth - padding.left - padding.right


        local background = display.newRect(frame, frameWidth*.5, 1, frameWidth, 2)
        background:setFillColor( 1,1,1 )
        background.strokeWidth = 1
        background:setStrokeColor(163/255,163/255,163/255)        

        if newsObj.image then
            local image = display.loadImageFromInternet(frame, newsObj.image, isSizeEstimateCalculation)
            local scaleFactor = math.min(frameContentWidth / image.contentWidth, 1)
            image:scale(scaleFactor, scaleFactor)
            image.x = background.x
            image.y = padding.top + image.contentHeight*.5
            --frame:insert(image)
            frame._image = image

        end


        local lbDate = display.newText{text=newsObj.date.day.."/"..newsObj.date.month.."/"..newsObj.date.year, font=APP_CONFIG.fontLight, fontSize=12, width = frameContentWidth}
        lbDate.anchorX, lbDate.anchorY = 0, 0
        lbDate:setTextColor( 0,0,0 )        
        lbDate.x = padding.left
        lbDate.y = frame.contentHeight + padding.top
        frame:insert(lbDate)

        local lbTitle = display.newText2{text=newsObj.title, font=APP_CONFIG.fontLight, fontSize=16, width = frameContentWidth, color={0,0,0}}
        --lbTitle.anchorX, lbTitle.anchorY = 0, 0
        --lbTitle:setTextColor( 0,0,0 )        
        lbTitle.x = padding.left        
        lbTitle.y = frame.contentHeight + padding.top
        frame:insert(lbTitle)
        frame.title = lbTitle

        local summary = newsObj.summary
        local summaryBegin = summary:sub(1,6)
        local summaryEnd = summary:sub(7)

        -- manually removing any newline and spaces from the beginning
        summaryBegin = summaryBegin:gsub("\r\n", "")
        summaryBegin = summaryBegin:gsub("\r\n", "")                
        summaryBegin = summaryBegin:gsub(string.char(194), "")  -- Latin Capital letter A with circumflex
        summaryBegin = summaryBegin:gsub(string.char(160), "")  -- Non-breaking space
        summary = summaryBegin .. summaryEnd


        local lbSummary = display.newText2{text=summary, spaceBetweenLines = 1, font=APP_CONFIG.fontLight, fontSize=14, color={62/255,62/255,62/255}, width = frameContentWidth}        
        lbSummary.x = padding.left        
        lbSummary.y = frame.contentHeight + padding.top
        frame:insert(lbSummary)
        frame.summary = title


        local btSeeMore = require("library-widgets").newButton{
            right = frameWidth - padding.right,
            top = frame.contentHeight,
            --width = 380*(320/1080), 
            height=30,
            
            label = "VER NOTÍCIA COMPLETA",
            labelColor = {0,162/255,1},
            labelOverColor = {0,162/255,1,.3},
            labelFont = APP_CONFIG["fontMedium"],
            labelFontSize = 10,
            
            imageFile = APP_CONFIG.images.icons["arrowLeft"].path,
            imageWidth = APP_CONFIG.images.icons["arrowLeft"].width,
            imageHeight = APP_CONFIG.images.icons["arrowLeft"].height,
            imagePosition = "right",
            imageScaleX = -1,
            imagePadding = {left=4, bottom=2},

            imageColor = {0,162/255,1},
            imageOverColor = {0,162/255,1,.3},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onEvent = function(event) 

                local phase = event.phase
                local target = event.target

                if ( "ended" == phase ) then

                    btSeeMoreHandler(newsObj) 
                    

                elseif "moved" == phase then

                     local dy = math.abs( event.y - event.yStart )

                     if dy > 12 then
                        if scrollViewObj then
                            --print("going to give focus to SV")
                            scrollViewObj:takeFocus( event )
                        end
                         
                     end
               end

            end, 
        }
        frame:insert(btSeeMore)


        
        background.height = frame.contentHeight
        background.y = background.height*.5

        -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
        local invisibleRect = display.newRect(frame, 0,frame.contentHeight + background.strokeWidth,frame.contentWidth, 20)
        invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
        invisibleRect.alpha = 0


        return frame

    end


-- frame used on the Agenda scene
f.eventEntry = function(eventObj, isSizeEstimateCalculation)

        local frame = display.newGroup( )
        --frame.anchorChildren = true

        local frameWidth = SCREEN_W - (35/3)*2
        local padding = {top=10, left=10, right=10}
        local frameContentWidth = frameWidth - padding.left - padding.right


        local background = display.newRect(frame, frameWidth*.5, 1, frameWidth, 2)        
        background:setFillColor( 1,1,1,.1 )

        
        local lbDay = display.newText{text=eventObj.date.day, font=APP_CONFIG.fontLight, fontSize=24, width=30}
        lbDay.anchorX, lbDay.anchorY = 0, 0
        lbDay:setTextColor( 0,171/255,235/255 )        
        lbDay.x = padding.left
        lbDay.y = frame.contentHeight + padding.top
        frame:insert(lbDay)

        lbTitleLeft = lbDay.x + lbDay.contentWidth + 4
        lbTitleW = frameContentWidth - lbTitleLeft - padding.right
        local lbTitle = display.newText2{text=eventObj.name or "", font=APP_CONFIG.fontLight, fontSize=18, width=lbTitleW, color={ 1,1,1 }}
        lbTitle.anchorX, lbTitle.anchorY = 0, 0        
        lbTitle.x = lbTitleLeft
        lbTitle.y = lbDay.y
        frame:insert(lbTitle)

        local fontSize = 16

        local lbAddressName = display.newText2{text=eventObj.address.name or "", font=APP_CONFIG.fontLight, fontSize=fontSize, width=lbTitleW, color={1,1,1}}
        lbAddressName.anchorX, lbAddressName.anchorY = 0, 0
        lbAddressName.x = lbTitle.x
        lbAddressName.y = lbTitle.y + lbTitle.contentHeight + 6
        frame:insert(lbAddressName)

        local minutes = eventObj.date.min
        if minutes == 0 then minutes = "00" end
        local lbHourString = "às " .. eventObj.date.hour .. ":" .. minutes
        local lbHour = display.newText{text=lbHourString, font=APP_CONFIG.fontLight, fontSize=fontSize, width = lbTitleW}
        lbHour.anchorX, lbHour.anchorY = 0, 0
        lbHour:setTextColor( 1,1,1)
        lbHour.x = lbTitle.x
        lbHour.y = lbAddressName.y + lbAddressName.contentHeight + 4
        frame:insert(lbHour)

        local lbStreet = display.newText{text= eventObj.address.street, font=APP_CONFIG.fontLight, fontSize=fontSize, width = lbTitleW}
        lbStreet.anchorX, lbStreet.anchorY = 0, 0
        lbStreet:setTextColor( 1,1,1)
        lbStreet.x = lbTitle.x
        lbStreet.y = lbHour.y + lbHour.contentHeight + 4
        frame:insert(lbStreet)


        local regionString  = eventObj.address.neighborhood  --.. ", " .. eventObj.address.city .. ", " .. eventObj.address.state 
        if #regionString > 0 then
            regionString = regionString .. ", " .. eventObj.address.city            
        end
        if #regionString > 0 then
            regionString = regionString .. ", " .. eventObj.address.state            
        end
        local lbRegion = display.newText{text= regionString, font=APP_CONFIG.fontLight, fontSize=fontSize, width = lbTitleW}
        lbRegion.anchorX, lbRegion.anchorY = 0, 0
        lbRegion:setTextColor( 1,1,1)
        lbRegion.x = lbTitle.x
        lbRegion.y = lbStreet.y + lbStreet.contentHeight + 4
        frame:insert(lbRegion)

        local lineY = lbStreet.y + lbStreet.contentHeight
        if #regionString > 0 then
            lineY = lbRegion.y + lbRegion.contentHeight*2
        end

        local line = display.newLine(lbDay.x,lineY, frameWidth - padding.right,lineY)
        line.strokeWidth = 1
        line:setStrokeColor( 64/255, 64/255, 68/255 )
        frame:insert(line)

        -- Adicionado por Bruna em 23/10/2015 
        -- nome do app cujo item será compartilhado
        local shareTo = "Compartilhar"
    
        local shareMessage = eventObj.name .. " dia " .. eventObj.date.day .. "/" .. eventObj.date.month .. " " .. lbHourString
        local btShare = require("custom-widget").newShareButton{
            parent = frame,
            right = frameWidth - 9,
            top = lineY,
            --shareImageFilename = function() print("on func=",sceneGroup.imageFilename); return sceneGroup.imageFilename; end,
            shareMessage = shareMessage,
            flurry = "outClick;Agenda;Agenda;Compartilhar;" .. shareTo .. ";" .. eventObj.name .." - " .. eventObj.address.name .. " " .. lbHourString .. ";",
        }

    
        background.height = frame.contentHeight
        background.y = background.height*.5

        -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
        local invisibleRect = display.newRect(frame, 0,frame.contentHeight + background.strokeWidth,frame.contentWidth, 2)
        invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
        invisibleRect.alpha = 0


        return frame

    end


f.videoEntry = function(videoObj, isSizeEstimateCalculation, btSeeMoreHandler)

        local frame = display.newGroup( )
        --frame.anchorChildren = true


        local frameWidth = SCREEN_W - (35/3)*2
        local padding = {top=10, left=10, right=10, bottom=10}
        


        local background = display.newRect(frame, frameWidth*.5, 1, frameWidth, 2)
        background:setFillColor( 1,1,1 )
        background.strokeWidth = 1
        background:setStrokeColor(163/255,163/255,163/255)        

                            
        local image = display.loadImageFromInternet(frame, videoObj.image, isSizeEstimateCalculation)
        local scaleFactor = (416/1080)* SCREEN_W / image.contentWidth
        image:scale(scaleFactor, scaleFactor)
        image.x = padding.left + image.contentWidth*.5
        image.y = padding.top + image.contentHeight*.5
        frame._image = image
        frame.videoURL = videoObj.videoURL

        
        local lbTitleLeft = image.x + image.contentWidth*0.5 + padding.left 
        local frameContentWidth = frameWidth - lbTitleLeft - padding.right
        local lbTitle = display.newText2{text=videoObj.title, font=APP_CONFIG.fontMedium, fontSize=12, width = frameContentWidth, color={0,0,0}}
        lbTitle.anchorX, lbTitle.anchorY = 0, 0
        --lbTitle:setTextColor( 0,0,0 )        
        lbTitle.x = lbTitleLeft
        lbTitle.y = image.y - image.contentHeight*0.5 + 2
        frame:insert(lbTitle)
        
        local lbSubTitle = display.newText{text=videoObj.subTitle or "", font=APP_CONFIG.fontLight, fontSize=10, width = frameContentWidth}
        lbSubTitle.anchorX, lbSubTitle.anchorY = 0, 0
        lbSubTitle:setTextColor( 62/255,62/255,62/255 )        
        lbSubTitle.x = lbTitle.x
        lbSubTitle.y = lbTitle.y + lbTitle.contentHeight + 10
        frame:insert(lbSubTitle)

        
        local lbDate = display.newText{text="Publicado em " .. videoObj.date.day.."/"..videoObj.date.month.."/"..videoObj.date.year, font=APP_CONFIG.fontLight, fontSize=10, width = frameContentWidth}
        lbDate.anchorX, lbDate.anchorY = 0, 0
        lbDate:setTextColor( 0,0,0 )        
        lbDate.x = lbTitle.x
        lbDate.y = lbSubTitle.y + lbSubTitle.contentHeight + 2
        frame:insert(lbDate)

            
        background.height = frame.contentHeight + padding.bottom
        background.y = background.height*.5


        return frame

    end






f.photoEntry = function(photoObj, isSizeEstimateCalculation)

    local frame = display.newGroup( )

    local frameHeight = 330*(320/1080) + 30

    if isSizeEstimateCalculation then
        return frameHeight
    end

    local frameWidth = (970/1080)*SCREEN_W
    local padding = {left=0, right=0,}

    local imageW = 220*(320/1080)*1.5
    local imageH = 160*(320/1080)*1.5


    local frameBackground = display.newRect(frame, frameWidth*.5, frameHeight*.5, frameWidth, frameHeight)
    frameBackground.alpha = 0

    local imageBackground = display.newRect(frame, imageW*.5 + padding.left, frameBackground.y, imageW, imageH)
    imageBackground:setFillColor( 4/255, 4/255, 4/255)


    local imageContainer = display.newContainer(frame, imageBackground.contentWidth, imageBackground.contentHeight )
    imageContainer.x, imageContainer.y = imageBackground.x, imageBackground.y
    local image = display.loadImageFromInternet(imageContainer, photoObj.image,false, true)
    local scaleFactor = math.max(imageBackground.contentWidth / image.contentWidth, imageBackground.contentHeight/image.contentHeight)
    image:scale(scaleFactor, scaleFactor)
    image.x = 0
    image.y = 0
    frame._image = image

    local lineY = imageContainer.y - imageContainer.contentHeight*.5 - 4
    local line = display.newLine(frame, imageContainer.x - imageContainer.contentWidth*.45,lineY, imageContainer.x + imageContainer.contentWidth*.45, lineY)    
    line.strokeWidth = 3
    line:setStrokeColor( 4/255, 4/255, 4/255)

    lineY = lineY - line.strokeWidth*.5 - 4
    local line = display.newLine(frame, imageContainer.x - imageContainer.contentWidth*.4,lineY, imageContainer.x + imageContainer.contentWidth*.4, lineY)    
    line.strokeWidth = 3
    line:setStrokeColor( 4/255, 4/255, 4/255)

    local lbTitleLeft = imageContainer.x + imageContainer.contentWidth*0.5 + 30*(320/1080)
       
    local titleMaxWidth =  frameWidth - lbTitleLeft - 4
    local lbTitle = display.newText2{text=photoObj.title, font=APP_CONFIG.fontMedium, fontSize=14, width=titleMaxWidth, color={ 0,0,0 }}
    lbTitle.anchorX, lbTitle.anchorY = 0, 0
    --lbTitle:setTextColor( 0,0,0 )        
    lbTitle.x = lbTitleLeft
    lbTitle.y = imageContainer.y - imageContainer.contentHeight*0.5
    frame:insert(lbTitle)
    frame.lbTitle = lbTitle
    --AUX.adjustTextSize(lbTitle,titleMaxWidth )
    
        
    local lbDateString = photoObj.date
    if type(lbDateString) == "table" then
        lbDateString = lbDateString.day .. "/" .. lbDateString.month .. "/" .. lbDateString.year
    end
    local lbDate = display.newText{text=lbDateString , font=APP_CONFIG.fontLight, fontSize=12}
    lbDate.anchorX, lbDate.anchorY = 0, 0
    lbDate:setTextColor( 0,0,0 )        
    lbDate.x = lbTitle.x
    lbDate.y = lbTitle.y + lbTitle.contentHeight + 4
    frame:insert(lbDate)

    local numOfImages = (photoObj.images == nil and 0) or #photoObj.images 
    local lbNumOfImages = display.newText{text=numOfImages, font=APP_CONFIG.fontMedium, fontSize=12}
    lbNumOfImages:setTextColor( 0,0,0 ) 
    lbNumOfImages.anchorX = 0
    lbNumOfImages.anchorY = 0
    lbNumOfImages.x = lbTitle.x
    lbNumOfImages.y = lbDate.y + lbDate.contentHeight + 4
    frame:insert(lbNumOfImages)
    frame.lbNumOfImages = lbNumOfImages

    local lbPhotosString = "fotos"
    if numOfImages == 1 then
        lbPhotosString = "foto"
    end
    local lbPhotos = display.newText{text=lbPhotosString, font=APP_CONFIG.fontLight, fontSize=12}
    lbPhotos:setTextColor( 0,0,0 ) 
    lbPhotos.anchorX = 0
    lbPhotos.anchorY = 0
    lbPhotos.x = lbNumOfImages.x + lbNumOfImages.contentWidth + 2
    lbPhotos.y = lbNumOfImages.y
    frame:insert(lbPhotos)



    -- local lineY = rowEntry.y + rowEntry.contentHeight
    -- local line = display.newLine(rowEntry.x,lineY, rowEntry.x + rowEntry.contentWidth ,lineY)
    -- line.strokeWidth = 1
    -- line:setStrokeColor( 207/255, 207/255, 207/255 )
    -- row:insert(line)

    -- -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
    -- local invisibleRect = display.newRect(0,lineY,rowEntry.contentWidth, 200)
    -- invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
    -- invisibleRect.alpha = 4
    -- row:insert(invisibleRect)


    return frame

end


-- showButtonBuyMusic: boolean. If false, the frame will show the music duration as well
f.albumEntry = function(albumObj, showButtonBuyMusic, isSizeEstimateCalculation, scrollViewObj, isDVD, isSingle)

    local frame = display.newGroup( )

    local frameWidth = (500/1080)*SCREEN_W*2 + 1
    local padding = {top=0, left=4, right=4, bottom=85*(320/1080)}    


    local image = display.loadImageFromInternet(frame, albumObj.image, isSizeEstimateCalculation)
    local scaleFactor = 88 * GROW_WITH_SCREEN_W / image.contentWidth
    image:scale(scaleFactor, scaleFactor)
    image.x = padding.left + image.contentWidth*.5
    image.y = image.contentHeight*.5
    frame._image = image
    


    local lbTitleLeft = image.x + image.contentWidth*0.5 + 4
    
    
    local titleMaxWidth =  frameWidth - padding.right - lbTitleLeft

    local lbTitle = display.newText{text=albumObj.title, font=APP_CONFIG.fontMedium, fontSize=12, width = titleMaxWidth}
    lbTitle.anchorX, lbTitle.anchorY = 0, 0
    lbTitle:setTextColor( 0,0,0 )        
    lbTitle.x = lbTitleLeft
    lbTitle.y = image.y - image.contentHeight*0.5 + 2
    frame:insert(lbTitle)
    
    local lbArtist = display.newText{text=albumObj.artist, font=APP_CONFIG.fontLight, fontSize=10, width = titleMaxWidth}
    lbArtist.anchorX, lbArtist.anchorY = 0, 0
    lbArtist:setTextColor( 62/255,62/255,62/255 )        
    lbArtist.x = lbTitle.x
    lbArtist.y = lbTitle.y + lbTitle.contentHeight + 2
    frame:insert(lbArtist)

    local yPos = lbTitle.y + lbTitle.contentHeight + 16

    -- se for single, nao mostrar o label "0 músicas"
    --if isSingle == false then
    if #albumObj.musics ~= 0 and isSingle == false then
        local lbNumOfMusics = display.newText{text=#albumObj.musics, font=APP_CONFIG.fontMedium, fontSize=10}
        lbNumOfMusics.anchorX, lbNumOfMusics.anchorY = 0, 0
        lbNumOfMusics:setTextColor( 62/255,62/255,62/255 )
        lbNumOfMusics.x = lbArtist.x
        lbNumOfMusics.y = lbArtist.y + lbArtist.contentHeight + 2
        frame:insert(lbNumOfMusics)

        local lbNumOfMusics2 = display.newText{text="músicas", font=APP_CONFIG.fontLight, fontSize=10}
        lbNumOfMusics2.anchorX, lbNumOfMusics2.anchorY = 0, 0
        lbNumOfMusics2:setTextColor( 62/255,62/255,62/255 )
        lbNumOfMusics2.x = lbNumOfMusics.x + lbNumOfMusics.contentWidth + 1
        lbNumOfMusics2.y = lbNumOfMusics.y
        frame:insert(lbNumOfMusics2)

        yPos = lbNumOfMusics.y + lbNumOfMusics.contentHeight + 2
    end


    local musicTitleMaxWidth = frameWidth - lbTitleLeft - padding.right
    
    local lbDate = display.newText{text="Lançado em " .. AUX.formatDateToMonthYear(albumObj.date) , font=APP_CONFIG.fontLight, fontSize=10}
    lbDate.anchorX, lbDate.anchorY = 0, 0
    lbDate:setTextColor( 0,0,0 )        
    lbDate.x = lbTitle.x
    --lbDate.y = lbNumOfMusics.y + lbNumOfMusics.contentHeight + 2
    lbDate.y = yPos
    frame:insert(lbDate)


    -- incluido por Bruna em 19/10/2015
    local tabSelected = "Letras"
    local sceneSelected = "Letras"
            
    if isSingle == false and isDVD == false then
        tabSelected = "ÁLBUNS"
    elseif isSingle == true then
        tabSelected = "SINGLES"
    elseif isDVD == true then
        tabSelected = "DVDs"
    end
    
    
    if tabSelected == "Letras" then
        sceneSelected = "Letras"
    else
        sceneSelected = "Discografia"
    end
    
    
    
    local platform = system.getInfo( "platformName" )
    --print(platform)
    
    
    local deviceDiscography
    
    if _G.DEVICE.isApple then
        
        deviceDiscography = "iTunes"
        
    elseif _G.DEVICE.isGoogle then

        deviceDiscography = "Google Play"       

    elseif _G.DEVICE.isKindleFire then
    
        deviceDiscography = "Amazon"           

    elseif platform == "WinPhone" then
        
        deviceDiscography = "Windows Phone"
        
    else
    
        -- any other DEVICE (windows, html,..) will end up here
        
    end

    --print(deviceDiscography)
    
    -- fim
    
    
    local btBuyMusicRelease = function(event)

        -- incluido por Bruna em 20/10/2015
        local id = event.target.id
        -- fim
        
        local btBuyMusic = event.target

        if btBuyMusic.buyMusicURL then
            system.openURL( btBuyMusic.buyMusicURL )
            
            -- incluido por Bruna em 19/10/2015 
            print ("outClick;" .. sceneSelected ..";" .. tabSelected .. ";Comprar Faixa;" .. deviceDiscography .. ";" .. albumObj.title .. ";")
            --print("Discografia;" .. tabSelected .. ";Comprar Músicas - " .. deviceDiscography .. ";" .. albumObj.title .. ";")
            local eventFlurry = "outClick;" .. sceneSelected ..";" .. tabSelected .. ";Comprar Faixa;" .. deviceDiscography .. ";" .. albumObj.title .. ";"
            _G.analytics.logEvent(eventFlurry)
            -- fim
            
        end

        
    end

    local btLyricRelease = function(event)
        
            -- incluido por Bruna em 20/10/2015
            local id = event.target.id
            -- fim
        
            local btLyric = event.target
            
            local function onServerResponse(e)
                
                btLyric.alpha = 1
                display.remove(btLyric.spinner)
                if e.result then
                    e.data.title = e.data.title or btLyric.musicTitle
                    require("module-popups").show("lyric", e.data)
                            
                    -- incluido por Bruna em 19/10/2015 
                    local eventFlurry = "view;" .. sceneSelected .. ";Letra;" .. albumObj.title .. ";" .. e.data.title .. ";"
                    print(eventFlurry)
                    _G.analytics.logEvent(eventFlurry)
                    -- fim
                
                end
            end
            btLyric.alpha = 0
            btLyric.spinner = AUX.showSpinner(btLyric)

            SERVER.getLyric(btLyric.musicID, onServerResponse)

    end

    

    local function btListener( event )
      print("on btListener - id", event.target.id)
        local phase = event.phase
        --print("phase: " .. phase)
        local target = event.target

        if ( "ended" == phase ) then
                        
            local id = event.target.id
            local url = nil
            print("id= ", id)
            
            -- incluído por Bruna em 21/10/2015 para validar que botão foi clicado para enviar para o flurry
            local botaoDiscografia
            local parceiro = deviceDiscography
            
            if id == "buyDigitalAlbumURL" then
                botaoDiscografia = "Comprar Álbum Digital"
                if isSingle == true then
                    botaoDiscografia = "Comprar Single"
                end
                
            elseif id == "buyDiscURL" then
                botaoDiscografia = "Comprar CD"
                parceiro = "MK Shopping"
                if isDVD == true then
                    botaoDiscografia = "Comprar DVD"
                end
                
            elseif id == "previewURL" then
                botaoDiscografia = "Preview"
            end
            -- fim
            
            if id == "buyDigitalAlbumURL" or id == "buyDiscURL" or id == "previewURL" then
                url = albumObj[id]
                if url then
                    system.openURL(url)
                    
                    -- incluido por Bruna em 19/10/2015 
                    local eventFlurry = "outClick;" .. sceneSelected ..";" .. tabSelected .. ";" .. botaoDiscografia .. ";" .. parceiro .. ";" .. albumObj.title .. ";"
                    print(eventFlurry)
                    _G.analytics.logEvent(eventFlurry)
                    -- fim
                    
                else
                    print("WARNING - button is trying to open a URL that was not defined!")
                end

            elseif id == "btLyric" then
                btLyricRelease(event)

            elseif id == "btBuyMusic" then
                btBuyMusicRelease(event)

            end
            
            

        elseif "moved" == phase then

             local dy = math.abs( event.y - event.yStart )

             if dy > 12 then
                if scrollViewObj then
                    --print("going to give focus to SV")
                    scrollViewObj:takeFocus( event )
                end
                 
             end
       end

    end


    local btH = math.min(33, image.y + image.contentHeight*.5 - (lbDate.y + lbDate.contentHeight + 2))
    local btW = (titleMaxWidth - 4)/2

    local buttonTop = math.max(image.y + image.contentHeight*.5 - btH, lbDate.y + lbDate.contentHeight + 2)

    local btBuyAlbumLabel = (isSingle and "COMPRAR SINGLE") or "COMPRAR\nÁLBUM DIGITAL"
    local btBuyAlbum = require("library-widgets").newButton{
       id="buyDigitalAlbumURL",  -- using the property of the albumObj received by the server (and used on class-album)
       left = lbTitle.x,
       top = buttonTop, --1436*(320/1080)* _G.GROW_WITH_SCREEN_H,
       width = btW, 
       height = btH,
       label = btBuyAlbumLabel,
       labelPadding = {top=1},
       labelColor = { 1,1,1 }, 
       labelLineSpacing = 4,
       --labelPadding ={bottom = 1},
       labelOverColor = { 0, 0, 0, 0.5 },
       backgroundColor = { 0,0,0, 1 }, 
       backgroundOverColor = { 0,0,0, .3 },

       labelFont = APP_CONFIG["fontMedium"],
       labelFontSize = 11,
       labelWrap = true,
       labelAlign = "center",
       onEvent = btListener
    }
    frame:insert(btBuyAlbum)    
    if albumObj["buyDigitalAlbumURL"] == nil or albumObj["buyDigitalAlbumURL"] == "" then
        btBuyAlbum.isVisible = false
    end

    local btBuyCDLabel = (isDVD and "COMPRAR DVD") or "COMPRAR CD"
    local btBuyCD = require("library-widgets").newButton{
        id="buyDiscURL",
        left =  btBuyAlbum.x + btBuyAlbum.contentWidth*.5 + 2,
        y = btBuyAlbum.y ,
        width = btW, 
        height = btH,
        label = btBuyCDLabel,
        labelPadding = {top=1},
        labelColor = { 1,1,1 }, 
        labelOverColor = { 0, 0, 0, 0.5 },
        backgroundColor = {184/255,0,16/255,1},
        backgroundOverColor = { 184/255,0,16/255,.3},

        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 11,
        labelWrap = true,
        labelAlign = "center",
        onEvent = btListener
    }
    frame:insert(btBuyCD)
    if albumObj["buyDiscURL"] == nil or albumObj["buyDiscURL"] == "" then
        btBuyCD.isVisible = false
    end
    

    local groupMusicList = display.newGroup()
    frame:insert(groupMusicList)
    groupMusicList.y = image.y + image.contentHeight*.5 + 10
    local verticalSpaceBetweenRows = 20

    local hasLyric = false
    for i=1,#albumObj.musics do

        local lbRowString = i
        if i < 10 then
            lbRowString = "0" .. lbRowString
        end
        local lbRow = display.newText{parent=groupMusicList, text=lbRowString, font=APP_CONFIG.fontLight, fontSize=16}
        lbRow.anchorX, lbRow.anchorY = 0, 0
        lbRow:setTextColor( 62/255,62/255,62/255 )        
        lbRow.x = image.x - image.contentWidth*.5
        lbRow.y = (i-1)*(lbRow.contentHeight + verticalSpaceBetweenRows)

        local horizontalSpaceBetweenElements = 4

        local btPlay
        local btPause

        local function onBtPlayPauseListener(e)
            
            if e.target.id == "btPlay" then
                btPlay.isVisible = false
                
                local spinner = AUX.showSpinner(btPlay)
                local sourceURL = albumObj.musics[i].previewURL

                if PLAYER.loadedSourceURL ~= nil and PLAYER.loadedSourceURL ~= sourceURL then
                    --print("music may be already playing. Let's stop it")
                    PLAYER.stop()
                end

                PLAYER.listener = function(e)
                    local phase = e.phase
                    --print("ppphase=", phase)
                
                    if phase == "started" then
                        display.remove(  spinner )
                        btPlay.isVisible = false
                        btPause.isVisible = true
                    
                    elseif phase == "ended" or phase == "stopped" or phase == "paused" then
                        print("endeddd")
                        btPlay.isVisible = true
                        btPause.isVisible = false                    
                    
                    end

                end
                
                -- incluido por Bruna em 19/10/2015 
                local eventFlurry = "view;" .. sceneSelected .. ";Play;" .. albumObj.title .. ";" .. albumObj.musics[i].title
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                
                PLAYER.play(sourceURL)

            else
                
                -- incluido por Bruna em 19/10/2015 
                local eventFlurry = "view;" .. sceneSelected .. ";Stop;" .. albumObj.title .. ";" .. albumObj.musics[i].title
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                
                PLAYER.stop()
            end



        end

        btPlay = require("library-widgets").newButton{
            id="btPlay",
            width=_G.IMAGES.discography.btPlay.width*1.5 + horizontalSpaceBetweenElements*2,
            height=lbRow.contentHeight,
            left = lbRow.x + lbRow.contentWidth,
            y = lbRow.y + lbRow.contentHeight*.5,

            imageFile = _G.IMAGES.discography.btPlay.path,
            imageOverFile = _G.IMAGES.discography.btPlayOver.path,
            imageWidth = _G.IMAGES.discography.btPlay.width,
            imageHeight = _G.IMAGES.discography.btPlay.height,

            backgroundColor = {0,0,0,0},
            onRelease = onBtPlayPauseListener,

        }
        groupMusicList:insert(btPlay)        

        btPause = require("library-widgets").newButton{
            id="btPause",
            width=_G.IMAGES.discography.btPause.width*1.5 + horizontalSpaceBetweenElements*2,
            height=lbRow.contentHeight,
            left = lbRow.x + lbRow.contentWidth,
            y = lbRow.y + lbRow.contentHeight*.5,
            imageFile = _G.IMAGES.discography.btPause.path,
            imageOverFile = _G.IMAGES.discography.btPauseOver.path,
            imageWidth = _G.IMAGES.discography.btPause.width,
            imageHeight = _G.IMAGES.discography.btPause.height,
            backgroundColor = {0,0,0,0},
            onRelease = onBtPlayPauseListener,
        }
        groupMusicList:insert(btPause)
        btPause.isVisible = false
        

        if albumObj.musics[i].previewURL == nil or albumObj.musics[i].previewURL == "" then 
            btPlay.isVisible = false
            btPause.isVisible = false
        end


        local btBuyMusic
        btH = lbRow.contentHeight

        if showButtonBuyMusic and albumObj.musics[i].buyMusicURL ~= nil and albumObj.musics[i].buyMusicURL ~= "" and albumObj.musics[i].buyMusicURL ~= false then 

            btBuyMusic = require("library-widgets").newButtonOLD{
               id="btBuyMusic",
               right = frameWidth - padding.right,
               y = lbRow.y + lbRow.contentHeight*.5,
               width=178*(320/1080)*1.4,
               height=btH,
               label = "COMPRAR FAIXA",
               labelColor = { default={ 31/255, 148/255, 241/255 }, over={ 0, 0, 0, 0.5 } },
               shape="rect",
               fillColor = { default={ 1,1,1, 1 }, over={ 31/255, 148/255, 241/255, 0.6 } },
               strokeWidth = 1,
               strokeColor = { default={ 31/255, 148/255, 241/255, 1 }, over={ 31/255, 148/255, 241/255, 0.6 } },
               font=APP_CONFIG["fontMedium"],
               fontSize=8,
               onEvent=btListener,
           }
           groupMusicList:insert(btBuyMusic)
           btBuyMusic.buyMusicURL = albumObj.musics[i].buyMusicURL           

       end
        
        local btLyricRightPos
        if btBuyMusic then
            btLyricRightPos = btBuyMusic.x - btBuyMusic.contentWidth*.5 - 10
        else
            btLyricRightPos = frameWidth - padding.right
        end        
       
        local btLyricW = 36
       
        local btLyric
        btLyric = require("custom-widget").newLyricButton{
            id="btLyric",
            width = btLyricW,
            height = btH,
            x = btLyricRightPos - btLyricW*.5,
            y = lbRow.y + lbRow.contentHeight*.5,      
            onEvent = btListener,                  
        }
        groupMusicList:insert(btLyric)
        btLyric.musicID = albumObj.barcode .. "-" .. i
        btLyric.musicTitle = albumObj.musics[i].title

        if albumObj.musics[i].hasLyric == false then
            btLyric.isVisible = false
        else
            hasLyric = true
        end
        
        local lbDuration
        if showButtonBuyMusic ~= true then

                local lbDurationString = "" 
                if albumObj.musics[i].duration.min and albumObj.musics[i].duration.min ~= "" and albumObj.musics[i].duration.sec and albumObj.musics[i].duration.sec ~= "" then
                    lbDurationString = albumObj.musics[i].duration.min .. ":" .. albumObj.musics[i].duration.sec
                end
                lbDuration = display.newText{parent=groupMusicList, text=lbDurationString, font=APP_CONFIG.fontMedium, fontSize=10}
                lbDuration.anchorX = 1
                lbDuration:setTextColor( 62/255,62/255,62/255 )        
                lbDuration.x = btLyric.x - btLyric.contentWidth*.5 - 4
                lbDuration.y = btLyric.y

        end


        local lbMusicNameLeft = btPlay.x + btPlay.contentWidth*.5 + 1
        local dX = lbMusicNameLeft  -- due to hiding play button
        if btPlay.isVisible == false then
            lbMusicNameLeft = btPlay.x - btPlay.contentWidth*.25
        end
        dX = math.abs(dX - lbMusicNameLeft)

        local lbMusicName = display.newText{parent=groupMusicList, text=albumObj.musics[i].title, font=APP_CONFIG.fontLight, fontSize=14}
        lbMusicName.anchorX, lbRow.anchorY = 0, 0
        lbMusicName:setTextColor( 62/255,62/255,62/255 )        
        lbMusicName.x = lbMusicNameLeft
        lbMusicName.y = btPlay.y

        local lbMusicNameMaxWidth
        if showButtonBuyMusic then
            lbMusicNameMaxWidth = (btLyric.x - btLyric.contentWidth*.5 - horizontalSpaceBetweenElements) - (lbMusicName.x)
        else
            lbMusicNameMaxWidth = (lbDuration.x - lbDuration.contentWidth - horizontalSpaceBetweenElements) - (lbMusicName.x)
        end
        lbMusicNameMaxWidth = lbMusicNameMaxWidth
        AUX.adjustTextSize(lbMusicName,lbMusicNameMaxWidth )


        if i < #albumObj.musics then
            
            local lineY = lbRow.y + lbRow.contentHeight + verticalSpaceBetweenRows*.5
            local lineRightPos = btLyric.x + btLyric.contentWidth*.5 
            if btBuyMusic then
                lineRightPos = btBuyMusic.x + btBuyMusic.contentWidth*.5  + 1
            end
            local line = display.newLine(lbRow.x,lineY, lineRightPos ,lineY)
            line.strokeWidth = 1
            line:setStrokeColor( 207/255, 207/255, 207/255 )
            groupMusicList:insert(line)        

        end

    end


   -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
   local invisibleRect = display.newRect(frame, 0,frame.contentHeight,frameWidth, 20)
   invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
   invisibleRect.alpha = 0



    return frame, hasLyric

end





f.radioEntry = function(albumObj, scrollViewObj)

    local frame = display.newGroup( )

    local frameWidth = 300 * GROW_WITH_SCREEN_W --(970/1080)*SCREEN_W
    local padding = {left=0, right=0,top = 0}


    local frameBackground = display.newRect(frame, frameWidth*.5, padding.top*.5, frameWidth, padding.top)    
    frameBackground.fill = {1,1,0,0}
    frame.background = frameBackground

    local image = display.loadImageFromInternet(frame, albumObj.image, isSizeEstimateCalculation)
    local scaleFactor = (251/1080)* SCREEN_W / image.contentWidth
    image:scale(scaleFactor, scaleFactor)
    image.x = padding.left + image.contentWidth*.5
    image.y = padding.top + image.contentHeight*.5
    frame._image = image
    

    
    local lbTitleLeft = image.x + image.contentWidth*0.5 + 30*(320/1080)
   
    local titleMaxWidth =  frameBackground.x + frameBackground.contentWidth*.5 - lbTitleLeft - 4
    local lbTitle = display.newText{text=albumObj.title, font=APP_CONFIG.fontMedium, fontSize=14}
    lbTitle.anchorX, lbTitle.anchorY = 0, 0
    lbTitle:setTextColor( 0,0,0 )        
    lbTitle.x = lbTitleLeft
    lbTitle.y = image.y - image.contentHeight*0.5
    frame:insert(lbTitle)
    AUX.adjustTextSize(lbTitle,titleMaxWidth )

    
    local lbArtist = display.newText{text=albumObj.artist , font=APP_CONFIG.fontLight, fontSize=12}
    lbArtist.anchorX, lbArtist.anchorY = 0, 0
    lbArtist:setTextColor( 0,0,0 )        
    lbArtist.x = lbTitle.x
    lbArtist.y = lbTitle.y + lbTitle.contentHeight + 2
    frame:insert(lbArtist)


    -- incluido por Bruna em 19/10/2015
    local tabSelected = "Rádios On-Line"
    local sceneSelected = "Rádios On-Line"
    
    
    local platform = system.getInfo( "platformName" )
    --print(platform)
    
    
    local deviceRadio
    
    if _G.DEVICE.isApple then
        
        deviceRadio = "iTunes"
        
    elseif _G.DEVICE.isGoogle then

        deviceRadio = "Google Play"       

    elseif _G.DEVICE.isKindleFire then
    
        deviceRadio = "Amazon"           

    elseif platform == "WinPhone" then
        
        deviceRadio = "Windows Phone"
        
    else
    
        -- any other DEVICE (windows, html,..) will end up here
        
    end
    
    
    
    

    local function btListener( event )
      
        local phase = event.phase
        local target = event.target

        if ( "ended" == phase ) then

            local id = event.target.id
            local url = nil
            --print("id= ", id)
            
            -- incluído por Bruna em 21/10/2015 para validar que botão foi clicado para enviar para o flurry
            local botaoRadio
            local parceiro = deviceRadio
            
            if id == "buyDigitalAlbumURL" then
                botaoRadio = "Comprar Álbum Digital"
            elseif id == "buyDiscURL" then
                botaoRadio = "Comprar CD"
                parceiro = "MK Shopping"
            elseif id == "previewURL" then
                botaoRadio = "Preview"
            end
            -- fim
            
            if id == "buyDigitalAlbumURL" or id == "buyDiscURL" or id == "previewURL" then
                url = albumObj[id]
                -- incluido por Bruna em 19/10/2015 
                local eventFlurry = "outClick;" .. sceneSelected ..";" .. tabSelected .. ";" .. botaoRadio .. ";" .. parceiro .. ";" .. albumObj.title .. ";"
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
            else
                if albumObj.radios then                
                    id = string.lower(id:sub(1,1)) .. id:sub(2)
                    parceiro = string.upper(id:sub(1,1)) .. id:sub(2)
                    print(parceiro)
                    url = albumObj.radios[id]
                    -- incluido por Bruna em 19/10/2015 
                    -- caso o usuário clique em um dos botões das rádios
                    local eventFlurry = "outClick;" .. sceneSelected ..";" .. tabSelected .. ";" .. parceiro .. ";" .. parceiro .. ";" .. albumObj.title .. ";"
                    print(eventFlurry)
                    _G.analytics.logEvent(eventFlurry)
                    -- fim
                end
            end
            
            if url then
                system.openURL(url)
            else
                print("WARNING - button is trying to open a URL that was not defined!")
            end
            


        elseif "moved" == phase then

         local dy = math.abs( event.y - event.yStart )
         if dy > 12 then
            if scrollViewObj then
                --print("going to give focus to SV")
                scrollViewObj:takeFocus( event )
            end
             
         end
       end

    end



    local btH = 32
    local btW = (titleMaxWidth - 4)/2

    local buttonTop = math.max(image.y + image.contentHeight*.5 - btH, lbArtist.y + lbArtist.contentHeight + 2)

    local btBuyAlbum = require("library-widgets").newButton{
       id="buyDigitalAlbumURL",  -- using the property of the albumObj received by the server (and used on class-album)
       left = lbTitle.x,
       top = buttonTop,
       width = btW, 
       height = btH,

       label = "COMPRAR\nÁLBUM DIGITAL",
       labelPadding = {top=1},
       labelLineSpacing = 4,
       labelColor = { 1,1,1 },        
       labelOverColor = { 0, 0, 0, 0.5 },

       backgroundColor = { 0,0,0, 1 }, 
       backgroundOverColor = { 0,0,0, .3 },

       labelFont = APP_CONFIG["fontMedium"],
       labelFontSize = 11,
       labelWrap = true,
       labelAlign = "center",
       onEvent = btListener,
    }
    frame:insert(btBuyAlbum)

    local btBuyCD = require("library-widgets").newButton{
        id="buyDiscURL",
        left =  btBuyAlbum.x + btBuyAlbum.contentWidth*.5 + 2,
        y = btBuyAlbum.y ,
        width = btW, 
        height = btH,
        label = "COMPRAR CD",
        labelPadding = {top=1},
        labelColor = { 1,1,1 }, 
        labelOverColor = { 0, 0, 0, 0.5 },
        backgroundColor = {184/255,0,16/255,1},
        backgroundOverColor = { 184/255,0,16/255,.3},
        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 11,
        labelWrap = true,
        labelAlign = "center",
        onEvent = btListener,
    }
    frame:insert(btBuyCD)

    --Thiago e Bruna só mostrar o botão quando houver link
    if albumObj.buyDigitalAlbumURL == "" then
        btBuyAlbum.isVisible = false
    end

    local lineY = btBuyAlbum.y + btBuyAlbum.contentHeight*.5 + 20
    local line = display.newLine(frame, frameBackground.x - frameBackground.contentWidth*.5, lineY, frameBackground.x + frameBackground.contentWidth*.5, lineY)
    line.strokeWidth = 1
    line:setStrokeColor( 200/255,200/255,200/255 )

   local lbListen = display.newText{parent=frame, x=frameBackground.x, y=line.y + 1, text="OUÇA AGORA NO", font=APP_CONFIG.fontMedium, fontSize=10, width=64,align="center"}
    lbListen:setFillColor( 37/255, 171/255, 254/255 )

    local lbBackground = display.newRect(frame, lbListen.x, lbListen.y, lbListen.contentWidth*1.1, lbListen.contentHeight)
    lbBackground:setFillColor( 1,1,1 )

    frame:insert(lbListen)



    local buttonsListen = {"Deezer", "Spotify", "Rhapsody", "MusicKey", "AppleMusic", "Rdio"}
    local groupButtons = display.newGroup()
    frame:insert(groupButtons)

    local i = 1
    for c=1, #buttonsListen do

        if albumObj.radios and albumObj.radios[string.lower(buttonsListen[c])] and albumObj.radios[string.lower(buttonsListen[c])] ~= "" then 

            local imageObj = APP_CONFIG.images.radios["bt"..buttonsListen[c]]
            


            local btX = imageObj.width*.5 * GROW_WITH_SCREEN_W + ((i-1)%3)*(imageObj.width* GROW_WITH_SCREEN_W + 2)
            local btY = imageObj.height*.5 * GROW_WITH_SCREEN_W
            if i > 3 then
                btY = btY + imageObj.height * GROW_WITH_SCREEN_W + 2
            end


            local btListen = require("widget").newButton{
                id = buttonsListen[c],
                x = btX,
                y = btY,
                defaultFile = imageObj.path,
                overFile = APP_CONFIG.images.radios["bt"..buttonsListen[c].."Over"].path,
                width = imageObj.width * GROW_WITH_SCREEN_W,
                height = imageObj.height * GROW_WITH_SCREEN_W,
                onEvent = btListener,
            }
            groupButtons:insert(btListen)

            i=i+1
        end
    end

    groupButtons.x = image.x - image.contentWidth*.5
    groupButtons.y = lineY + 20



    local lineY = groupButtons.y + groupButtons.contentHeight + 20
    local line = display.newLine(frameBackground.x - frameBackground.contentWidth*.5,lineY, frameBackground.x + frameBackground.contentWidth*.5 ,lineY)
    line.strokeWidth = 1
    line:setStrokeColor( 207/255, 207/255, 207/255 )
    frame:insert(line)

    frame.line = line

    -- resizing background
    frameBackground.height = frameBackground.height + frame.contentHeight
    frameBackground.y = frameBackground.contentHeight*.5


    
    return frame

end

f.previewEntry = function(albumObj)
    --print("entrou na cena Previews")
    local frame = display.newGroup( )

    local frameHeight = 130 * (SCREEN_H / 570)

    local frameWidth = (970/1080)*SCREEN_W
    local padding = {left=0, right=0,}


    local frameBackground = display.newRect(frame, frameWidth*.5, frameHeight*.5, frameWidth, frameHeight)
    frameBackground:setFillColor( 1,1,1)
    frame.background = frameBackground
    frameBackground.isHitTestable = true

    local image = display.loadImageFromInternet(frame, albumObj.image)
    local scaleFactor = (251/1080)* SCREEN_W / image.contentWidth
    image:scale(scaleFactor, scaleFactor)
    image.x = padding.left + image.contentWidth*.5
    image.y = frameBackground.y
    frame._image = image
    

    local btPlay = require("custom-widget").newTapButton{
        width=_G.IMAGES.preview.play.width + 4,
        height=_G.IMAGES.preview.play.height,
        left = image.x + image.contentWidth*.5 + 8,
        top = image.y - image.contentHeight*.5,
        imageFilename = _G.IMAGES.preview.play.path,
        imageWidth = _G.IMAGES.preview.play.width,
        imageHeight = _G.IMAGES.preview.play.height,
        fillColor = {0,0,0,0},
        --onTap = function() print("aa") end
    }
    frame:insert(btPlay)

    
    local lbTitleLeft = btPlay.x + btPlay.contentWidth*0.5 + 8
   
    local titleMaxWidth =  frameWidth - lbTitleLeft - 4
    local lbMusicName = display.newText{text=albumObj.musics[1].title, font=APP_CONFIG.fontMedium, fontSize=14, width=titleMaxWidth}
    lbMusicName.anchorX, lbMusicName.anchorY = 0, 0
    lbMusicName:setTextColor( 0,0,0 )        
    lbMusicName.x = lbTitleLeft
    lbMusicName.y = btPlay.y - btPlay.contentHeight*0.3
    frame:insert(lbMusicName)


    --local lbAlbum = display.newText{text=albumObj.title , font=APP_CONFIG.fontLight, fontSize=12}
    local lbAlbum = display.newText{text=albumObj.title , font=APP_CONFIG.fontLight, fontSize=12, width=frameWidth - lbTitleLeft - 4}
    lbAlbum.anchorX, lbAlbum.anchorY = 0, 0
    lbAlbum:setTextColor( 0,0,0 )        
    lbAlbum.x = lbMusicName.x
    lbAlbum.y = lbMusicName.y + lbMusicName.contentHeight + 10
    frame:insert(lbAlbum)

    
    local lbArtist = display.newText{text=albumObj.artist , font=APP_CONFIG.fontLight, fontSize=12}
    lbArtist.anchorX, lbArtist.anchorY = 0, 0
    lbArtist:setTextColor( 0,0,0 )        
    lbArtist.x = lbAlbum.x
    lbArtist.y = lbAlbum.y + lbAlbum.contentHeight + 2
    frame:insert(lbArtist)


    local lineY = frame.contentHeight
    local line = display.newLine(frame, frame.x,lineY, frame.x + frame.contentWidth ,lineY)
    line.strokeWidth = 1
    line:setStrokeColor( 207/255, 207/255, 207/255 )
    frame._line = line
    

    return frame

end





f.directFromArtistEntry = function(socialMediaObj, scrollViewObj)

        local frame = display.newGroup( )
        --frame.anchorChildren = true


        local frameWidth = (300/320)*SCREEN_W
        local padding = {top=10, left=10, right=10, bottom=10}
        local frameContentWidth = frameWidth - padding.left - padding.right


        local background = display.newRect(frame, frameWidth*.5, 1, frameWidth, 2)
        background:setFillColor( 1,1,1 )
        background.strokeWidth = 1
        background:setStrokeColor(163/255,163/255,163/255)        


        local socialNetworkName = socialMediaObj.network
        local socialMediaIcon = display.loadImage(APP_CONFIG.images.directFromArtist[string.lower(socialNetworkName)])
        socialMediaIcon.x = padding.left + socialMediaIcon.contentWidth*.5
        socialMediaIcon.y = padding.top + socialMediaIcon.contentHeight*.5
        frame:insert(socialMediaIcon)

        local lbDirectFrom = display.newText{text="Direto do ", font=APP_CONFIG.fontLight, fontSize=16}
        lbDirectFrom.anchorX, lbDirectFrom.anchorY = 0, .5
        lbDirectFrom:setTextColor( 0,0,0 )        
        lbDirectFrom.x = socialMediaIcon.x + socialMediaIcon.contentWidth*.5 + padding.left        
        lbDirectFrom.y = socialMediaIcon.y
        frame:insert(lbDirectFrom)

        local lbSocialMediaString
        local lbSocialMediaColor
        if socialNetworkName == "facebook" then
            lbSocialMediaString = "Facebook"
            lbSocialMediaColor = {0,0, 121/255}
        elseif socialNetworkName == "twitter" then
            lbSocialMediaString = "Twitter"
            lbSocialMediaColor = {40/255, 174/255, 213/255}
        elseif socialNetworkName == "instagram" then
            lbSocialMediaString = "Instagram"
            lbSocialMediaColor = {105/255, 47/255, 29/255}
        end


        local lbSocialMedia = display.newText{text=lbSocialMediaString, font=APP_CONFIG.fontMedium, fontSize=16}
        lbSocialMedia.anchorX, lbSocialMedia.anchorY = 0, .5
        lbSocialMedia:setTextColor(unpack(lbSocialMediaColor)) 
        lbSocialMedia.x = lbDirectFrom.x + lbDirectFrom.contentWidth
        lbSocialMedia.y = lbDirectFrom.y
        frame:insert(lbSocialMedia)


        local lineY = socialMediaIcon.y + socialMediaIcon.contentHeight*.5 + padding.top
        local line = display.newLine(socialMediaIcon.x - socialMediaIcon.contentWidth*.5,lineY, frameWidth - padding.right,lineY)
        line.strokeWidth = 1
        line:setStrokeColor( 240/255, 240/255, 240/255 )
        frame:insert(line)



        local posts = socialMediaObj.posts

        for i=1, #posts do

            local imgProfile = display.loadImageFromInternet(frame, {url=socialMediaObj.profileAvatarURL, width=26, height=26})
            imgProfile.x = padding.left + imgProfile.contentWidth*.5
            imgProfile.y = lineY + padding.top + imgProfile.contentHeight*.5

            local lbProfileName = display.newText{text=socialMediaObj.profileName, font=APP_CONFIG.fontMedium, fontSize=12}
            lbProfileName.anchorX, lbProfileName.anchorY = 0, 0
            lbProfileName:setTextColor(30/255,30/255,30/255)
            lbProfileName.x = imgProfile.x + imgProfile.contentWidth*.5 + padding.left
            lbProfileName.y = imgProfile.y - imgProfile.contentHeight*.5
            --lbProfileName.width = frameWidth - padding.right - lbProfileName.x
            --lbProfileName.height = 30

            --print ("---------")
            --print (lbProfileName.width)

            frame:insert(lbProfileName)

            local previousContentHeight = lbProfileName.contentHeight

            if socialMediaObj.profileHandler then
                local lbProfileHandler = display.newText{text= "("..socialMediaObj.profileHandler..")", font=APP_CONFIG.fontLight, fontSize=12}
                lbProfileHandler.anchorX, lbProfileHandler.anchorY = 0, 0
                lbProfileHandler:setTextColor(30/255,30/255,30/255)
                lbProfileHandler.x = lbProfileName.x --+ lbProfileName.contentWidth + 2
                lbProfileHandler.y = lbProfileName.y + lbProfileName.contentHeight + 2
                frame:insert(lbProfileHandler)

                previousContentHeight = previousContentHeight + lbProfileHandler.contentHeight

            end

            local lbDateString = AUX.formatDatetime(posts[i].datetime)
            local lbDate = display.newText{text=lbDateString, font=APP_CONFIG.fontLight, fontSize=10}
            lbDate.anchorX, lbDate.anchorY = 0, 0
            lbDate:setTextColor(30/255,30/255,30/255)
            lbDate.x = lbProfileName.x
            lbDate.y = lbProfileName.y + previousContentHeight + 2
            frame:insert(lbDate)


            local lbPostContentMaxWidth = frameWidth - padding.right - lbProfileName.x            
            -- local lbPostTemp = display.newText{text=posts[i].content or "", font=native.systemFont, fontSize=14, width=lbPostContentMaxWidth}
            -- local lbPostContentH = lbPostTemp.contentHeight + 10
            -- display.remove(lbPostTemp)
            local lbPostContent = display.newText2{text=posts[i].content or "", spaceBetweenLines = 3, font=APP_CONFIG.fontLight, fontSize=14, width=lbPostContentMaxWidth, color={.2,.2,.2}}
            lbPostContent.anchorX, lbPostContent.anchorY = 0, 0
            --lbPostContent:setTextColor(30/255,30/255,30/255) 
            lbPostContent.x = lbProfileName.x
            lbPostContent.y = lbDate.y + lbDate.contentHeight + 4
            frame:insert(lbPostContent)

            -- local lbPostContent = native.newTextBox(-500,-500,lbPostContentMaxWidth,lbPostContentH)            
            -- lbPostContent.font = native.newFont( native.systemFont, 12 )
            -- lbPostContent.text=  posts[i].content
            -- lbPostContent.anchorX, lbPostContent.anchorY = 0, 0
            -- lbPostContent:setTextColor(30/255,30/255,30/255) 
            -- lbPostContent.x = lbProfileName.x
            -- lbPostContent.y = lbDate.y + lbDate.contentHeight + 4            
            -- lbPostContent.hasBackground = false
            -- lbPostContent.isEditable = false            
            -- frame:insert(lbPostContent)
            
            



            lineY = lbPostContent.y + lbPostContent.contentHeight + padding.top


            local postImageMaxHeight = 50

            local imageFilename = nil
            if posts[i].image and posts[i].image ~= "" and posts[i].image.url and posts[i].image.url ~= "" then

                local imageURL = posts[i].image.url
                
                local imageHeight = postImageMaxHeight
                local imageWidth = imageHeight
                if posts[i].image.width then
                    posts[i].image.width = tonumber(posts[i].image.width)
                    imageWidth = math.min(posts[i].image.width, lbPostContentMaxWidth)
                    imageHeight = posts[i].image.height * ( imageWidth / posts[i].image.width)
                end

                local imgPost = display.loadImageFromInternet(frame, {url =  imageURL , width = imageWidth, height = imageHeight})
                imgPost.x = lbPostContent.x +  imgPost.contentWidth*.5
                imgPost.y = lbPostContent.y + lbPostContent.contentHeight + imgPost.contentHeight*.5 + 10
                
                imgPost.contentHeight = tonumber(imgPost.contentHeight)
                lineY = imgPost.y + imgPost.contentHeight*.5 + padding.top

                imageFilename = _G.AUX.getFilenameFromURL(imageURL)
            end

            local shareTo = "Compartilhar"
 
            local line = display.newLine(socialMediaIcon.x - socialMediaIcon.contentWidth*.5,lineY, frameWidth - padding.right,lineY)
            line.strokeWidth = 1
            line:setStrokeColor( 240/255, 240/255, 240/255 )
            frame:insert(line)

            local btShare = require("custom-widget").newShareButton{
                top=lineY,
                right = frameWidth - padding.right,
                --width = 160,
                height = 30,
                useBlackColor = true,
                shareMessage = posts[i].content,
                shareURL = posts[i].webURL,
                shareImageFilenameOrFunction = imageFilename,
                scrollViewParent = scrollViewObj,
                flurry = "outClick;Direto do Artista;" .. lbSocialMediaString .. ";Compartilhar;" .. shareTo .. ";" .. socialMediaObj.profileAvatarURL .. ";"
            }
            frame:insert(btShare)


            local btAccessOriginal = require("library-widgets").newButton{
                y=btShare.y,
                right = btShare.x - btShare.contentWidth*.5 - 20,
                --width = 160,
                height = 30,
                backgroundColor={1,0,0,0},
                label="ACESSAR O " .. string.upper(socialNetworkName) .. " OFICIAL", 
                labelFontSize = 8,
                labelFont = APP_CONFIG.fontMedium,
                labelColor = {30/255,30/255,30/255},                
                labelOverColor = {30/255,30/255,30/255, .3},
       
                imageFile = APP_CONFIG.images.icons.share.path,
                imageWidth = APP_CONFIG.images.icons.share.width,
                imageHeight = APP_CONFIG.images.icons.share.height,
                imageColor = {30/255,30/255,30/255},
                imageOverColor = {30/255,30/255,30/255, .3},
                imagePosition="right",
                imagePadding = {left = 4},
                        
                onEvent = function(event)

                    local phase = event.phase
                    local target = event.target

                    if ( "ended" == phase ) then

                        if posts[i].webURL then
                            system.openURL( posts[i].webURL )
                        end
                        

                    elseif "moved" == phase then

                         local dy = math.abs( event.y - event.yStart )

                         if dy > 12 then
                            if scrollViewObj then
                                --print("going to give focus to SV")
                                scrollViewObj:takeFocus( event )
                            end
                             
                         end
                   end

                end
            }
            frame:insert(btAccessOriginal)
            if posts[i].webURL == nil or posts[i].webURL == "" then
                btAccessOriginal.isVisible = false
            end
            
        end

            


        -- local lbDate = display.newText{text=newsObj.date.day.."/"..newsObj.date.month.."/"..newsObj.date.year, font=APP_CONFIG.fontLight, fontSize=12, width = frameContentWidth}
        -- lbDate.anchorX, lbDate.anchorY = 0, 0
        -- lbDate:setTextColor( 0,0,0 )        
        -- lbDate.x = padding.left
        -- lbDate.y = frame.contentHeight + padding.top
        -- frame:insert(lbDate)

        -- local lbTitle = display.newText{text=newsObj.title, font=APP_CONFIG.fontLight, fontSize=16, width = frameContentWidth}
        -- lbTitle.anchorX, lbTitle.anchorY = 0, 0
        -- lbTitle:setTextColor( 0,0,0 )        
        -- lbTitle.x = padding.left        
        -- lbTitle.y = frame.contentHeight + padding.top
        -- frame:insert(lbTitle)

        -- local lbSummary = display.newText{text=newsObj.summary, font=APP_CONFIG.fontLight, fontSize=14, width = frameContentWidth}
        -- lbSummary.anchorX, lbSummary.anchorY = 0, 0
        -- lbSummary:setTextColor( 62/255,62/255,62/255 )        
        -- lbSummary.x = padding.left        
        -- lbSummary.y = frame.contentHeight + padding.top
        -- frame:insert(lbSummary)


        --   local btSeeMore = require("library-widgets").newArrowButton{
        --     top=frame.contentHeight,

        --     fillColor={1,0,0,0},
        --     label="VER NOTÍCIA COMPLETA", 
        --     labelColor = {0,162/255,1},
        --     labelOverColor = {0,162/255,1,.3},
        --     arrowFillColor = {0,162/255,1},
        --     arrowOverColor = {0,162/255,1,.3},
            
        --     height = 30,
        --     right = frameWidth - padding.right,
        --     fontSize=10,            
        --     onTap = function() btSeeMoreHandler(newsObj) end, --function() print("tap") end


        -- }
        -- frame:insert(btSeeMore)
        
        background.height = frame.contentHeight + padding.bottom
        background.y = background.height*.5

        -- -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
        -- local invisibleRect = display.newRect(frame, 0,frame.contentHeight + background.strokeWidth,frame.contentWidth, 20)
        -- invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
        -- invisibleRect.alpha = 0


        return frame

    end




-- group that shows a spinner and a label when the user pull down the scrollview in order to refresh it
f.newRefreshingGroup = function()

    local groupRefreshing = display.newGroup()

    local backgroundH = 70
    local background = display.newRect(groupRefreshing, SCREEN_W*.5, backgroundH*.5, SCREEN_W, backgroundH)
    background:setFillColor(226/255,226/255,226/255,1)
    
    
    local verticalMargin = 10
    local spinnerH = 20


    local spinner = AUX.showLoadingAnimation(SCREEN_W*.5, verticalMargin + spinnerH*.5, spinnerH)
    groupRefreshing:insert(spinner)
    groupRefreshing.spinner = spinner


    local lbRefreshing = display.newText{parent=groupRefreshing, text="Carregando...",  font=APP_CONFIG.fontLight, fontSize=16}
    lbRefreshing:setTextColor(75/255,75/255,75/255)
    lbRefreshing.x, lbRefreshing.y = spinner.x, spinner.y + spinner.contentHeight*.5 + lbRefreshing.contentHeight*.5 + 4

    background.height = lbRefreshing.y + lbRefreshing.contentHeight*.5 + verticalMargin
    background.y = background.height*.5

    print("groupRefreshing.ch=", groupRefreshing.contentHeight)

    return groupRefreshing

end


return f


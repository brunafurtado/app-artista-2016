local photo = {}


	photo.new = function(params)

		local obj = {}

		obj.date = params.date		-- string or table {day, month, year}
		obj.image = params.image  	-- image={url=, width, height=}  -- this is a thumbnail image
		obj.title = params.title
		obj.images = params.images	-- list of images  {thumbnail={url, width, height},  full={url, width, height}}

		return obj

	end


return photo
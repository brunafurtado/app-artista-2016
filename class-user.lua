local user = {}

user.loggedUser = nil

user.new = function(id, firstName, lastName, isProfessional)
    
    user.id = id
    user.firstName = firstName
    user.lastName = lastName
    user.isProfessional = isProfessional

    
    user.loggedUser = user
    
    return user
end



user.getLoggedUser = function()
    
    return user.loggedUser
    
end


return user


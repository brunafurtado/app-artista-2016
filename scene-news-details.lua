local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------
local tableView = nil



-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.news

    local topBar = (event.params and event.params.topBar) or require("module-topBar").new(sceneGroup, "Notícias")
    sceneGroup.topBar = topBar

    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}
    

   local btBack = require("library-widgets").newButton{
        left = 8,
        top = topBar.y + topBar.contentHeight,
        --width = 380*(320/1080), 
        height=30,
        
        label = "VOLTAR",
        labelColor = {0,162/255,1},
        labelOverColor = {0,162/255,1,.3},
        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 10,
        
        imageFile = APP_CONFIG.images.icons["arrowLeft"].path,
        imageWidth = APP_CONFIG.images.icons["arrowLeft"].width,
        imageHeight = APP_CONFIG.images.icons["arrowLeft"].height,
        imagePosition = "left",
        imagePadding = {right=4, bottom=2},

        imageColor = {0,162/255,1},
        imageOverColor = {0,162/255,1,.3},

        backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
        backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

        strokeWidth = 1,
        strokeColor = { 31/255, 148/255, 241/255, 1 },
        strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

        onRelease = function() print("tap"); 

            display.getCurrentStage( ):insert(sceneGroup.topBar) 
            composer.setVariable( "temp_topBar", sceneGroup.topBar )

            _G.rbBack.goToBackScene(); 

        end
    }
    sceneGroup:insert(btBack)


    sceneGroup.showNewsDetails = function(newsObj)

         local frame = display.newGroup( )
        --frame.anchorChildren = true


        local frameWidth = SCREEN_W - (35/3)*2
        local padding = {top=10, left=10, right=10, bottom=10}
        local frameContentWidth = frameWidth - padding.left - padding.right


        local background = display.newRect(frame, frameWidth*.5, 1, frameWidth, 2)
        background:setFillColor( 1,1,1 )
        background.strokeWidth = 1
        background:setStrokeColor(163/255,163/255,163/255)        

        local lbTitle = display.newText2{text=newsObj.title, font=APP_CONFIG.fontLight, fontSize=16, width = frameContentWidth, color={0,0,0}}
        --lbTitle.anchorX, lbTitle.anchorY = 0, 0
        --lbTitle:setTextColor( 0,0,0 )        
        lbTitle.x = padding.left        
        lbTitle.y = frame.contentHeight + padding.top
        frame:insert(lbTitle)

        local lbDate = display.newText{text=newsObj.date.day.."/"..newsObj.date.month.."/"..newsObj.date.year, font=APP_CONFIG.fontLight, fontSize=12, width = frameContentWidth}
        lbDate.anchorX, lbDate.anchorY = 0, 0
        lbDate:setTextColor( 0,0,0 )        
        lbDate.x = padding.left
        lbDate.y = frame.contentHeight + padding.top
        frame:insert(lbDate)

        local lineY = frame.contentHeight + padding.top
        local line = display.newLine(0,lineY, frameContentWidth, lineY)
        line.strokeWidth = 1
        line.x = CENTER_X - frameWidth*.5
        line:setStrokeColor( 0,0,0,.1 )
        --line:setStrokeColor( 0,1,0 )
        frame:insert(line)


        if newsObj.image then
            local image = display.loadImageFromInternet(newsObj.image, isSizeEstimateCalculation)
            local scaleFactor = math.min(frameContentWidth / image.contentWidth,1)
            image:scale(scaleFactor, scaleFactor)
            image.x = background.x
            image.y = frame.contentHeight + padding.top*.5 + image.contentHeight*.5
            frame:insert(image)
            print("frame=", frame)
            frame._image = image

        end


        
        
        local lbContent = display.newText2{text=newsObj.content, spaceBetweenLines = 3, spaceBetweenWords = 4, font=APP_CONFIG.fontLight, fontSize=14, width = frameContentWidth, color={62/255,62/255,62/255}}
        --lbContent.anchorX, lbContent.anchorY = 0, 0
        --lbContent:setTextColor( 62/255,62/255,62/255 )        
        lbContent.x = padding.left        
        lbContent.y = frame.contentHeight + padding.top
        frame:insert(lbContent)


        lineY = frame.contentHeight + padding.top*2
        local line = display.newLine(0,lineY, frameContentWidth, lineY)
        line.strokeWidth = 1
        line.x = CENTER_X - frameWidth*.5
        line:setStrokeColor( 0,0,0,.1 )
        frame:insert(line)

        -- Adicionado por Bruna em 23/10/2015 
        -- nome do app cujo item será compartilhado
        local shareTo = "Compartilhar"
        
        local shareMessage = newsObj.title .. " - Veja mais em " .. (newsObj.webURL or APP_CONFIG.artistSite)
        local btShare = require("custom-widget").newShareButton{
            parent = frame,
            right = frameWidth - 9,
            top = frame.contentHeight + padding.top*.5,            
            shareMessage = shareMessage,
            shareURL = false,
            useBlackColor = true,
            flurry = "outClick;Notícia;Notícia;Compartilhar;" .. shareTo .. ";" .. event.params.newsObj.title .. " - " .. event.params.newsObj.webURL .. ";",
        }



        background.height = frame.contentHeight + padding.top*.5
        background.y = background.height*.5

        -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
        local invisibleRect = display.newRect(frame, 0,frame.contentHeight + background.strokeWidth,frameWidth, 20)
        invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
        invisibleRect.alpha = 0

        frame.x = CENTER_X - frameWidth*.5
        frame.y = 2 --btBack.y + btBack.contentHeight*.5



        local scrollViewTop = btBack.y + btBack.contentHeight*.5 + 2
        local scrollViewH = SCREEN_H - scrollViewTop
        local shouldDisableSrollView = ( frame.contentHeight < ( scrollViewTop))

          -- creates the scrollview
        local scrollView = require("widget").newScrollView{
           x = CENTER_X,
           top = scrollViewTop,
           width = SCREEN_W,
           height = scrollViewH,    
           hideBackground = true,  
           hideScrollBar = true, 
           horizontalScrollDisabled = true,
           verticalScrollDisabled = shouldDisableSrollView,
           bottomPadding = 20
        }  
        sceneGroup:insert(scrollView)
        scrollView:insert(frame)

    end
  
     
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
        _G.rbBack.addBack()
        
        sceneGroup.showNewsDetails((event.params and event.params.newsObj) or { image={url="https://s3-sa-east-1.amazonaws.com/redbeach-mk/news/image1%403x.png", width=100, height=100}, title= "A da Bruna Karla ultrapassa 500 mil views", summary="Ipsum Lorem dfsdfdsfds sdfsdflkgjklfs gfdgsflkjgfldkjgdf dslkgjflkdjgoifd", date = {day=13, month=2, year=2015}, content='O clipe "Deixar a Lágrima Rolar" já ultrapassou 2 milhões de visualizações no canal oficial da MK Music no Youtube, com menos de um ano no ar!\n\nAs cenas foram gravadas em Orlando (EUA). Bruna e toda a produção enfrentam o frio para a gravação deste lindo louvor, que conta com a direção de Dayana Andrade e roteiro de Alomara Andrade.'})

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        _G.rbBack.addBack()

        sceneGroup.topBar = (event.params and event.params.topBar) or require("module-topBar").new(sceneGroup, "Notícias")
        sceneGroup:insert(sceneGroup.topBar)

        _G.MENU.highlightMenuWithLabel("Notícias")
        
        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Notícia;Notícia;" .. event.params.newsObj.title .. ";" .. event.params.newsObj.webURL .. ";"
        print(eventFlurry)
        _G.analytics.logEvent( eventFlurry )
        -- fim
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
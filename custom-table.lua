
-- merge 2 tables, by appending the tableToAppends elements to the end of the tableMasterObj
table.appendTable = function(tableMasterObj, tableToAppend)
    for i=1, #tableToAppend do
        tableMasterObj[#tableMasterObj+1] = tableToAppend[i]
    end
end


table.mergeTablesOrderedByDate = function(table1, table2)

    if table1 ~= nil and table2 == nil then return table1 end
    if table1 == nil and table2 ~= nil then return table2 end


    local mergedTable = {}


    local j,k = 1,1
    for i=1, #table1 + #table2 do

        if j > #table1 then
            for z=k, #table2 do 
                mergedTable[#mergedTable+1] = table2[z]
            end
            break   

        elseif k > #table2 then
            for z=j, #table1 do 
                mergedTable[#mergedTable+1] = table1[z]
            end
            break   
        elseif os.time(table1[j].date) > os.time(table2[k].date) then
            mergedTable[#mergedTable+1] = table1[j]
            j=j+1

        else
            mergedTable[#mergedTable+1] = table2[k]
            k=k+1
        end

    end

    return mergedTable

end



--  https://dev.twitter.com/oauth/reference/post/oauth2/token

local tw = {}

local host = "https://api.twitter.com/"



local bearerToken = nil



-- function that gets a JSON from a server
local function getJSON(endpoint, method, paramHeaders, paramBody, onCompleteDownload )

    local url = host .. (endpoint or "")

    
    -- converts a table from to a string format
    local paramToString = function(paramsTable)
        local str = ""
        local i = 1
        
        for paramName,paramValue in pairs(paramsTable) do 
            --print(paramName .. ": " .. paramValue)
            if type(paramValue) == "boolean" then
            	paramValue = tostring(paramValue)
            end
            if i == 1 then
                str = paramName .. "=" .. paramValue
            else
                str = str .. "&" .. paramName .. "=" .. paramValue
            end
            i=i+1
        end
        
        return str
    end 
    



    -- request listener
    local function networkListener( event )
	
        local result, data, errorMessage = false, nil, nil

        if ( event.isError or event.status ~= 200) then                            
                print( "Network error! - status=", event.status, " - response:", event.response)

                errorMessage = "Houve um problema ao tentar se comunicar com o servidor"

        else
            --print("Network ok. Now let's decode the JSON")
            
            local response = event.response
			--print("response=", response)
				
            data = require("json").decode(response)

            if type(data) ~= "table" then
                errorMessage = "Dados enviados pelo servidor estão com formato JSON inválido"
            else
                result = true          
            end

        end
        onCompleteDownload(data, event.response)
    end






	-- Add any extra header fixed parameter here
	local headers = paramHeaders or {}
    headers["Content-Type"] = "application/x-www-form-urlencoded"
 	--headers["key"] = value


    -- Add any extra body fixed parameter here
    local body = paramBody or {}
    --body["key"] = value

    local bodyString = paramToString(body)



    -- wrapping the headers/body parameters
	local requestParams = {}
	requestParams.headers = headers

	if method == "POST" then        
        requestParams.body = bodyString
    else        
        url = url .. "?" .. bodyString
    end


    -- making the request
    network.request( url, method, networkListener, requestParams)

end


local function createEncodedKeys(consumerKey, consumerSecret)

	local encodedKey = require("socket.url").escape(consumerKey)

	local encodedSecret = require("socket.url").escape(consumerSecret)

	local bearerTokenCredential = encodedKey .. ":" .. encodedSecret

	return require("mime").b64(bearerTokenCredential)

end


local function getBearerToken(consumerKey,consumerSecret, onCallback)

	local headers = {}
    headers["Authorization"] = "Basic " .. createEncodedKeys(consumerKey,consumerSecret)  
    
    local body = {
    	grant_type = "client_credentials"
    }



    getJSON("oauth2/token", "POST", headers, body, function(returnData) 
   		
   		local result = false

   		if returnData and returnData["token_type"] and returnData["token_type"] == "bearer" and returnData["access_token"] then
   			bearerToken = returnData["access_token"]
   			result = true
   		end
        if onCallback then
   		   onCallback(result)
        end

	end)

end


local twitterRequest = function(args)
		
    if bearerToken == nil then
        if args.callback then
            args.callback(nil, "You need to call 'init' before using 'getTweets'")
        end
    end

	-- receiving args
	local headers = { Authorization = "Bearer " .. bearerToken }
    local method = args.method or "GET"
    local body = args.parameters
    local endpoint = "/1.1/" .. args.endpoint .. ".json"
    local callback = args.callback






    getJSON(endpoint, method, headers, body, function(data, response)          
        --print("a=", args.endpoint)
    		if callback then
    			callback(response)

    		end
	end)


end




-- PUBLIC FUCTIONS
tw.init = getBearerToken
	
tw.request = twitterRequest

return tw

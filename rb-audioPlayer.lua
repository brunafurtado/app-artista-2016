local audioPlayer = {}

local player = nil
local timerID = nil


local sourceURL = nil
audioPlayer.loadedSourceURL = nil

local hasEnded = false
local isPlaying = false


-- listener
audioPlayer.listener = nil
local onLoaded = nil



 local function playerListener( event )
    print( "Event phase: " .. event.phase )    
	local params = {}
	if event.errorCode then
		if timerID then timer.cancel(timerID) end
		params = {
    				phase="canceled",
    				currentTime = player.currentTime,
    				totalTime = player.totalTime,
    				isMuted = player.isMuted,
    				isError = true,
    				errorCode = event.errorCode,
    				errorMessage = event.errorMessage,
    	}
		--native.showAlert( "Error!", event.errorMessage, { "OK" } )
        
    else

	 	if event.phase == "ended" then
	 		if timerID then timer.cancel(timerID) end
			params = {	phase="ended",
	    				currentTime = player.totalTime, -- on Android, the currentTime at ended phase is not identical to the totalTime
	    				totalTime = player.totalTime,
	    				isMuted = player.isMuted,	    				
	    	}
	    	hasEnded = true
	    	isPlaying = false

	    elseif event.phase == "ready" then

	    	params = {
	    				phase="loaded",
	    				currentTime = player.currentTime,
	    				totalTime = player.totalTime,
	    				isMuted = player.isMuted,
	    	}

	    	audioPlayer.loadedSourceURL = sourceURL	

	    	if onLoaded then
	    		timer.performWithDelay( 10, function()  -- adding a timer to allow the audioPlayer.listener run before this onLoaded function
	    			onLoaded(params)
	    		end)
	    	end    		    	
	    end

	end
    if audioPlayer.listener then
   		audioPlayer.listener(params)
    end

    
end


audioPlayer.load = function(audioSourceURL, onLoadedAudio)
print("audioSourceURL=", audioSourceURL)
	
	if audioSourceURL == nil or audioSourceURL == "" then error("rb-audioPlayer: no audio source specified to load") end
	
	audioSourceURL = string.gsub (audioSourceURL, "\r\n", "")

	if player == nil then
	  	player = native.newVideo( -200, -200, 50, 50 )   -- creating an object outside of the screen
	    player:addEventListener( "video", playerListener )
	end

	sourceURL = audioSourceURL
	onLoaded = onLoadedAudio

    player:load( sourceURL, media.RemoteSource )	


end

audioPlayer.play = function(audioSourceURL)

	if audioSourceURL == nil or audioSourceURL == "" then error("rb-audioPlayer: no audio source specified to play") end

	audioSourceURL = string.gsub (audioSourceURL, "\r\n", "")

	if player and audioPlayer.loadedSourceURL == audioSourceURL then	
		local currentTime = player.currentTime
		if hasEnded then
			player:seek(0)
			currentTime = 0
			hasEnded = false
		end

		player:play()
		isPlaying = true
		if audioPlayer.listener then
			audioPlayer.listener({
						phase="started",
						currentTime = currentTime,
						totalTime = player.totalTime,
						isMuted = player.isMuted,
			})


			local numOfIterations = (player.totalTime - currentTime)*1000 / 500
			timerID = timer.performWithDelay( 500, function()

				audioPlayer.listener({
						phase="progress",
						currentTime = player.currentTime,
						totalTime = player.totalTime,
						isMuted = player.isMuted,
						})


				end, numOfIterations )
		end
	else

		audioPlayer.load(audioSourceURL, function() audioPlayer.play(audioSourceURL) end)

	end
	
	

end


audioPlayer.seek = function(time)

	player:seek(time)

end


audioPlayer.pause = function(isStop)
	
	if player == nil then return  end

	player:pause()
	isPlaying = false
	if timerID then timer.cancel(timerID) end
	if audioPlayer.listener then
		local phase = (isStop and "stopped") or "paused"
			audioPlayer.listener({
						phase=phase,
						currentTime = player.currentTime,
						totalTime = player.totalTime,
						isMuted = player.isMuted,
			})
	end

end


-- always call audioPlayer.stop(true) on scene hide() in order to save memory
audioPlayer.stop = function(discardObj)

	if player == nil then return end

	player:seek(0)
	audioPlayer.pause(true)
	

	if discardObj then
		player:removeSelf()
		player = nil
		sourceURL = nil
		onLoaded = nil
		audioPlayer.listener = nil
	end
	

end





return audioPlayer
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------
local tableView = nil



-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.icons

    local topBar = (event.params and event.params.topBar) or require("module-topBar").new(sceneGroup, "Fotos")
    sceneGroup.topBar = topBar

    _G.BACKGROUND.fill = {1,1,1} --{234/255, 236/255, 239/255}
    
    --sceneGroup.lbLoading = display.newText{parent=sceneGroup, text="Carregando...", x=CENTER_X, y=CENTER_Y, font=APP_CONFIG.fontLight, fontSize=24}
    --sceneGroup.lbLoading:setTextColor(0,0,0)   

    sceneGroup.buttonsEnabled = false


    local btBack = require("library-widgets").newButton{
        left = 8,
        top = topBar.y + topBar.contentHeight,
        --width = 380*(320/1080), 
        height=30,
        
        label = "VOLTAR",
        labelColor = {20/255,20/255,20/255},
        labelOverColor = {20/255,20/255,20/255, .3},
        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 10,
        
        imageFile = APP_CONFIG.images.icons["arrowLeft"].path,
        imageWidth = APP_CONFIG.images.icons["arrowLeft"].width,
        imageHeight = APP_CONFIG.images.icons["arrowLeft"].height,
        imagePosition = "left",
        imagePadding = {right=4, bottom=2},

        imageColor = {20/255,20/255,20/255},
        imageOverColor = {20/255,20/255,20/255, .3},

        backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
        backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

        strokeWidth = 1,
        strokeColor = { 31/255, 148/255, 241/255, 1 },
        strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

        onRelease = function()
            if sceneGroup.buttonsEnabled ~= true then return end

            display.getCurrentStage( ):insert(sceneGroup.topBar) 
            composer.setVariable( "temp_topBar", sceneGroup.topBar )

            _G.rbBack.goToBackScene(); 

        end
    }
    sceneGroup:insert(btBack)


    sceneGroup.showHeader = function(photoObj)

        local groupHeader = display.newGroup()
        sceneGroup:insert(groupHeader)
        sceneGroup.groupHeader = groupHeader

        groupHeader.x = btBack.x - btBack.contentWidth*.5
        groupHeader.y = btBack.y + btBack.contentHeight*.5 + 10

        local iconCamera = display.loadImage(groupHeader, sceneAssets.camera)
        iconCamera.x = iconCamera.contentWidth*.5
        iconCamera.y = iconCamera.contentHeight*.5

        local lbW = SCREEN_W - 8 - (groupHeader.x + iconCamera.x + iconCamera.contentWidth*.5 + 4)
        local lbAlbumTitle = display.newText2{text=photoObj.title, font=APP_CONFIG.fontMedium, fontSize=14, width = lbW, align="left", color={0,0,0}}    
        lbAlbumTitle.x = iconCamera.x + iconCamera.contentWidth*.5 + 4
        lbAlbumTitle.y = iconCamera.y - iconCamera.contentHeight*.5
        groupHeader:insert(lbAlbumTitle)
        
        local lbDate = display.newText{parent=groupHeader, text=photoObj.date.day .. "/" .. photoObj.date.month .. "/" .. photoObj.date.year, x=lbAlbumTitle.x, font=APP_CONFIG.fontLight, fontSize = 12}
        lbDate.anchorX, lbDate.anchorX = 0, 0
        lbDate.y=lbAlbumTitle.y + lbAlbumTitle.contentHeight + 8
        lbDate:setFillColor( 100/255,100/255, 100/255)
        sceneGroup.lbDate = lbDate

    end
    

    sceneGroup.closeOverlay = function()
        print("on close overlay")
        composer.hideOverlay( "slideDown", 400 )
        _G.rbBack.externalBackFuction = nil
    end


    sceneGroup.showPhotos = function(albumObj)


        local thumbnails = {}

            for i=1, #albumObj.images do
                thumbnails[#thumbnails+1] = albumObj.images[i].thumbnail
            end

        local grid = require("library-widgets").newGrid{
            objectWidth = (SCREEN_W - 2)/3,
            objectHeight = (SCREEN_W - 2)/3,
            gridWidth = SCREEN_W+1,
            imagesURLs = thumbnails,  --aa,            
            spacing = 1,
            onRelease = function(e) 
                if sceneGroup.buttonsEnabled ~= true then return end

                -- Options table for the overlay scene "pause.lua"
                local options = {
                    isModal = true,
                    effect = "slideUp",
                    time = 400,
                    params = {
                        image = albumObj.images[e.target.index].full,
                        albumObj = albumObj,
                        photoIndex = e.target.index,
                        closeOverlay = sceneGroup.closeOverlay,
                    }
                }
                _G.rbBack.externalBackFuction = sceneGroup.closeOverlay                
                composer.showOverlay( "scene-photo-full", options )

            end,
            onRender = function(e)
                sceneGroup.scrollView:setScrollHeight( e.target.contentHeight )                
            end
        }

        local scrollViewTop = sceneGroup.groupHeader.y + sceneGroup.groupHeader.contentHeight + 6
        local scrollViewH = SCREEN_H - scrollViewTop
        --local shouldDisableSrollView = ( grid.contentHeight < ( scrollViewTop))

          -- creates the scrollview
        local scrollView = require("widget").newScrollView{
           x = CENTER_X,
           top = scrollViewTop,
           width = SCREEN_W,
           height = scrollViewH,    
           hideBackground = true,   
           hideScrollBar = true,
           horizontalScrollDisabled = true,
           verticalScrollDisabled = shouldDisableSrollView,
           bottomPadding = 20
        }  
        sceneGroup:insert(scrollView)
        scrollView:insert(grid)

        sceneGroup.scrollView = scrollView

    end
    



         
end





-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase
print("scene show - ", phase)
    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
        


        local photosObj = event.params and event.params.photosObj

        if sceneGroup.photosObj ~= photosObj then
            display.remove(sceneGroup.groupHeader)
            display.remove(sceneGroup.scrollView)
            sceneGroup.showHeader(photosObj)
            sceneGroup.showPhotos(photosObj)
        end



        sceneGroup.photosObj = photosObj

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        _G.rbBack.addBack()

        sceneGroup.topBar = (event.params and event.params.topBar) or require("module-topBar").new(sceneGroup, "Fotos")
        sceneGroup:insert(sceneGroup.topBar)


        --local albumObj = event.params and event.params.albumObj
        --sceneGroup.showPhotos(albumObj)

        sceneGroup.buttonsEnabled = true

        _G.MENU.highlightMenuWithLabel("Fotos")
        
        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry        
        local eventFlurry = "view;Fotos;Fotos;" .. event.params.photosObj.title .. ";"
        print(eventFlurry)
        _G.analytics.logEvent(eventFlurry)
        -- fim
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
        sceneGroup.buttonsEnabled = false

    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
        
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
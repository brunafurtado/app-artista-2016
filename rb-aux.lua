local aux = {}

aux.isFirstRun = require("rb-storage").load() ~= "App Oficial MK"

if aux.isFirstRun then
    require("rb-storage").save("App Oficial MK")
end


aux.comma_value = function(n) -- credit http://richard.warburton.it. Adapted to use dot as separator
    local left,num,right = string.match(n,'^([^%d]*%d)(%d*)(.-)$')
    return left..(num:reverse():gsub('(%d%d%d)','%1.'):reverse())..right
end


aux.getFilenameFromURL = function(url)
        
        local filename = url:match( "([^/]+)$" )

        local imgFilename = filename                            
        local imgExtension = imgFilename:sub(#imgFilename - 3)
        if imgExtension:sub(1,1) == "." then imgExtension = imgExtension:sub(2) end  

        -- let's use MD5 to create a unique filename based on the URL
        filename = require( "crypto" ).digest( crypto.md5, url ) .. "." .. imgExtension

        return filename
end



aux.adjustTextSize = function(textObject, limit)
    local text = textObject.text
    local extraWidth = textObject.width - limit
    local safeGuard = 1  -- variable to safe guard against infinite loop
    if extraWidth > 1  then
        while extraWidth > 1 and safeGuard < 100 do
            local percentReduction = limit / textObject.width
            local totalChars = text:len()
            local charactersToRemove = math.ceil(totalChars * (1-percentReduction) + 3)
            text = text:sub(1,totalChars-charactersToRemove) .. "..."
            textObject.text = text
            extraWidth = textObject.width - limit
            safeGuard = safeGuard + 1
        end
    end
end


aux.showLoadingAnimation = function(x,y,size)

    
     -- loading the spinner
    local iconLoadingObj = APP_CONFIG.images.icons.loading
    local icon = display.newImageRect(iconLoadingObj.path,  iconLoadingObj.width,  iconLoadingObj.height)
    if size then
        icon:scale(size/icon.contentWidth, size/icon.contentHeight)
    end
    icon.x = x
    icon.y = y
    
    transition.to( icon, {time=1300*10000, rotation=360*10000} )

    return icon

end

aux.showSpinner = function(objToReplace)

    local diameter = math.min(objToReplace.contentHeight, objToReplace.contentWidth)
    local spinner = require("widget").newSpinner({
        x = objToReplace.x,
        y = objToReplace.y,
        width = diameter,
        height = diameter
    })
    if objToReplace.parent then
        objToReplace.parent:insert(spinner)
    end
    spinner:start()

    return spinner

end


local dicMonths = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"}
aux.formatDatetime = function(date)

    return date.day .. " de " .. string.lower(dicMonths[tonumber(date.month)]) .. " às " .. date.hour .. ":" .. date.min

end


aux.formatDateToMonthYear = function(date)
    
    return dicMonths[date.month] .. " " .. date.year

end


-- function: validates a string based on its chars
-- retuns:  bool, errorCode (int)
--   1   not has minimum size
--   2   invalid char
--   3   invalid email format


function aux.validateString(text,txtType,minSize, ignoreCharsCheck)
    --print("running: checkRestrictions for ", text,  " tyoe = ",txtType )
    local charSetList = {
            password = "abcdefghijklmnopqrswtuvxyz0123456789._-@!#$%()*+?",
            username = "abcdefghijklmnopqrswtuvxyz0123456789._-",
            email    = "abcdefghijklmnopqrswtuvxyz0123456789._-@",
            text     = "abcdefghijklmnopqrswtuvxyz 'áéíóúàèìòùãiõñêîôûäëïöü-.",
            name     = "abcdefghijklmnopqrswtuvxyz 'áéíóúàèìòùãiõñêîôûäëïöüç-.123456789",
            phone     = "0123456789+ -()",
            birthday     = "0123456789/",
            }
            
    if minSize then
        if not text or text:len() < minSize then
            return false, 1
        end
    end
    
    if ignoreCharsCheck == true then
        return true
    end
    
    local charset = charSetList[txtType or "username"]
    if charset == nil then
        charset = charSetList["username"]
    end
    
    local lowerText = string.lower(text)
    --print("testing string ",lowerText)
    for i=1, #lowerText do
        local charNow = lowerText:sub(i,i)
        --print("lookink for",charNow )
        
        if charset:find(charNow) == nil then
      --      print("found invalid")
            return false, 2
        end
    end
    
    if txtType == "email" then
        local position
        position = lowerText:find("@",1)
        if not position then
            return false, 3
        end
        
        position = lowerText:find("@",position+1)
        if position then
            return false, 3
        end
        
        position = lowerText:find(".",1, true)
        if not position then
            return false, 3
        end
        
        local firstChar = lowerText[1]
        if firstChar == "." or firstChar == "@" then
            return false, 3
        end
    end
    
    return true
end




-- check if an event (x,y) is within the bounds of an object
aux.isWithinBounds = function( object, event )
    local bounds = object.contentBounds
    local x, y = event.x, event.y
    local isWithinBounds = false        
    if "table" == type( bounds ) then        
        if "number" == type( x ) and "number" == type( y ) then
            isWithinBounds = bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y            
        end
    end
    
    return isWithinBounds
end

-- creates a transparent rect above a group, making it easy to debug its boundaries
aux.debugGroup = function(groupOj)
    
    local contentBounds = groupOj.contentBounds

    local width = contentBounds.xMax - contentBounds.xMin
    local height = contentBounds.yMax - contentBounds.yMin

    local xCenter = contentBounds.xMin + width*.5
    local yCenter = contentBounds.yMin + height*.5

    local aa = display.newRect(xCenter,yCenter, width, height)
    aa:setFillColor( 1,0,1,.3 )

end



return aux


local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.agenda

    local topBar = require("module-topBar").new(sceneGroup, "Agenda")
    sceneGroup:insert(topBar)
    topBar.background.alpha = 0

    local background = display.loadImage(sceneGroup, sceneAssets.background)
    background.x, background.y = CENTER_X, CENTER_Y
    sceneGroup:insert(topBar) -- bringing to front

    local timerID = nil

    local margin={left=35/3, right=35/3}

    local frameWidth = SCREEN_W - (35/3)*2


    local months = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"}
    local currentDateSelected = os.date("*t")
    sceneGroup.currentDateSelected = currentDateSelected

    local lbMonthYear = display.newText{parent=sceneGroup, text=months[currentDateSelected.month] .. " " .. currentDateSelected.year, font=APP_CONFIG.fontLight, 18, x=CENTER_X, y =124}
    lbMonthYear:setTextColor( 0,171/255,235/255 ) 

    

    local function btOnTap(e)
         --print("tap - e.id=", e.target.id)
         --sceneGroup.lbLoading.alpha = 0
         display.remove(sceneGroup.loading)
         sceneGroup.loading = nil
         if timerID then            
            timer.cancel(timerID)
            timerID = nil
         end

         if e.target.id == "right" then
             currentDateSelected.month = currentDateSelected.month + 1
             if currentDateSelected.month == 13 then
                currentDateSelected.month = 1
                currentDateSelected.year = currentDateSelected.year + 1
             end
         else
            currentDateSelected.month = currentDateSelected.month - 1
             if currentDateSelected.month == 0 then
                currentDateSelected.month = 12
                currentDateSelected.year = currentDateSelected.year - 1
             end

         end

         local dateNow = os.date("*t")
         if dateNow.month == currentDateSelected.month and dateNow.year == currentDateSelected.year then
            sceneGroup.btArrowLeft.isVisible = false
         else
            sceneGroup.btArrowLeft.isVisible = true
         end

        lbMonthYear.text = months[currentDateSelected.month] .. " " .. currentDateSelected.year

        sceneGroup.updateTableView(currentDateSelected.month, currentDateSelected.year)

    end

    local btArrowLeft = require("library-widgets").newTapButton{
            id="left",
            y=lbMonthYear.y,
            w = 40,
            fillColor={1,0,0,0},
            label="", 
            labelColor = {0,162/255,1},
            labelOverColor = {0,162/255,1,.3},
            imageObj = APP_CONFIG.images.icons.arrowLeft,
            imageFillColor = {.8,.8,.8, .8},
            imageOverFillColor = {0,162/255,1,.3},
            height = 30,
            left = 0,
            fontSize=10,            
            onTap = btOnTap

    }
    sceneGroup:insert(btArrowLeft)
    sceneGroup.btArrowLeft = btArrowLeft
    btArrowLeft.isVisible  = false


    local btArrowRight = require("library-widgets").newTapButton{
            id="right",
            y=lbMonthYear.y,
            w = 40,
            fillColor={1,0,0,0},
            label="", 
            labelColor = {0,162/255,1},
            labelOverColor = {0,162/255,1,.3},
            imageObj = APP_CONFIG.images.icons.arrowLeft,
            imageFillColor = {.8,.8,.8, .8},
            imageOverFillColor = {0,162/255,1,.3},
            height = 30,
            right = SCREEN_W,
            fontSize=10,            
            onTap = btOnTap

    }
    btArrowRight.image:scale(-1,1)
    sceneGroup:insert(btArrowRight)

    local lineY = lbMonthYear.y + lbMonthYear.contentHeight + 6
    local line = display.newLine(0,lineY, frameWidth, lineY)
    line.strokeWidth = 1
    line.x = CENTER_X - frameWidth*.5
    line:setStrokeColor( 1,1,1,.1 )
    sceneGroup:insert(line)

    local tableView = nil
    local tableViewData = {}  -- table with the current data of the tableView
    local isReloadInProgress = false
    local tableViewContentH = 0


    local function onRowRender( event )

        -- Get reference to the row group
        local row = event.row
        local index = event.row.index
        
        -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
        local rowHeight = row.contentHeight
        local rowWidth = row.contentWidth
        

        local rowEntry = require("module-frames").eventEntry(tableViewData[index])
        if tableViewData[index].hasShown ~= true then
            transition.from(rowEntry,{alpha=0, time=400})
            tableViewData[index].hasShown = true
        end
        
        row:insert(rowEntry)
        rowEntry.x = CENTER_X - rowEntry.contentWidth*.5        
        
    end

    -- inserts 1 row inside the tableView with a specified delay (ms)
    local function insertRow(eventObj, delay)
        
        -- uncomment below to not add past events
        -- if eventObj.date.month ~= currentDateSelected.month or eventObj.date.year ~= currentDateSelected.year or (eventObj.date.day < currentDateSelected.day and eventObj.date.month == currentDateSelected.month and eventObj.date.year == currentDateSelected.year) then            
        --     print("not adding because of date ", eventObj.date.day, eventObj.date.month, eventObj.date.year)
        --     return false
        -- end

        local rowFrame = require("module-frames").eventEntry(eventObj, true)
        local rowHeight = rowFrame.contentHeight
        tableViewContentH = tableViewContentH + rowHeight
        display.remove(rowFrame)
        rowFrame = nil

        -- Insert a row into the tableView
        timerID = timer.performWithDelay( delay, function()
            tableView:insertRow({
                    rowHeight = rowHeight,
                    rowColor = { default={ 0, 1, 1,0 }, over={ 1, 0.5, 0, 0 } },
                }
            )
        end)



        return true

    end

    -- add data (list of objects) to the tableView. It can do a clean insert (deleteBeforeAdding = true) or just append new data
    local function addDataToTableView(data, deleteBeforeAdding)
                
        sceneGroup.lbNoEvents.alpha = 0
        local hasEvent = false
        
        if data then
            if deleteBeforeAdding then
                tableView:deleteAllRows()
                tableViewData = data
                tableViewContentH = 0

                
                for i = 1, #tableViewData do
                    hasEvent = insertRow(tableViewData[i], 10 + 130*i) or hasEvent
                end


            else
                -- let's append the data to our current tableViewData
                for i=1,#data do                
                    hasEvent = insertRow(data[i], 10 + 130*i) or hasEvent
                    tableViewData[#tableViewData+1] = data[i]
                end

            end
        end

        if hasEvent == false then
            sceneGroup.lbNoEvents.alpha = 1
            --sceneGroup.lbLoading.alpha = 0
            display.remove(sceneGroup.loading)
            sceneGroup.loading = nil
        end

        if tableViewContentH < tableView.contentHeight then
            tableView:setIsLocked(true)
        else
            tableView:setIsLocked(false)
        end
        
    end


    


    -- Create the widget
    --local tableViewTop = 50
    local tableViewTop = lineY + 10
    local tableViewH = SCREEN_H - tableViewTop
    tableView = require("library-widgets").newTableView{
        left = 0,
        top = tableViewTop,
        height = tableViewH,
        width = SCREEN_W,
        hideBackground = true,
        noLines = true,
        --topPadding = 10,
        bottomPadding = 8,
        onRowRender = onRowRender,        
        --isLocked = true,
        --onRowTouch = onRowTouch,        
        --listener = scrollListener
    }    

    sceneGroup:insert(tableView)


    --sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, SCREEN_H*.5) 
    --sceneGroup:insert(sceneGroup.loading)

    sceneGroup.lbNoEvents = display.newText{parent=sceneGroup, text="Sem eventos este mês.", x=CENTER_X, y=CENTER_Y, font=APP_CONFIG.fontLight, fontSize=24}
    sceneGroup.lbNoEvents:setTextColor(1,1,1)   
    sceneGroup.lbNoEvents.alpha = 0

    sceneGroup.updateTableView = function(month, year)
        
        tableView:deleteAllRows( )

        sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y) 
        sceneGroup:insert(sceneGroup.loading)

        sceneGroup.lbNoEvents.alpha = 0

        --timer.performWithDelay( 1000, function()
            SERVER.getEvents(currentDateSelected.month, function(e) 
                    
                    display.remove(sceneGroup.loading)
                    addDataToTableView(e.data, true)  
            end) 
        --end)

    end


    
    
    





    
-- local x = require("module-frames").eventEntry({name= "Bruna Karla e Banda", date={day=29, month=12, year=2015, hour=22, min=00}, address={name="Igreja Batista em Rancho Novo", street="Rua Dona Clara de Araújo, 2458", neighborhood="Nova Iguaçu", city="Rio de Janeiro", state="RJ"}})
-- x.x = CENTER_X - x.contentWidth*.5
-- x.y = CENTER_Y  
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).        
        sceneGroup.updateTableView(sceneGroup.currentDateSelected.month, sceneGroup.currentDateSelected.year)

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

      _G.rbBack.addBack()

      _G.MENU.highlightMenuWithLabel("Agenda")
    
      -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
      local eventFlurry = "view;Agenda;Agenda;"
      print(eventFlurry)
      _G.analytics.logEvent(eventFlurry)
      -- fim
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------
local tableView = nil



-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    local albumsList = event.params and event.params.albumsList
    local selectedAlbumIndex = event.params and event.params.selectedAlbumIndex

    local albumObj = albumsList[selectedAlbumIndex]

    -- forward declaration. This function will be defined on the end of this block
    local playNextMusic
    local btPlayOnRelease
    local btShuffle



--local albumObj = (event.params and event.params.albumObj)
    -- local albumObj = (event.params and event.params.albumObj) or require("class-album").new( { 
    --         image={url="https://s3-sa-east-1.amazonaws.com/redbeach-mk/discography/album1%403x.png", width=253/3, height=251/3},
    --         title= "Como Águia", artist="Bruna Karla",
    --         date = {day=13, month=2, year=2015},
    --         musics={{title="Como Águia", previewURL="www.g1.com", duration={min=3, sec=49}},                    
                    
    --         },
    --         radios={ deezer="http://www.deezer.com",
    --                  spotify="http://www.spotify.com",
    --                  rhapsody="http://www.rhapsody.com",
    --                  musicKey="http://www.musicKey.com",
    --                  appleMusic = "http://www.apple.com"
    --         },
    --         previewURL = "http://www.google.com/?q=previewURL",
    --         buyDiscURL = "http://www.google.com/?q=buyDiscURL",
    --         buyDigitalAlbumURL =  "http://www.google.com/?q=buyDigitalAlbumURL",
    -- })


    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.previewPlayer

    local topBar = (event.params and event.params.topBar) or require("module-topBar").new(sceneGroup, "Preview")
    sceneGroup.topBar = topBar
    topBar.background.alpha = 0


    local margin = 18  -- left and right margin value


    local background = display.loadImage(sceneGroup, sceneAssets.background)
    background.x, background.y = CENTER_X, CENTER_Y
    sceneGroup:insert(topBar) -- bringing to front


   local btBack = require("library-widgets").newButton{
        left = 12, --8,
        top = topBar.y + topBar.contentHeight,
        --width = 380*(320/1080), 
        height=30,
        
        label = "",
        labelColor = {0,162/255,1},
        labelOverColor = {0,162/255,1,.3},
        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 10,
        
        imageFile = APP_CONFIG.images.icons["arrowLeft"].path,
        imageWidth = APP_CONFIG.images.icons["arrowLeft"].width,
        imageHeight = APP_CONFIG.images.icons["arrowLeft"].height,
        imagePosition = "left",
        imagePadding = {right=4, bottom=2},

        imageColor = {138/255,138/255,138/255,1},
        imageOverColor = {138/255,138/255,138/255,.3},

        backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
        backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

        strokeWidth = 1,
        strokeColor = { 31/255, 148/255, 241/255, 1 },
        strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

        onRelease = function() print("tap"); 

            --display.getCurrentStage( ):insert(sceneGroup.topBar) 
            --composer.setVariable( "temp_topBar", sceneGroup.topBar )

            _G.rbBack.goToBackScene(); 

        end
    }
    sceneGroup:insert(btBack)
    
    local devicePreview
    
    if _G.DEVICE.isApple then
        
        devicePreview = "iTunes"
        
    elseif _G.DEVICE.isGoogle then

        devicePreview = "Google Play"       

    elseif _G.DEVICE.isKindleFire then
    
        devicePreview = "Amazon"           

    elseif platform == "WinPhone" then
        
        devicePreview = "Windows Phone"
        
    else
    
        -- any other DEVICE (windows, html,..) will end up here
        
    end

    --print(deviceDiscography)
    
    -- fim

    local btLyricRightPos = SCREEN_W - margin
    local btLyricW = 36
    local btLyric
     btLyric = require("custom-widget").newLyricButton{
        id="btLyric",
        width = btLyricW,
        height = btH,
        x = btLyricRightPos - btLyricW*.5,
        y = btBack.y,
        onRelease = function() 
        
            local function onServerResponse(e)
                
                btLyric.alpha = 1
                display.remove(btLyric.spinner)
                if e.result then
                    require("module-popups").show("lyric", e.data)
                    
                    -- incluido por Bruna em 19/10/2015 
                    local eventFlurry = "view;Previews;Letra;" .. albumObj.title .. ";" .. e.data.title .. ";"
                    print(eventFlurry)
                    _G.analytics.logEvent(eventFlurry)
                    -- fim
                    
                end
            end
            btLyric.alpha = 0
            btLyric.spinner = AUX.showSpinner(btLyric)

            SERVER.getLyric(sceneGroup.music.id, onServerResponse)
            
        end
    }
    sceneGroup:insert(btLyric)


    local btBuySingle = require("library-widgets").newButton{
        right = btLyric.x - btLyric.contentWidth*.5 - 6,
        y = btBack.y,
        -- Alterado por Bruna em 22/10/2015 pois o label estava "COMPRAR SINGLE" mas o link aponta para o álbum no itunes
        width = 140, height=btLyric.contentHeight,
        
        label = "COMPRAR ÁLBUM DIGITAL",
        -- fim
        labelColor = { 1, 1, 1 }, 
        labelOverColor = { 0, 0, 0, 0.5 },
        labelFont = APP_CONFIG["fontMedium"],
        labelFontSize = 10,

        backgroundColor = { 31/255, 148/255, 241/255, 1 }, 
        backgroundOverColor ={ 31/255, 148/255, 241/255, 0.6 },

        strokeWidth = 1,
        strokeColor = { 31/255, 148/255, 241/255, 1 },
        strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

        onRelease=function()
            if sceneGroup.buyMusicURL then
                system.openURL( sceneGroup.buyMusicURL )
                -- incluido por Bruna em 19/10/2015 
                local parceiro = devicePreview
                local eventFlurry = "outClick;Previews;Player;Comprar Álbum Digital;" .. parceiro .. ";" .. albumObj.title .. ";"
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
            end

        end
    }
    sceneGroup:insert(btBuySingle)




    sceneGroup.showMusic = function(albumToShow)
        
    print("btShuffle=", btShuffle)
    print("btShuffle.isOn=", btShuffle and btShuffle.isOn)
        local isShuffleOn = (btShuffle and btShuffle.isOn)

        -- let's remove any previous music info already on screen
        display.remove(sceneGroup.scrollView)
            
        local music = albumToShow.musics[1]
        sceneGroup.music = music
        sceneGroup.music.id = albumToShow.barcode .. "-" .. 1

        sceneGroup.buyMusicURL = music.buyMusicURL
        if sceneGroup.buyMusicURL then
            btBuySingle.isVisible = true
        else
            btBuySingle.isVisible = false
        end

        local groupContent = display.newGroup()
        sceneGroup:insert(groupContent)
        groupContent.y = btBuySingle.y + btBuySingle.contentHeight*.5


        local imageMaxWidth = SCREEN_W - margin*2
        local image = display.loadImageFromInternet(groupContent, albumToShow.image)
        local scaleFactor = math.min(imageMaxWidth / image.contentWidth, 1)
        image:scale(scaleFactor, scaleFactor)
        image.x = CENTER_X
        image.y = image.contentHeight*.5     --btBuySingle.y + btBuySingle.contentHeight*.5 + 20 + image.contentHeight*.5    
        

        local lbArtist = display.newText{parent=groupContent, text=albumToShow.artist, x = image.x, y=image.y + image.contentHeight*.5 + 10, font=APP_CONFIG.fontMedium, fontSize=14, width=imageMaxWidth, align="center"}
        lbArtist.anchorY = 0
        lbArtist:setTextColor( 180/255,180/255,180/255)

        local lbMusicTitle = display.newText{parent=groupContent, text=music.title, x = image.x, y=lbArtist.y + lbArtist.contentHeight + 6, font=APP_CONFIG.fontLight, fontSize=20, width=imageMaxWidth, align="center"}
        lbMusicTitle.anchorY = 0
        lbMusicTitle:setTextColor( 180/255,180/255,180/255)



        local groupBar = display.newGroup()


        local barW = SCREEN_W - 10*2
        local barH = 2

        local btRepeat
        local btRepeatHandler
        local btShuffleHandler

        local function updateButtonColor(obj)
            print("obj.isOn=", obj.isOn)
            if obj.isOn then
                obj.image:setFillColor(34/255, 157/255, 235/255)
            else
                obj.image:setFillColor(242/255, 242/255, 242/255)
            end
        end

        btRepeatHandler = function(e)
            local newStatus = not e.target.isOn            
            e.target.isOn = newStatus            
            updateButtonColor(e.target, newStatus)
            if newStatus == true then
                btShuffle.isOn = false
                print("updating button shuffle")
                updateButtonColor(btShuffle)
            end            
        end

        btRepeat = require("library-widgets").newButton{
            left = 0,
            top = 0,
            width = sceneAssets["repeat"].width*2, 
            height = sceneAssets["repeat"].height*2,
                    
            imageFile = sceneAssets["repeat"].path,
            imageWidth = sceneAssets["repeat"].width,
            imageHeight = sceneAssets["repeat"].height,
            imagePosition = "left",
            imagePadding = {left=-sceneAssets["repeat"].width*.5, top=sceneAssets["repeat"].height*.5},

            imageColor = {242/255, 242/255, 242/255,1},
            imageOverColor = {34/255, 157/255, 235/255, 1},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onRelease=btRepeatHandler
        }
        groupBar:insert(btRepeat)
        btRepeat.isOn = false


        btShuffleHandler = function(e)
            local newStatus = not e.target.isOn            
            e.target.isOn = newStatus            
            updateButtonColor(e.target, newStatus)

            if newStatus == true then
                btRepeat.isOn = false
                updateButtonColor(btRepeat)
            end   
        end

        btShuffle = require("library-widgets").newButton{
            right = barW,
            top = 0,
            width = sceneAssets["shuffle"].width*2, 
            height = sceneAssets["shuffle"].height*2,
                    
            imageFile = sceneAssets["shuffle"].path,
            imageWidth = sceneAssets["shuffle"].width,
            imageHeight = sceneAssets["shuffle"].height,
            imagePosition = "left",
            imagePadding = {right=-sceneAssets["shuffle"].width*.5, top=sceneAssets["shuffle"].height*.5},

            imageColor = {242/255, 242/255, 242/255,1},
            imageOverColor = {34/255, 157/255, 235/255, 1},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onRelease=btShuffleHandler
        }
        groupBar:insert(btShuffle)
        btShuffle.isOn = isShuffleOn
        updateButtonColor(btShuffle)


        local barTop = btRepeat.y + btRepeat.contentHeight*.5 + 6
        local barBackground = display.newRect(groupBar, barW*.5, barTop + barH*.5,barW,barH)
        barBackground:setFillColor( 180/255,180/255,180/255 )

        local barFront = display.newRect(groupBar, barBackground.x - barBackground.contentWidth*.5, barBackground.y , 0, barH)
        barFront:setFillColor( 36/255,163/255,236/255 )
        barFront.anchorX = 0

        local function setProgress(newTimeInSeconds)
            transition.cancel("bar")
            local totalDuration = sceneGroup.totalDuration or 60 --music.duration.min*60 + music.duration.sec

            local percentValue = newTimeInSeconds / totalDuration

            local newWidth = barBackground.contentWidth * percentValue
            local newTimeSec = math.floor(newTimeInSeconds % 60)
            if newTimeSec < 10 then newTimeSec = "0" .. newTimeSec end
            local newTimeString = math.floor(newTimeInSeconds / 60) .. ":" .. newTimeSec
            groupBar.lbTimeCurrent.text = newTimeString

            --barFront.width = newWidth

            transition.to(barFront,{width=newWidth, time=10, tag="bar"})

        end

        local function resetProgress()        
            setProgress(0)
        end

        --local c = 1
        --timer.performWithDelay( 1000, function() c=c+1; setProgress(c) end, 60)

        local lbTimeCurrent = display.newText{parent=groupBar, text="0:00", font=APP_CONFIG.fontLight, fontSize=10}
        lbTimeCurrent:setTextColor(242/255, 242/255, 242/255)
        lbTimeCurrent.anchorX,lbTimeCurrent.anchorY = 0, 0
        lbTimeCurrent.x = barBackground.x - barBackground.contentWidth*.5
        lbTimeCurrent.y = barBackground.y + barBackground.contentHeight*.5 + 2
        groupBar.lbTimeCurrent = lbTimeCurrent
         
        -- label of total duration of the music. Let's use 30 seconds, but this value is updated                                                         --music.duration.min or "00") .. ":" .. (music.duration.sec or "00")
        local lbTimeTotal = display.newText{parent=groupBar, text="0:30", font=APP_CONFIG.fontLight, fontSize=10}
        lbTimeTotal:setTextColor(242/255, 242/255, 242/255)
        lbTimeTotal.anchorX,lbTimeTotal.anchorY = 1, 0
        lbTimeTotal.x = barBackground.x + barBackground.contentWidth*.5
        lbTimeTotal.y = lbTimeCurrent.y






            
        local btPlay
        local btPause

        btPlayOnRelease = function()
                btPlay.isVisible = false
                btPlay.spinner = AUX.showSpinner(btPlay)
                print("music.title=", music.title)
                local sourceURL = music.previewURL
                print("sourceURL=", sourceURL)           

                -- incluido por Bruna em 19/10/2015 
                local eventFlurry = "view;Previews;Play;" .. albumObj.title .. ";" .. music.title
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
            
                -- PLAYER.load(sourceURL, function(e)
                --     print("loadeddd - totalTime=", e.totalTime)
                --     sceneGroup.totalDuration = e.totalTime
                --     display.remove(  btPlay.spinner )
                --     btPause.isVisible = true
                --     PLAYER.play(sourceURL)
                -- end)
                PLAYER.play(sourceURL)
        end

        -- audio player listener
        PLAYER.listener = function(e)

            local phase = e.phase

            if phase == "progress" or phase == "ended" then
                display.remove(  btPlay.spinner )
                btPause.isVisible = true
                setProgress(e.currentTime)
                print("e.ct=", e.currentTime)

            elseif phase == "loaded" then
                print("loaded")
                if e.totalTime then
                    sceneGroup.totalDuration = e.totalTime
                    local min = math.floor(e.totalTime/60)
                    local sec = math.floor(e.totalTime - min*60)
                    if sec < 10 then
                        sec =  "0" .. sec
                    end
                    lbTimeTotal.text = min .. ":" .. sec
                end
                

                display.remove(  btPlay.spinner )
                btPause.isVisible = true

            end

            if phase == "ended" then
            
                btPlay.isVisible = true
                btPause.isVisible = false
                
                -- incluido por Bruna em 19/10/2015 
                local eventFlurry = "view;Previews;Stop;" .. albumObj.title .. ";" .. music.title
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                
                PLAYER.stop()

                if btRepeat.isOn then
                    btPlayOnRelease()
                elseif btShuffle.isOn then
                    playNextMusic("random")
                end
            end

        end

        -- button play
        btPlay = require("library-widgets").newButton{
            x = barBackground.x,
            top = barBackground.y + barBackground.contentHeight*.5 + 12,
                    
            imageFile = sceneAssets["play"].path,
            imageWidth = sceneAssets["play"].width,
            imageHeight = sceneAssets["play"].height,
            imagePosition = "left",

            imageColor = {242/255, 242/255, 242/255,1},
            imageOverColor = {34/255, 157/255, 235/255, 1},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onRelease=btPlayOnRelease,
        }
        groupBar:insert(btPlay)

        sceneGroup.btPauseHandler = function()
                print("pause") 
                PLAYER.pause()
                btPlay.isVisible = true
                btPause.isVisible = false
        end

        -- bt pause
        btPause = require("library-widgets").newButton{
            x = barBackground.x,
            top = barBackground.y + barBackground.contentHeight*.5 + 12,
                    
            imageFile = sceneAssets["pause"].path,
            imageWidth = sceneAssets["pause"].width,
            imageHeight = sceneAssets["pause"].height,
            imagePosition = "left",

            imageColor = {242/255, 242/255, 242/255,1},
            imageOverColor = {34/255, 157/255, 235/255, 1},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onRelease = sceneGroup.btPauseHandler,
            -- onRelease=function()
            --     print("pause") 
            --     PLAYER.pause()
            --     btPlay.isVisible = true
            --     btPause.isVisible = false

            -- end
        }
        groupBar:insert(btPause)
        btPause.isVisible = false


        local btReward = require("library-widgets").newButton{
            right= btPlay.x - btPlay.contentWidth*.5 - 10,
            y = btPlay.y,
                    
            imageFile = sceneAssets["reward"].path,
            imageWidth = sceneAssets["reward"].width,
            imageHeight = sceneAssets["reward"].height,
            imagePosition = "left",

            imageColor = {242/255, 242/255, 242/255,1},
            imageOverColor = {34/255, 157/255, 235/255, 1},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onRelease=function() 
                        PLAYER.stop()
                        playNextMusic("previous") 
                    end
        }
        groupBar:insert(btReward)


        local btForward = require("library-widgets").newButton{
            left = btPlay.x + btPlay.contentWidth*.5 + 10,
            y = btPlay.y,
                    
            imageFile = sceneAssets["forward"].path,
            imageWidth = sceneAssets["forward"].width,
            imageHeight = sceneAssets["forward"].height,

            imageColor = {242/255, 242/255, 242/255,1},
            imageOverColor = {34/255, 157/255, 235/255, 1},

            backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
            backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

            strokeWidth = 1,
            strokeColor = { 31/255, 148/255, 241/255, 1 },
            strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

            onRelease=function() 
                        PLAYER.stop()
                        playNextMusic("next") 
                    end
        }
        groupBar:insert(btForward)
        groupContent:insert(groupBar)

        groupBar.y = lbMusicTitle.y + lbMusicTitle.contentHeight + 10
        groupBar.x = CENTER_X - groupBar.contentWidth*.5

        local scrollViewTop = groupContent.y
        local scrollViewHeight = SCREEN_H - scrollViewTop

        local shouldDisableSrollView = ( scrollViewHeight < groupContent.contentHeight)


        local scrollView = require("widget").newScrollView{
           x = CENTER_X,
           y = scrollViewTop + scrollViewHeight*.5,
           width = SCREEN_W,
           height = scrollViewHeight,    
           hideBackground = true,   
           backgroundColor = { 0.8, 0.8, 0},
           horizontalScrollDisabled = true,
           hideScrollBar = true,
           --verticalScrollDisabled = shouldDisableSrollView,
           topPadding = 10,
           bottomPadding = 40,
           --isBounceEnabled = false
        }  
         groupContent.y = 0 
        sceneGroup:insert(scrollView)
        sceneGroup.scrollView = scrollView
        scrollView:insert(groupContent)
        sceneGroup.currentMusic = music

    end
    


    playNextMusic = function(order)
            local musicIndex

            if order == "random" then

                local randomIndex = math.random( #albumsList )

                while randomIndex == sceneGroup.selectedAlbumIndex do
                    randomIndex = math.random( #albumsList )
                end            

                musicIndex = randomIndex

            elseif order == "next" then
                musicIndex = sceneGroup.selectedAlbumIndex + 1
                if musicIndex > #albumsList then
                    musicIndex = 1
                end

            elseif order =="previous" then
                musicIndex = sceneGroup.selectedAlbumIndex - 1
                if musicIndex < 1 then
                    musicIndex = #albumsList
                end
            end

            sceneGroup.selectedAlbumIndex = musicIndex            
            sceneGroup.showMusic(albumsList[musicIndex])

            btPlayOnRelease()

        end

    sceneGroup.showMusic(albumObj)
     
end


-- "scene:show()"
function scene:show( event )
    print("on enterScene")
    local sceneGroup = self.view
    local phase = event.phase

    -- incluido por Bruna em 22/10/2015 
    local albumsList = event.params and event.params.albumsList
    local selectedAlbumIndex = event.params and event.params.selectedAlbumIndex
    sceneGroup.selectedAlbumIndex = selectedAlbumIndex
    local albumObj = albumsList[selectedAlbumIndex]
    local music = albumObj.musics[1]
    -- fim
    
    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
        _G.rbBack.addBack()

           -- retirado de dentro do bloco por Bruna em 22/10/2015 pois precisava retornar as variáveis para enviar para o flurry.
           --local albumsList = event.params and event.params.albumsList
           --local selectedAlbumIndex = event.params and event.params.selectedAlbumIndex
           --sceneGroup.selectedAlbumIndex = selectedAlbumIndex
           --local albumObj = albumsList[selectedAlbumIndex]
           --local music = albumObj.musics[1]
           -- fim
        
        if music ~= nil and music ~=  sceneGroup.currentMusic and music.previewURL ~=  sceneGroup.currentMusic.previewURL then
            
            --display.remove(sceneGroup.scrollView)
            sceneGroup.showMusic(albumObj)
        end

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        _G.rbBack.addBack()

        sceneGroup.topBar = (event.params and event.params.topBar) or sceneGroup.topBar
        sceneGroup:insert(sceneGroup.topBar)  

        _G.MENU.highlightMenuWithLabel("Preview")      

        sceneGroup.onSystemEvent = function( event )
            --print("running: main.onSystemEvent." .. tostring(event.type))    

            if ( event.type == "applicationSuspend") then
                -- Stops transitions and timers
                print("suspending application")
                if composer.getSceneName( "current" ) == "scene-preview-player" then
                    _G.rbBack.goToBackScene();
                end
                
                
            end

        end
        Runtime:addEventListener( "system", sceneGroup.onSystemEvent )
        
         -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Previews;Player;" .. albumObj.title .. ";" .. music.title .. ";"
        print(eventFlurry)
        _G.analytics.logEvent( eventFlurry )
        -- fim
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
        PLAYER.stop(true)
        Runtime:removeEventListener( "system", sceneGroup.onSystemEvent )

    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.

        

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
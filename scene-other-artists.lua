local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


-- "scene:create()"
function scene:create( event )

    -- incluído por Bruna em 23/10/2015
    local parceiro
    
    if _G.DEVICE.isApple then
        
        parceiro = "iTunes"
        
    elseif _G.DEVICE.isGoogle then

        parceiro = "Google Play"       

    elseif _G.DEVICE.isKindleFire then
    
        parceiro = "Amazon"           

    elseif platform == "WinPhone" then
        
        parceiro = "Windows Phone"
        
    else
    
        -- any other DEVICE (windows, html,..) will end up here
        
    end
    -- fim
    
    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        
    local sceneAssets = _G.IMAGES.otherArtists

    local topBar = require("module-topBar").new(sceneGroup, "Outros Artistas")
    sceneGroup:insert(topBar)
    topBar.background.alpha = 0

    local background = display.loadImage(sceneGroup, sceneAssets.background)
    background.x, background.y = CENTER_X, CENTER_Y
    sceneGroup:insert(topBar) -- bringing to front

    local margin={left=35/3, right=35/3}

    local frameWidth = SCREEN_W - (35/3)*2


    local function newArtistGroup(artistObj)

        local group = display.newGroup()


        local image = display.loadImageFromInternet(group, {url=artistObj.imageURL, width=256/3, height=256/3}, false)
        image.x, image.y = image.contentWidth*.5, image.contentHeight*.5

        local lbArtist = display.newText2{parent=group, text=artistObj.name, font=APP_CONFIG.fontLight, fontSize=18, width=140, align="center", color={170/255,170/255,170/255}}
        lbArtist.anchorChildren = true
        lbArtist.anchorX, lbArtist.anchorY = 0.5, 0
        --lbArtist:setTextColor( 170/255,170/255,170/255 )        
        lbArtist.x = 0
        lbArtist.y = image.y + image.contentHeight*.5 + 4

        local button
        if artistObj.appURL and artistObj.appURL ~= "" then
            button = require("library-widgets").newButton{
                x = image.x,
                top = lbArtist.y + lbArtist.contentHeight + 10,
                --width = 380*(320/1080), 
                height=30,
                
                label = "BAIXAR APLICATIVO",
                labelColor = {236/255,169/255,23/255,1},
                labelOverColor = {236/255,169/255,23/255,.3},
                labelFont = APP_CONFIG["fontMedium"],
                labelFontSize = 10,
                
                imageFile = sceneAssets.icon_download.path,
                imageWidth = sceneAssets.icon_download.width,
                imageHeight = sceneAssets.icon_download.height,
                imagePosition = "left",
                imagePadding = {right=4, bottom=0},

                imageColor = {236/255,169/255,23/255,1},
                imageOverColor = {236/255,169/255,23/255,.3},

                backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
                backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

                strokeWidth = 1,
                strokeColor = { 31/255, 148/255, 241/255, 1 },
                strokeOverColor = { 31/255, 148/255, 241/255, 0.6 },

                onRelease = function() 
                    -- Incluído por Bruna em 23/10/2015 para enviar o evento para o Flurry 
                    -- caso o usuario queira baixar outro aplicativo.
                    local eventFlurry = "outClick;Outros Artistas;Outros Artistas;Baixar Aplicativo;" .. parceiro .. ";".. artistObj.appURL ..";"
                    print(eventFlurry)
                    _G.analytics.logEvent(eventFlurry)
                    -- fim
                    system.openURL(artistObj.appURL) 

                end
            }
        else
            button = require("library-widgets").newButton{
                x = image.x,
                top = lbArtist.y + lbArtist.contentHeight + 10,
                --width = 380*(320/1080), 
                height=30,
                
                label = "EM BREVE",
                labelColor = {236/255,169/255,23/255,1},
                labelOverColor = {236/255,169/255,23/255,.3},
                labelFont = APP_CONFIG["fontMedium"],
                labelFontSize = 10,                

                imageColor = {236/255,169/255,23/255,1},
                imageOverColor = {236/255,169/255,23/255,1},

                backgroundColor = { 31/255, 148/255, 241/255, 0 }, 
                backgroundOverColor ={ 31/255, 148/255, 241/255, 0 },

                strokeWidth = 1,
                strokeColor = { 31/255, 148/255, 241/255, 1 },
                strokeOverColor = { 31/255, 148/255, 241/255, 1 },

                onRelease = function()                     

                end
            }            
        end
        group:insert(button)

        -- making sure all objects are centered inside the group
        local groupCenterX = 0.5*math.max(image.contentWidth, button.contentWidth, lbArtist.contentWidth)
        image.x = groupCenterX
        button.x = groupCenterX
        lbArtist.x = groupCenterX


        return group


    end

    local groupContent = display.newGroup()
    groupContent.y = 0--120


    local scrollView

    local function showOtherArtist(returnData)

        display.remove(sceneGroup.loading)
        sceneGroup.loading = nil

        if returnData.result then

            local data = returnData.data
            local lineY = 0
            local isEmpty = true
            local margin = 0
            local blockWidth = 150

            local numOfColumns = math.floor((SCREEN_W - 2*margin)/blockWidth)
            local updatedMargin = (SCREEN_W - numOfColumns*blockWidth)/2

            
            for i=1, #data do

                    isEmpty = false
     
                    local currentColumn = i % numOfColumns
                    if currentColumn == 0 then
                        currentColumn = numOfColumns
                        maxHeight = 0
                    end

                    local groupArtist = newArtistGroup(data[i])
                    groupContent:insert(groupArtist)
                    groupArtist.x = updatedMargin + (currentColumn - 1)*blockWidth + blockWidth/2 - groupArtist.contentWidth*.5
                    groupArtist.y = lineY + 20
                    
                    if i % numOfColumns == 0  and i < #data then
                        lineY = groupContent.contentHeight + 20
                        local line = display.newLine(groupContent, 10,lineY, SCREEN_W - 10 ,lineY)
                        line.strokeWidth = 1
                        line:setStrokeColor( 40/255, 40/255, 40/255 )

                    end

            end
            
            if isEmpty then

                local lbNoApps = display.newText2{text="Em breve, mais aplicativos dos seus artistas favoritos!", spaceBetweenWords = 3, spaceBetweenLines = 4, x = CENTER_X, y = CENTER_Y, font=APP_CONFIG.fontLight, fontSize=20, color={1,1,1}, width=SCREEN_W*.8, align="center"}
                lbNoApps.x = CENTER_X - lbNoApps.contentWidth*.5
                lbNoApps.y = CENTER_Y
                sceneGroup:insert(lbNoApps)

            else            
            
                local scrollViewH = SCREEN_H - 120
                local shouldDisableSrollView = false --( groupScrollViewContent.contentHeight < ( SCREEN_H - backgroundWhite.contentBounds.yMin))

                  -- creates the scrollview
                scrollView = require("widget").newScrollView{
                   x = CENTER_X,
                   y = 120 + scrollViewH*.5,
                   width = SCREEN_W,
                   height = scrollViewH,    
                   hideBackground = true,   
                   hideScrollBar = true,
                   horizontalScrollDisabled = true,
                   verticalScrollDisabled = shouldDisableSrollView,
                   bottomPadding = 100,
                }  
                sceneGroup:insert(scrollView)
                scrollView:insert(groupContent)

            end

        end

    end


    SERVER.getOtherArtists(showOtherArtist)

    
    sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y) 
    sceneGroup:insert(sceneGroup.loading)
-- local x = require("module-frames").eventEntry({name= "Bruna Karla e Banda", date={day=29, month=12, year=2015, hour=22, min=00}, address={name="Igreja Batista em Rancho Novo", street="Rua Dona Clara de Araújo, 2458", neighborhood="Nova Iguaçu", city="Rio de Janeiro", state="RJ"}})
-- x.x = CENTER_X - x.contentWidth*.5
-- x.y = CENTER_Y  
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).        
        

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
   
        _G.rbBack.addBack()


        _G.MENU.highlightMenuWithLabel("Outros Artistas")
       
        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Outros Artistas;Outros Artistas;"
        print(eventFlurry)
        _G.analytics.logEvent(eventFlurry)
        -- fim
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
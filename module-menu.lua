local m = {}

m.menu = nil  -- pointer to the menu group object



local show
local hide
local showNotification
local highlightMenuWithLabel


local rows = {}     -- will store references for the rows as a dictionary  [label]:rowGroup

local rowSelected = nil  -- pointer to the current selected row

local menuWidth = (999 / 1080) * 320 --SCREEN_W

-- opens the menu
show = function()

    if m.menu.x > 0  then
        return
    end

    animated = animated or false

    -- making sure menu is above composer scene and behind tabbar
    display.getCurrentStage():insert(m.menu)
    display.getCurrentStage():insert(_G.statusBarBackground)

    local duration = 600
    transition.to(m.menu, {x=0, time=duration, transition=easing.inOutExpo})

    if m.menu.objToTranslate then
        if m.menu.objToTranslate.x == nil then
           m.menu.objToTranslate.x = 1
        end
        m.menu.objToTranslate._x = m.menu.objToTranslate.x
        transition.to(m.menu.objToTranslate, {x=m.menu.objToTranslate.x + m.menu.contentWidth, time=duration, transition=easing.inOutExpo})
    end


    _G.rbBack.externalBackFuction = hide



end


-- closes the menu
hide = function()
    --print("show tabbar")
    if m.menu.x < 0  then
        return
    end


    local duration = 400
    transition.to(m.menu, {x=-m.menu.contentWidth, time=duration, transition=easing.inOutExpo})
    if m.menu.objToTranslate then
        transition.to(m.menu.objToTranslate, {x=m.menu.objToTranslate._x, time=duration, transition=easing.inOutExpo})
        m.menu.objToTranslate._x = nil
    end

    _G.rbBack.externalBackFuction = nil

end


-- highlights a row. If nil is passed, it makes no row selected
local function highlightRow(rowGroup)

    if rowSelected then

        rowSelected.background.isVisible = false
        rowSelected.icon:setFillColor(unpack(rowSelected.icon._colorNotSelected))
    end
    if rowGroup then

        rowSelected = rowGroup
        rowSelected.background.isVisible = true
        rowSelected.icon:setFillColor(unpack(rowGroup.icon._colorSelected))
    end

end


-- menu touch handler
local function onMenuSelect(rowGroup, rowInfo, notHighlightWhenSelected)

        -- adds a alpha to label/icon to simulate a over effect
        rowGroup.label.alpha = .5
        rowGroup.icon.alpha = .5

        -- hiding any visible notification on this row
        rowGroup.notificationBackground.alpha = 0
        rowGroup.notificationNumber.alpha = 0

        if notHighlightWhenSelected ~= true then
            highlightRow(rowGroup)
        end

        timer.performWithDelay( 10, function()  -- we use a delay just to allow the alpha effect above already show on screen before trying to open the scene
            hide()
            if rowInfo.openScene then
                require("composer").gotoScene( rowInfo.openScene )
            elseif rowInfo.openURL then
                -- incluido por Bruna em 19/10/2015
                local eventFlurry = "outClick;Menu;Menu;" .. rowInfo.label .. ";Link Externo;" .. rowInfo.openURL .. ";"
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                --print(rowInfo.label)
                system.openURL( rowInfo.openURL )
            end

            rowGroup.label.alpha = 1
            rowGroup.icon.alpha = 1
        end)


end


-- creates a row in the menu
local function createRow(parentGroup, rowInfo, notHighlightWhenSelected)

	local rowGroup = display.newGroup()

	local icon = display.loadImage(rowGroup, rowInfo.icon)
    if icon.contentWidth > 15 then
        icon:scale(12/icon.contentWidth, 12/icon.contentWidth)
    end
	icon.x = icon.contentWidth*.5 + 10
	icon.y = icon.contentHeight*.5
    icon._colorSelected = {234/255,170/255,0}
    icon._colorNotSelected = {.9,.9,.9}
    icon:setFillColor(unpack(icon._colorNotSelected))
    rowGroup.icon = icon

	local label = display.newText{parent=rowGroup, text=rowInfo.label, font=APP_CONFIG["fontLight"], fontSize=20}
	label.anchorX = 0
	label.x = 34 --icon.x + icon.contentWidth*.5 + 10
	label.y = icon.y
	label:setTextColor( .9, .9 ,.9, .9)
    rowGroup.label = label

	local notification = display.loadImage(rowGroup, IMAGES.menu.notification)
	notification.anchorX = 0
	notification.x = label.x + label.contentWidth + 10
	notification.y = icon.y
	parentGroup:insert(rowGroup)
    notification.alpha = 0
    rowGroup.notificationBackground = notification

	local notificationNumber = display.newText{parent=rowGroup, text="99", font=APP_CONFIG["fontMedium"], fontSize=10}
	notificationNumber.x = notification.x + notification.contentWidth*.5
	notificationNumber.y = notification.y
    notificationNumber.alpha = 0
    rowGroup.notificationNumber = notificationNumber

	local rowWidth = menuWidth*.95
	local rowHeight = math.max(icon.contentHeight, label.contentHeight) + 4
	local background = display.newRect(rowWidth*.5,icon.y, rowWidth, rowHeight)
	background.fill = {0.3,0.3,0.3, .4}
    background.isVisible = false
	rowGroup:insert(1, background)
	background.isHitTestable = true
    rowGroup.background = background

	rowGroup:addEventListener( "tap", function()
        onMenuSelect(rowGroup, rowInfo, notHighlightWhenSelected)
	end )

    -- storing reference for the row as a dictionary  label -> rowGroup
    rows[string.lower(rowInfo.label)] = rowGroup

	return rowGroup

end


-- show the notification ballow in a row
showNotification = function(rowLabel, notificationQty)

    local row = rows[string.lower(rowLabel)]

    if row == nil then print("WARNING - menu row does not exist") return end

    if notificationQty < 1 then
        row.notificationBackground.alpha = 0
        row.notificationNumber.alpha = 0
    else
        row.notificationBackground.alpha = 1
        row.notificationNumber.alpha = 1
        if notificationQty > 1 then
            if notificationQty > 99 then notificationQty = 99 end
            row.notificationNumber.text = notificationQty
            row.notificationNumber.x = row.notificationBackground.x + row.notificationBackground.contentWidth*.5
        end

    end

end


-- creates the menu object
local new = function()



	local menu = display.newGroup()

	-- background
    local obj = {path="images/menu/menu_background.png", width = menuWidth, height = SCREEN_H}
    local background = display.loadImage(menu, obj)
    background.anchorX, background.anchorY = 0,0
    menu:insert(background)
    background:addEventListener( "tap", function() return true end )
    background:addEventListener( "touch", function() return true end )

    local invisibleBackground = display.newRect(menu, background.x + background.contentWidth,CENTER_Y,SCREEN_W-background.contentWidth, SCREEN_H)
    invisibleBackground.anchorX = 0
    invisibleBackground.alpha = 0
    invisibleBackground.isHitTestable = true
    invisibleBackground:addEventListener( "tap", function() hide() end )


    -------------------------------------------------------------------------------------
    -- HEADER
    ------------------------------------------------------------------------------------

    local headerLabel = display.newText( {parent=menu, text=APP_CONFIG.appName, x=20, y=40 , font=APP_CONFIG.fontLight, fontSize=20} )
    headerLabel.anchorX = 0
    headerLabel.x = 10
    headerLabel:setTextColor(255/255,240/255,0/255)


    local iconSettings = display.loadImage(menu, IMAGES.menu.settings)
    iconSettings.x = background.x + background.contentWidth - 70/3
    iconSettings.y = headerLabel.y
    iconSettings._colorSelected = {234/255,170/255,0}
    iconSettings._colorNotSelected = {.9,.9,.9}


    local settingsRowGroup = {icon=iconSettings, background={}, label={}, notificationBackground={}, notificationNumber = {}}
    iconSettings:addEventListener("tap", function()
        onMenuSelect(settingsRowGroup,
            {openScene="scene-settings"})

    end)
    rows["configurações"] = settingsRowGroup


    local verticalLineX = iconSettings.x - 70/3
    local verticalLine = display.newLine(menu, verticalLineX, iconSettings.y - iconSettings.contentHeight*.65, verticalLineX, iconSettings.y + iconSettings.contentHeight*.65 )
	verticalLine.strokeWidth = 1
	verticalLine:setStrokeColor( 64/255, 64/255, 68/255 )



	local invisibleBackgroundHeader = display.newRect(menu, 0, iconSettings.y,verticalLineX - headerLabel.x, verticalLine.contentHeight)
    invisibleBackgroundHeader.anchorX = 0
    invisibleBackgroundHeader.alpha = 0
    invisibleBackgroundHeader.isHitTestable = true
    invisibleBackgroundHeader:addEventListener( "tap", function()
    		require("composer").gotoScene( "scene-first" )
			hide()
	end )


    -- horizontal separator
    local lineY = iconSettings.y + iconSettings.contentHeight*.5 + 10
	local line = display.newLine(menu, 0,lineY, background.contentWidth,lineY)
	line.strokeWidth = 1
	line:setStrokeColor( 64/255, 64/255, 68/255 )


    -- Incluído por Bruna em 26/10/2015
    local platform = system.getInfo( "platformName" )
    --print(platform)
    local parceiro

    if _G.DEVICE.isApple then

        parceiro = "iTunes"

    elseif _G.DEVICE.isGoogle then

        parceiro = "Google Play"

    elseif _G.DEVICE.isKindleFire then

        parceiro = "Amazon"

    elseif platform == "WinPhone" then

        parceiro = "Windows Phone"

    else

        -- any other DEVICE (windows, html,..) will end up here

    end

    --print(deviceDiscography)

    -- fim


    -------------------------------------------------------------------------------------
    -- FEATURED ALBUM
    -------------------------------------------------------------------------------------

	-- album featured
	local albumFeatured = _G.FEATURED_ALBUM
	--local albumInfo = {path="images/dynamic/album_photo@3x.png", width = 279/3, height = 278/3}
	--local albumPhoto = display.loadImage(menu, albumInfo)
    local albumPhoto = display.loadImageFromInternet(menu, _G.FEATURED_ALBUM.getThumbnailObj())
    albumPhoto:scale(93/albumPhoto.contentWidth, 93/albumPhoto.contentWidth)
	albumPhoto.anchorX = 0
	albumPhoto.x = headerLabel.x
	albumPhoto.y = line.y + 10 + albumPhoto.contentHeight*.5


    local groupAlbumInfo = display.newGroup()
    menu:insert(groupAlbumInfo)

    local groupAlbumInfoLeft = albumPhoto.x + albumPhoto.contentWidth + 10
    local groupAlbumInfoWidth = (background.contentWidth - groupAlbumInfoLeft - 10)

    local lbTitle = display.newText{parent=groupAlbumInfo, text=albumFeatured.title, x = 0, y = 0, font=APP_CONFIG.fontLight, fontSize = 16, width=groupAlbumInfoWidth}
	--local lbTitle = display.newText{parent=groupAlbumInfo, text=albumFeatured.title, x = 0, y = 0, font=APP_CONFIG.fontLight, fontSize = 16}
	lbTitle.anchorX, lbTitle.anchorY = 0, 0
	lbTitle:setTextColor(255/255,240/255,0/255)

	local lbSubtitle = display.newText{parent=groupAlbumInfo, text=albumFeatured.subtitle or "", x = lbTitle.x , y = lbTitle.y + lbTitle.contentHeight, font=APP_CONFIG.fontLight, fontSize = 12}
	lbSubtitle.anchorX, lbSubtitle.anchorY = 0, 0

    local btH = 26
    local btW = (groupAlbumInfoWidth - lbTitle.x - 6 ) / 2
    local btY = albumPhoto.y + albumPhoto.contentHeight*.5 - btH*.5

    local btBuyAlbum
    local btBuyCDLabel
    if FEATURED_ALBUM.buyDigitalAlbumURL ~= nil and FEATURED_ALBUM.buyDigitalAlbumURL ~= "" then
        btBuyAlbum = require("library-widgets").newButton{
           id="buyDigitalAlbumURL",  -- using the property of the albumObj received by the server (and used on class-album)
           left = groupAlbumInfoLeft,
           y = btY, --1436*(320/1080)* _G.GROW_WITH_SCREEN_H,
           width = btW,
           height = btH,
           label = "COMPRAR\nÁLBUM DIGITAL",
           labelPadding = {top=1},
           labelColor = { 1,1,1 },
           labelLineSpacing = 4,
           --labelPadding ={bottom = 1},
           labelOverColor = { 0, 0, 0, 0.5 },
           backgroundColor = { 0,0,0, 1 },
           backgroundOverColor = { 0,0,0, .3 },

           labelFont = APP_CONFIG["fontMedium"],
           labelFontSize = 8,
           labelWrap = true,
           labelAlign = "center",
           onRelease = function()
                -- incluido por Bruna em 19/10/2015
                local eventFlurry = "outClick;Menu;Menu;Comprar Álbum Digital;" .. parceiro .. ";" .. albumFeatured.title .. ";"
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                system.openURL(FEATURED_ALBUM.buyDigitalAlbumURL)
           end
        }
        menu:insert(btBuyAlbum)
    end
    if FEATURED_ALBUM.buyDiscURL ~= nil and FEATURED_ALBUM.buyDiscURL ~= "" then
        local left = groupAlbumInfoLeft
        if btBuyAlbum then
            left = btBuyAlbum.x + btBuyAlbum.contentWidth*.5 + 3
        end
        btBuyCD = require("library-widgets").newButton{
            id="buyDiscURL",
            left =  left,
            y = btY ,
            width = btW,
            height = btH,
            label = "COMPRAR CD",
            labelPadding = {top=1},
            labelColor = { 1,1,1 },
            labelOverColor = { 0, 0, 0, 0.5 },
            backgroundColor = {184/255,0,16/255,1},
            backgroundOverColor = { 184/255,0,16/255,.3},

            labelFont = APP_CONFIG["fontMedium"],
            labelFontSize = 8,
            labelWrap = true,
            labelAlign = "center",
            onRelease = function()
                -- incluido por Bruna em 19/10/2015
                local eventFlurry = "outClick;Menu;Menu;Comprar CD;MK Shopping;" .. albumFeatured.title .. ";"
                print(eventFlurry)
                _G.analytics.logEvent(eventFlurry)
                -- fim
                system.openURL(FEATURED_ALBUM.buyDiscURL)
            end
        }
        menu:insert(btBuyCD)
    end



	local albumDescriptionLabelY = lbSubtitle.y + lbSubtitle.contentHeight + 8
    if lbSubtitle.text == "" then
        lbSubtitle.isVisible = false
        albumDescriptionLabelY = lbTitle.y + lbTitle.contentHeight
    end

    local albumDescriptionLabel = display.newText2{text=albumFeatured.description, font=APP_CONFIG.fontLight, fontSize=12, width = groupAlbumInfoWidth, color={1,1,1}}
        albumDescriptionLabel.x = lbSubtitle.x
        albumDescriptionLabel.y = albumDescriptionLabelY
        groupAlbumInfo:insert(albumDescriptionLabel)

    local svH = albumPhoto.contentHeight
    if btBuyAlbum or btBuyCD then
        svH = svH - btH - 4
    end
    local svDescription = require("widget").newScrollView{
        x = groupAlbumInfoLeft + groupAlbumInfoWidth*.5,
        y = albumPhoto.y - albumPhoto.contentHeight*.5 + svH*.5,
        height = svH,
        width = groupAlbumInfoWidth,
        hideBackground = true,
        horizontalScrollDisabled = true,
        bottomPadding = 10,
        hideScrollBar = true,
    }
    menu:insert(svDescription)
    svDescription:insert(groupAlbumInfo)



    -- horizontal separator
    lineY = albumPhoto.y + albumPhoto.contentHeight*.5 + 10
    local line = display.newLine(menu, 0,lineY, background.contentWidth,lineY) --132)
	line.strokeWidth = 1
	line:setStrokeColor( 64/255, 64/255, 68/255 )



	-------------------------------------------------------------------------------------
    -- MENU FEATURED
    -------------------------------------------------------------------------------------

    local menuEntries ={
        {icon=IMAGES.menu.settings, label="Configurações", openScene="scene-settings"},
    	{icon=IMAGES.menu.news, label="Notícias", openScene="scene-news"},
    	{icon=IMAGES.menu.agenda, label="Agenda", openScene="scene-agenda"},
    	{icon=IMAGES.menu.profile, label="Perfil", openScene="scene-profile"},
    	{icon=IMAGES.menu.video, label="Vídeos", openScene="scene-videos"},
    	{icon=IMAGES.menu.photos, label="Fotos", openScene="scene-photos"},
    	{icon=IMAGES.menu.disc, label="Discografia", openScene="scene-discography"},
    	{icon=IMAGES.menu.lyrics, label="Letras", openScene="scene-lyrics"},
    	{icon=IMAGES.menu.radio, label="Rádios On-Line", openScene="scene-radios"},
    	{icon=IMAGES.menu.preview, label="Previews", openScene="scene-preview"},
    	{icon=IMAGES.menu.direct_from_artist, label="Direto do Artista", openScene="scene-direct-from-artist"},
    	{icon=IMAGES.menu.other_artists, label="Outros Artistas", openScene="scene-other-artists"},

	}


	local groupMenuEntries = display.newGroup()
	menu:insert(groupMenuEntries)

    local rowH
	for i=1, #menuEntries do
		local row = createRow(groupMenuEntries, menuEntries[i])
        rowH = row.contentHeight
		row.y = (i-1)*(rowH+8)
	end



	lineY = (#menuEntries)*(rowH+8)
	local separator = display.newLine(groupMenuEntries, 0,lineY, background.contentWidth,lineY)
	separator.strokeWidth = 1
	separator:setStrokeColor( 64/255, 64/255, 68/255 )

	menuEntries ={
    	{icon=IMAGES.menu.digital_booklet, label="Encarte Digital", openURL = "http://encartedigitalmk.com.br/"},
    	{icon=IMAGES.menu.site, label="Site Oficial " .. APP_CONFIG.appName, openURL = APP_CONFIG.artistSite},
    	{icon=IMAGES.menu.site, label="Site Oficial MK Music", openURL = "http://mkmusic.com.br/"},
        {icon=IMAGES.menu.site, label="Site MK Shopping", openURL = "http://mkshopping.com.br/"},
	}


	for i=1, #menuEntries do
		local row = createRow(groupMenuEntries, menuEntries[i], true)
        rowH = row.contentHeight
		row.y = lineY + 10 + (i-1)*(rowH+8)
	end





	local groupScrollViewContent = groupMenuEntries

	local scrollViewTop = line.y
	local scrollViewH = SCREEN_H - scrollViewTop

    local shouldDisableSrollView = ( groupScrollViewContent.contentHeight < scrollViewH )


    -- creates the scrollview
    local scrollView = require("widget").newScrollView{
       x = menu.contentWidth*.5,
       y = scrollViewTop + scrollViewH*.5,
       width = menu.contentWidth,
       height = scrollViewH,
       hideBackground = true,
       horizontalScrollDisabled = true,
       verticalScrollDisabled = shouldDisableSrollView,
       topPadding = 10,
       bottomPadding = 10,
       hideScrollBar = true,
    }
    menu:insert(scrollView)
    scrollView:insert(groupScrollViewContent)


	-- hiding the menu
	menu.x = -menu.contentWidth


    -- PUBLIC FUNCTIONS
   	menu.show = show
   	menu.hide = hide
    menu.showNotification = showNotification
    menu.highlightMenuWithLabel = highlightMenuWithLabel



   	-- updating menu pointer
	m.menu = menu




    -- making the tabBar to be displayed above the composer scenes
    local stage = display.getCurrentStage()
    stage:insert( require( "composer" ).stage )
    stage:insert( menu )
    stage:insert(_G.statusBarBackground)

   	return menu

end


highlightMenuWithLabel = function(label)
    highlightRow(label and rows[string.lower(label)])
end



-------------------
-- PUBLIC FUNCTIONS

m.new = new


return m

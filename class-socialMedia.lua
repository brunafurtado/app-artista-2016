local socialMedia = {}


	socialMedia.new = function(params)

		local obj = {}

		obj.network = params.network		-- "facebook" , "twitter", "instragram"
		obj.profileName = params.profileName
		obj.profileHandler = params.profileHandler
		obj.profileAvatarURL = params.profileAvatarURL
		
		obj.posts = params.posts 
		-- for i=1, #params.posts do
		-- 	obj.posts[i] = {}
		-- 	obj.posts[i].content = params.posts[i].content
		-- 	obj.posts[i].datetime = params.posts[i].datetime	-- date obj {date, month, year}
		-- 	obj.posts[i].image = params.posts[i].image  		-- image obj {url, width, height}
		-- end
		
		return obj

	end


	

	-- merge the feeds, having the post in inverted cronologic area (most recent first)
	socialMedia.mergeFeeds = function(feed1, feed2, feed3)
	
		local mergedTable = table.mergeTablesOrderedByDate(feed1, feed2)

		mergedTable = table.mergeTablesOrderedByDate(mergedTable, feed3)


		return mergedTable

	end



	local socialMediaFeed = {}
	local socialMediaFeedLastFetch = {}


	local fetchCount = 0


	--  feed = mergedFeedOfTwitterAndFacebookAndInstagram
	socialMedia.formatFeed = function(feed, networkInfo)  

		socialMediaFeedLastFetch = {}

		local function createSocialMediaObj(startIndex, endIndex)

			local posts = {}
			for i=startIndex, endIndex do
				posts[#posts+1] = feed[i]

			end

			local networkName = posts[1].network
			local networkDetails = networkInfo[networkName]
			return socialMedia.new({ 
		        network=posts[1].network,
		        profileName = networkDetails.profileName,
		        profileHandler = networkDetails.profileHandler,
		        profileAvatarURL = networkDetails.profileAvatarURL,
		        posts=posts, 
		        
		    })  

		end


		local startIndex = 1
		local endIndex = 0

		for i=0, #feed do
			local hasChangedNetwork = false
            if i>1 then
                if feed[i] ~= nil then
                    hasChangedNetwork = true
                    print("feed[i].network =", feed[i].network )

                end
            end
                -- (feed[i].network ~= feed[i-1].network)


			if hasChangedNetwork then
				startIndex = endIndex + 1
				endIndex = i-1
				local socialMediaObj = createSocialMediaObj(startIndex, endIndex)

				socialMediaFeed[#socialMediaFeed+1] = socialMediaObj
				socialMediaFeedLastFetch[#socialMediaFeedLastFetch+1] = socialMediaObj
			end	
						
		end

		startIndex = endIndex + 1
		endIndex = #feed
		local socialMediaObj = createSocialMediaObj(startIndex, endIndex)

		socialMediaFeed[#socialMediaFeed+1] = socialMediaObj
		socialMediaFeedLastFetch[#socialMediaFeedLastFetch+1] = socialMediaObj

		fetchCount = fetchCount + 1

		return socialMediaFeedLastFetch



	end





return socialMedia
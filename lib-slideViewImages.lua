-- slideViewImages.lua
-- based on Conrona slideView sample code (Version 1.0).
--
-- v2
--
--
--
--
-- slideView.lua
-- 
-- Version 1.0 
--
-- Copyright (C) 2010 Corona Labs Inc. All Rights Reserved.
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of 
-- this software and associated documentation files (the "Software"), to deal in the 
-- Software without restriction, including without limitation the rights to use, copy, 
-- modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
-- and to permit persons to whom the Software is furnished to do so, subject to the 
-- following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all copies 
-- or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
-- DEALINGS IN THE SOFTWARE.



local sv = {}

local screenW, screenH = display.contentWidth, display.contentHeight
local viewableScreenW, viewableScreenH = display.viewableContentWidth, display.viewableContentHeight
local screenOffsetW, screenOffsetH = display.contentWidth -  display.viewableContentWidth, display.contentHeight - display.viewableContentHeight

local currColumnIndex = nil
local columns = nil
local touchListener, nextImage, prevImage, cancelMove, initImage
local background
local imageNumberText, imageNumberTextShadow

sv.new = function( options )

	-- receiving params
	local imageSet = options.imageSet
	local slideBackground = options.slideBackground
	local top = options.top
	local bottom = options.bottom
	local columnRender = options.columnRender
	local onShow = options.onShow
	local numColumns = options.numColumns
	local initialIndex = options.initialIndex or 3



	local pad = 0
	local top = top or 0 
	local bottom = bottom or 0
	local background


	local groupSlideView = display.newGroup()

	


	-- render the column with index
	local preLoadFunction = function(index)

		if index > numColumns or index <= 0 then return end

		if columns[i] and columns[i].x then print("image already lodaded"); return end


		local p = columnRender({index=index})
		local h = viewableScreenH-(top+bottom)
		-- if p.width > viewableScreenW or p.height > h then
		-- 	if p.width/viewableScreenW > p.height/h then 
		-- 			p.xScale = viewableScreenW/p.width
		-- 			p.yScale = viewableScreenW/p.width
		-- 	else
		-- 			p.xScale = h/p.height
		-- 			p.yScale = h/p.height
		-- 	end		 
		-- end
		local groupColumn = display.newGroup()
		groupColumn.anchorChildren = true
		groupColumn.anchorX, groupColumn.anchorY = 0.5, 0.5
		groupColumn:insert(p)

		groupSlideView:insert(groupColumn)
	    
	    -- positioning group
		groupColumn.x = screenW*1.5 + pad -- all images offscreen except the first one	
		groupColumn.y = h*.5

		-- storing reference
		columns[index] = groupColumn

	end

	-- remove the column
	local destroyFunction = function(index)

		if index > numColumns or index <= 0 then return end

		display.remove(columns[index])
		columns[index] = {}

	end

	
	
	-- slideview listener
	function touchListener (self, touch) 
		local phase = touch.phase
		
		if ( phase == "began" ) then
            -- Subsequent touch events will target button even if they are outside the contentBounds of button
            display.getCurrentStage():setFocus( self )
            self.isFocus = true

			startPos = touch.x
			prevPos = touch.x
															
        elseif( self.isFocus ) then
        
			if ( phase == "moved" ) then						
						
				if tween then transition.cancel(tween) end
					print("currImg=", currColumnIndex)
				local delta = touch.x - prevPos
				prevPos = touch.x
				
				columns[currColumnIndex].x = columns[currColumnIndex].x + delta
				
				if (columns[currColumnIndex-1]) then
					columns[currColumnIndex-1].x = columns[currColumnIndex-1].x + delta
				end
				
				if (columns[currColumnIndex+1]) then
					columns[currColumnIndex+1].x = columns[currColumnIndex+1].x + delta
				end

			elseif ( phase == "ended" or phase == "cancelled" ) then
				
				dragDistance = touch.x - startPos
				--print("dragDistance: " .. dragDistance)
				
				if (dragDistance < -40 and currColumnIndex < numColumns) then
					nextImage()
				elseif (dragDistance > 40 and currColumnIndex > 1) then
					prevImage()
				else
					cancelMove()
				end
									
				if ( phase == "cancelled" ) then		
					cancelMove()
				end

                -- Allow touch events to be sent normally to the objects they "hit"
                display.getCurrentStage():setFocus( nil )
                self.isFocus = false
														
			end
		end
					
		return true
		
	end
	
	

	

	function cancelTween()
		if prevTween then 
			transition.cancel(prevTween)
		end
		prevTween = tween 
	end
	
	function nextImage()
		tween = transition.to( columns[currColumnIndex], {time=400, x=(screenW*.5 + pad)*-1, transition=easing.outExpo } )
		tween = transition.to( columns[currColumnIndex+1], {time=400, x=screenW*.5, transition=easing.outExpo } )
		currColumnIndex = currColumnIndex + 1
		initImage(currColumnIndex, 1)
	end
	
	function prevImage()
		tween = transition.to( columns[currColumnIndex], {time=400, x=screenW*1.5+pad, transition=easing.outExpo } )
		tween = transition.to( columns[currColumnIndex-1], {time=400, x=screenW*.5, transition=easing.outExpo } )
		currColumnIndex = currColumnIndex - 1
		initImage(currColumnIndex, -1)
	end
	
	function cancelMove()
		tween = transition.to( columns[currColumnIndex], {time=400, x=screenW*.5, transition=easing.outExpo } )
		tween = transition.to( columns[currColumnIndex-1], {time=400, x=(screenW*.5 + pad)*-1, transition=easing.outExpo } )
		tween = transition.to( columns[currColumnIndex+1], {time=400, x=screenW*1.5+pad, transition=easing.outExpo } )
	end
	
	function initImage(num, direction)

		if direction then
			-- preloading images
			preLoadFunction(num + 2*direction)

			-- destroying former images
			destroyFunction(num - 3*direction)
		end

		if (num < numColumns) then
			columns[num+1].x = screenW*1.5 + pad			
		end
		if (num > 1) then
			columns[num-1].x = (screenW*.5 + pad)*-1
		end

		-- triggering external show function
		if onShow then
			onShow({index=num})
		end
		
	end



	----------------------------------
	-- PUBLIC FUNCTIONS

	function groupSlideView:jumpToImage(num)
		local i
		
		for i = 1, table.maxn(columns) do
			if columns[i] then
				if i < num then
					columns[i].x = -screenW*.5;
				elseif i > num then
					columns[i].x = screenW*1.5 + pad
				else
					columns[i].x = screenW*.5 - pad
				end
			end
		end
		currColumnIndex = num
		initImage(currColumnIndex)
	end

	function groupSlideView:destroy()
		print("slides cleanUp")
		background:removeEventListener("touch", touchListener)
	end


	----------------------------------
	-- RUN CODE


	columns = {}


	-- creates background
	if slideBackground then
		background = display.newImage(slideBackground, 0, 0, true)
	else
		background = display.newRect( 0, 0, screenW, screenH-(top+bottom) )

		-- set anchors on the background
		background.anchorX = 0
		background.anchorY = 0

		background:setFillColor(0, 0, 0)
	end
	groupSlideView:insert(background)
	
	-- loading the first group (and adjacentes) to be shown
	for i = initialIndex-2,initialIndex+2 do
		preLoadFunction(i)
	end
	
	-- showing the first group
	groupSlideView:jumpToImage(initialIndex)
	

	-- positiong the entire slideview
	groupSlideView.x = 0
	groupSlideView.y = top + display.screenOriginY

	-- adding touch listener on slideview background
	background.touch = touchListener
	background:addEventListener( "touch", background )


	return groupSlideView	
end

return sv

local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


local fetchQty = 10
local pageIndex = 0

local scrollViewSelected = nil



-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.

    local sceneAssets = _G.IMAGES.discography

    local topBar = require("module-topBar").new(sceneGroup, "Letras")
    sceneGroup:insert(topBar)

    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}


    --------------------------------------------------
    -- REFRESHING FUNTIONS
    --------------------------------------------------


    local groupRefreshing
    local function showScrollViewRefreshing()

        if groupRefreshing then return end  -- if already being shown, don't show again

        groupRefreshing = require("module-frames").newRefreshingGroup()
        sceneGroup:insert(groupRefreshing)
        groupRefreshing.y = scrollViewSelected.y - scrollViewSelected.contentHeight*.5
        groupRefreshing.x = scrollViewSelected.x - scrollViewSelected.contentWidth*.5

        return groupRefreshing

    end

    local function removeScrollViewRefreshing()
        if groupRefreshing then
            display.remove(groupRefreshing)
            groupRefreshing = nil
            timer.performWithDelay( 10, function()
                scrollViewSelected:scrollTo( "top", {
                    time=800,
                    --onComplete=function() print("finshied scrolling to top") end
                    })
            end)
        end
    end


    --------------------------------------------------
    -- FOOTER FUNTIONS
    --------------------------------------------------

    local function addScrollViewFooter(hasMoreToLoad)

        local view = scrollViewSelected:getView()

        local groupFooter = display.newGroup()
        groupFooter.y = (#scrollViewSelected._data == 0 and 0) or view.contentHeight
        scrollViewSelected:insert(groupFooter)
        scrollViewSelected.groupFooter = groupFooter


        if hasMoreToLoad then

            local btLoadMore = require("library-widgets").newButton{
                    top=14,
                    x = CENTER_X,
                    --width = 160,
                    height = 30,
                    backgroundColor={1,0,0,0},
                    backgroundOverColor={1,0,1,0},
                    label="CARREGAR MAIS",
                    labelFontSize = 8,
                    labelFont = APP_CONFIG.fontMedium,
                    labelColor = {26/255,118/255,173/255},
                    labelOverColor = {26/255,118/255,173/255,.3},

                    imageFile = APP_CONFIG.images.icons.plus.path,
                    imageWidth = APP_CONFIG.images.icons.plus.width,
                    imageHeight = APP_CONFIG.images.icons.plus.height,
                    imageColor = {26/255,118/255,173/255},
                    imageOverColor = {26/255,118/255,173/255,.3},
                    imagePosition="left",
                    imagePadding = {right = 4},

                    onRelease = function(e)
                        sceneGroup.loadMoreSpinner = AUX.showSpinner(e.target)
                        e.target.isVisible = false
                        sceneGroup.getContent()
                    end,
            }
            groupFooter:insert(btLoadMore)

        else

            local lbNoMoreToLoad = display.newText{text="NÃO HÁ MAIS LETRAS", font=APP_CONFIG.fontMedium, fontSize=8}
            lbNoMoreToLoad.x = CENTER_X
            lbNoMoreToLoad.y = 29
            lbNoMoreToLoad:setTextColor( 26/255,118/255,173/255)

            groupFooter:insert(lbNoMoreToLoad)

        end

    end


    local function removeScrollViewFooter()
        display.remove(scrollViewSelected.groupFooter)
    end


    --------------------------------------------------
    -- SCROLLVIEWS
    --------------------------------------------------


    local scrollViewTop = topBar.y + topBar.contentHeight
    local scrollViewHeight = SCREEN_H - scrollViewTop

    -- ScrollView listener
    local function scrollListener( event )

            local phase = event.phase
            if ( phase == "began" ) then print( "Scroll view was touched" )
            --elseif ( phase == "moved" ) then
            --elseif ( phase == "ended" ) then print( "Scroll view was released" )
            end

            -- In the event a scroll limit is reached...
            if ( event.limitReached ) then
                if ( event.direction == "up" ) then print( "Reached top limit" )
                elseif ( event.direction == "down" ) then print( "Reached bottom limit" )

                    if groupRefreshing or sceneGroup.loading then
                        print("there is already a refreshing in progress")
                        return
                    end


                    local x, y = scrollViewSelected:getContentPosition()

                    if y > 50 then
                        local function onScrollComplete()
                            print( "Scroll complete!" )
                            scrollViewSelected:setIsLocked( true )
                        end


                        local groupRefreshing = showScrollViewRefreshing()

                        scrollViewSelected:scrollToPosition{
                            --y = 50,
                            y = groupRefreshing.contentHeight + 10,
                            time = 800,
                            onComplete = onScrollComplete
                        }

                        timer.performWithDelay( 10, function() sceneGroup.getContent(nil, true) end )
                    end

                end
            end

            return true
        end

    local scrollViewOptions = {
       x = CENTER_X,
       y = scrollViewTop + scrollViewHeight*.5,
       width = SCREEN_W,
       height = scrollViewHeight,
       hideBackground = true,
       hideScrollBar = true,
       backgroundColor = { 0.8, 0.8, 0},
       horizontalScrollDisabled = true,
       topPadding = 10,
       bottomPadding = 20,
       listener = scrollListener,
    }


    -- creates the scrollview for Albums (1st button)
    local scrollViewAlbums = require("widget").newScrollView(scrollViewOptions)
    sceneGroup:insert(scrollViewAlbums)
    scrollViewAlbums._data = {}
    scrollViewAlbums._content = display.newGroup()
    scrollViewAlbums._media = "album"
    scrollViewAlbums._pageIndex = 0

    scrollViewSelected = scrollViewAlbums




    -- adds/appends content to scrollView
    local function addContentToScrollView(group)

        local view = scrollViewSelected:getView()

        local groupTop = (#scrollViewSelected._data == 0 and 0) or (view.contentHeight + 14)
        group.x = CENTER_X - group.contentWidth*.5
        group.y = groupTop

        scrollViewSelected:insert(group)

    end


    -- get content from server
    sceneGroup.getContent = function(onFinish, resetData)
        isLoading = true

        if resetData then
            pageIndex = 0
        end


        local function onCallBack(event)

            scrollViewSelected:setIsLocked( false )
            if event.result ~= true then
                return print("something wrong happened with the data of discography")

            end

            if resetData then
                display.remove(scrollViewSelected._content)
                scrollViewSelected._content = nil
                scrollViewSelected._content = display.newGroup()
                scrollViewSelected._data = {}
            end


            --local data = event.data[media]
            local data = event.data[string.lower(scrollViewSelected._media)]

            if (data == nil) then
                data = ""
            end

            for i= #data,1,-1 do
                local hasLyric = false
                for j=1,#data[i].musics do
                   if data[i].musics[j].hasLyric ~= false then
                        hasLyric = true
                    end
                end
                if hasLyric == false then
                    table.remove(data,i)
                    --print("removidp album")
                end


            end



            removeScrollViewRefreshing()

            removeScrollViewFooter()


            local function createScrollViewContentGroup(data, scrollViewObj, isDVD)

                local groupContent = scrollViewSelected._content

                for i=1, #data do

                    local frame, hasLyric = require("module-frames").albumEntry(data[i], false, nil,scrollViewObj, isDVD)

                    print(hasLyric)

                    if hasLyric == false then
                        display.remove(frame)
                    else

                        scrollViewSelected._data[#scrollViewSelected._data+1] = data[i]

                        local frameY = groupContent.contentHeight

                        if groupContent.numChildren == 0 then
                            frameY = 0
                        end

                        frame.y = frameY + 50/3
                        frame.x = CENTER_X - frame.contentWidth*.5
                        groupContent:insert(frame)

                        frame._scrollView = scrollViewObj
                        frame.isDVD = isDVD

                         if i<#data then

                            local lineY = groupContent.contentHeight + 20
                            local line = display.newLine(groupContent, frame.x,lineY, frame.x + frame.contentWidth ,lineY)
                            line.strokeWidth = 1
                            line:setStrokeColor( 207/255, 207/255, 207/255 )

                            -- adding a space on the bottom of the frame, so we can have the frames separatated on the tableview
                            local invisibleRect = display.newRect(groupContent, frame.x,lineY, frame.contentWidth, 20)
                            invisibleRect.anchorX, invisibleRect.anchorY = 0, 0
                            invisibleRect.alpha = 0
                        end
                    end


                end

                local scrollViewBackground = groupContent._scrollViewBackground

                if scrollViewBackground == nil then
                    -- incluido por Bruna em 10/11/2015
                    if (groupContent.numChildren ~= 0) then
                    -- fim
                        scrollViewBackground = display.newRect(CENTER_X, groupContent.y + groupContent.contentHeight*.5, groupContent.contentWidth, groupContent.contentHeight+20)
                        scrollViewBackground.anchorY = 0
                        scrollViewBackground.y = groupContent.y
                        scrollViewBackground:setFillColor(1,1,1)
                        scrollViewBackground.strokeWidth = 1
                        scrollViewBackground:setStrokeColor( 207/255, 207/255, 207/255 )
                        groupContent:insert(1, scrollViewBackground)
                        groupContent._scrollViewBackground = scrollViewBackground
                    -- incluido por Bruna em 10/11/2015
                    end
                    -- fim
                else
                    scrollViewBackground.height = groupContent.contentHeight
                    scrollViewBackground.y = groupContent.y

                end

                return groupContent

            end


            local svContentSelected = createScrollViewContentGroup(data, scrollViewSelected, media == "DVD")
            scrollViewSelected:insert(svContentSelected)
            scrollViewSelected._content = svContentSelected

            addScrollViewFooter(#data > 0)


            transition.from(svContentSelected,{alpha=0, time=400})

            display.remove(sceneGroup.loading)
            sceneGroup.loading = nil

            if onFinish then
                onFinish()
            end

        end

        pageIndex = pageIndex + 1
        scrollViewSelected._pageIndex = pageIndex
        SERVER.getDiscography(scrollViewSelected._media, fetchQty, pageIndex, onCallBack)

    end



    sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y)
    sceneGroup:insert(sceneGroup.loading)

    sceneGroup.getContent()


end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
        adsHelper.showInterstitial()

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
            _G.rbBack.addBack()

            _G.MENU.highlightMenuWithLabel("Letras")

            -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
            local eventFlurry = "view;Letras;Letras;"
            print(eventFlurry)
            _G.analytics.logEvent(eventFlurry)
            -- fim

    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
        PLAYER.stop(true)

    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene

local featuredAlbum = {}

featuredAlbum.title = nil
featuredAlbum.subtitle = nil
featuredAlbum.artist = nil
featuredAlbum.description = nil
featuredAlbum.barcode = nil
featuredAlbum.buyDiscURL = nil
featuredAlbum.buyDigitalAlbumURL = nil

featuredAlbum.image = {}
featuredAlbum.image["thumbnail"] = nil 	-- {url=nil, width=nil, height = nil}
featuredAlbum.image["@1x"] = nil 		-- {url=nil, width=nil, height = nil}
featuredAlbum.image["@2x"] = nil		-- {url=nil, width=nil, height = nil}
featuredAlbum.image["@3x"] = nil		-- {url=nil, width=nil, height = nil}
featuredAlbum.imageKey = nil			-- key (index) of the image being used for this device


featuredAlbum.download = function(onCallBack)

	SERVER.getFeaturedAlbum(function(e)

			if e.result ~= true then print("an error ocurring getting the featured album"); return end

			local data = e.data
			featuredAlbum.title = data.title
			featuredAlbum.subtitle = data.subtitle
			featuredAlbum.artist = data.artist
			featuredAlbum.description = data.description
			featuredAlbum.barcode = data.barcode
			featuredAlbum.buyDiscURL = data.buyDiscURL
			featuredAlbum.buyDigitalAlbumURL = data.buyDigitalAlbumURL
			featuredAlbum.image = data.image

			-- finding the best image to download for this device
			local deviceW = display.pixelWidth
			print("deviceW=", display.pixelWidth)
			local key, minDif = nil, nil
			for k, v in pairs(featuredAlbum.image) do
				local dif =  math.abs(deviceW - v.width)
				if minDif == nil or dif < minDif then
					key = k
					minDif = dif
				end
			end
			featuredAlbum.imageKey = key

			local i = 0
			local onFinishDownload = function()
				i=i+1
				print("onFinishDownload ", i)
				if i == 2 then
					if onCallBack then
						onCallBack()
					end
				end
			end

			SERVER.downloadImage(featuredAlbum.image["thumbnail"].url, onFinishDownload)
			SERVER.downloadImage(featuredAlbum.image[key].url, onFinishDownload)

		end)


end

featuredAlbum.getThumbnailObj = function()

	return featuredAlbum.image.thumbnail

end

featuredAlbum.getImageObj = function()

	return featuredAlbum.image[featuredAlbum.imageKey]

end



return featuredAlbum
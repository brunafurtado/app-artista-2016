adsHelper = require"adsHelper"

__DEBUG__ = false
require"appDebug"

-- This function gets called when the user opens a notification or one is received when the app is open and active.
-- Change the code below to fit your app's needs.
function DidReceiveRemoteNotification(message, additionalData, isActive)
    if (additionalData) then
        if (additionalData.discount) then
            native.showAlert( "Discount!", message, { "OK" } )
            -- Take user to your app store
        elseif (additionalData.actionSelected) then -- Interactive notification button pressed
            native.showAlert("Button Pressed!", "ButtonID:" .. additionalData.actionSelected, { "OK"} )
        end
    else
        native.showAlert("OneSignal Message", message, { "OK" } )
    end
end

_G.APP_CONFIG = require "appConfig"

ONESIGNAL = require("plugin.OneSignal")
--OneSignal.Init("a863c5c4-37b4-11e5-8456-77be84972d8a", "306343297282", DidReceiveRemoteNotification)
ONESIGNAL.Init(_G.APP_CONFIG["oneSignalApplicationKEY"], _G.APP_CONFIG["oneSginalGoogleProjectNumber"], DidReceiveRemoteNotification)


-- definying globals
_G.CENTER_X = display.contentCenterX
_G.CENTER_Y = display.contentCenterY
_G.SCREEN_W = display.contentWidth
_G.SCREEN_H = display.contentHeight


_G.GROW_WITH_SCREEN_W = (SCREEN_W / 320)
_G.GROW_WITH_SCREEN_H = (SCREEN_H / 568)

_G.LAYOUT_RATIO = (320/1080) -- standard content size  (used to code) versus the layout (PSD) given


_G.IMAGES = APP_CONFIG.images


 --loads app background

_G.BACKGROUND = display.newRect(display.contentWidth*.5, display.contentHeight*.5, display.contentWidth, display.contentHeight)
_G.BACKGROUND.fill = {0/255, 135/255, 158/255}


display.setStatusBar( display.TranslucentStatusBar )

-- creates the statusBar background
local statusBarH = display.topStatusBarContentHeight + 2

_G.DEVICE = require("rb-device")

if DEVICE.isAndroid then
    statusBarH = 0;
end
_G.statusBarBackground = display.newRect(display.contentWidth*0.5,statusBarH*0.5,display.contentWidth,statusBarH)
_G.TOP_Y_AFTER_STATUS_BAR = _G.statusBarBackground.contentHeight
_G.statusBarBackground.isVisible = false


math.randomseed( os.time() )


require("rb-back") -- we load this module here so we can start tracking the scenes


timer.performWithDelay(10, function()
    local composer = require("composer");

    composer.gotoScene("scene-loading")
end)


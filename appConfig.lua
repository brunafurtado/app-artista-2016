local appConfig = {}


-------------------------------------------------------------------
-- Modifique este arquivo para customizar o app para outro artista
-------------------------------------------------------------------


-- APP OVERALL INFO
appConfig["appName"] = "Elaine Martins"


-- ARTIST INFO
appConfig["artistName"] = {first = "Elaine", last="Martins"}
appConfig["albumName"] = "ROMPENDO"
appConfig["artistSite"] = "http://www.elainemartinsoficial.com.br/"


-- API/SERVER INFO
appConfig["serverURL"] = "http://www.elainemartinsoficial.com.br/json/"
appConfig["serverKEY"] = "jsonmk"
appConfig["serverPASS"] = "JsonMK2015"

-- PUSH NOTIFICATION SERVER (ONESIGNAL  -  www.onesignal.com)
appConfig["oneSignalApplicationKEY"] = "48b6471e-677f-11e5-93c3-8f0e23ddf7f6"
appConfig["oneSginalGoogleProjectNumber"] = "35214252838"


-- ANALYTICS KEY (FLURRY  -  www.flurry.com)
appConfig["flurryAppleKey"] = "2SN5DB6YBH5WC43PP7VN"
appConfig["flurryGoogleKey"] = "N6FHQMZ5KBFFWR57C798"
appConfig["flurryAmazonKey"] = "TC4R6KJP9P9DDCFSX4RC"
appConfig["flurryWindowsKey"] = "FR5FMYZ9F7G758XZYX28"

-- SOCIAL NETWORKS API INFO
appConfig["twitterKEY"] = "UGawEuS4Tfoqc01LZvsuGhXRo"
appConfig["twitterPASS"] = "ktTPkoSfT1RvAccMUkTKFOkg5mzTGQ7ghAZxVghJyb8D7Oxu4E"

-- appConfig["facebookKEY"] = "xxxxx"
-- appConfig["facebookPASS"] = "xxxxx"

-- appConfig["instagramKEY"] = "xxxx"
-- appConfig["instagramPASS"] = "xxxxx"




-- ARTIST SOCIAL ACCOUNTS INFO
appConfig["twitterHandler"] = "elainemartins1"  				    -- twitter handler without the '@'
appConfig["facebookPageID"] =  "194698883953908"			-- you can get a page ID by using the FB Graph API Explorer. Eg.: https://developers.facebook.com/tools/explorer/391660947598993/?method=GET&path=BrunaKarlaOficial&version=v2.4


-- FONTS
appConfig["fontLight"] = "Helvetica35-Thin"
appConfig["fontMedium"] = "Helvetica65-Medium"
appConfig["fontLarge"] = "Gotham-Black"


-- ADS
appConfig.ads=
{
    ANDROID_AD_UNIT_ID = "ca-app-pub-7629839498805590/8221775048",
    IOS_AD_UNIT_ID = "ca-app-pub-7629839498805590/3651974642",
    provider = "admob",
    showSkip = 5,
    showDelay = 3000, -- milisegundos
}



--------------------------------------
-- SCENE ASSETS


appConfig.images = {}

-- path = full path of the asset
-- width = width on the screen (usually is the same as the image at @1x scale)
-- height = height on the screen (usually is the same as the image at @1x scale)



-- ICONS
appConfig.images.icons = {}
appConfig.images.icons["share"] = {path="images/icons/icon_share@3x.png", width=45/3, height=38/3}
appConfig.images.icons["arrowLeft"] = {path="images/icon_arrow@3x.png", width=74/5, height=55/5}
appConfig.images.icons["plus"] = {path="images/icons/icon_plus@3x.png", width=36/3, height=36/3}
appConfig.images.icons["camera"] = {path="images/icons/icon_camera@3x.png", width=42/3, height=36/3}
appConfig.images.icons["more"] = {path="images/icons/icon_more@3x.png", width=46/3, height=12/3}
appConfig.images.icons["loading"] = {path="images/icons/icon_loading@3x.png", width=24, height=24}


-- TOP BAR
appConfig.images.topBar = {}
appConfig.images.topBar["background"] = {path="images/top_bar/background.png", width=display.contentWidth, height=111}
appConfig.images.topBar["menu"] = {path="images/top_bar/icon_menu.png", width=23, height=13}


-- MENU
appConfig.images.menu = {}
appConfig.images.menu["background"] = {path="images/menu/menu_background.png", width = menuWidth, height = display.contentHeight}
appConfig.images.menu["settings"] = {path="images/menu/icon_settings@3x.png", width = 49/3, height = 49/3}
appConfig.images.menu["notification"] = {path="images/menu/icon_notification@3x.png", width = 59/3, height = 38/3}
appConfig.images.menu["news"] = {path="images/menu/icon_news@3x.png", width = 31/3, height = 36/3}
appConfig.images.menu["agenda"] = {path="images/menu/icon_agenda@3x.png", width = 34/3, height = 34/3}
appConfig.images.menu["profile"] = {path="images/menu/icon_agenda@3x.png", width = 36/3, height = 35/3}
appConfig.images.menu["video"] = {path="images/menu/icon_video@3x.png", width = 37/3, height = 25/3}
appConfig.images.menu["photos"] = {path="images/menu/icon_photos@3x.png", width = 36/3, height = 29/3}
appConfig.images.menu["disc"] = {path="images/menu/icon_disc@3x.png", width = 35/3, height = 35/3}
appConfig.images.menu["lyrics"] = {path="images/menu/icon_lyrics@3x.png", width = 36/3, height = 31/3}
appConfig.images.menu["radio"] = {path="images/menu/icon_radio@3x.png", width = 36/3, height = 34/3}
appConfig.images.menu["preview"] = {path="images/menu/icon_preview@3x.png", width = 36/3, height = 34/3}
appConfig.images.menu["direct_from_artist"] = {path="images/menu/icon_direct_from_artist@3x.png", width = 36/3, height = 36/3}
appConfig.images.menu["other_artists"] = {path="images/menu/icon_other_artists@3x.png", width = 36/3, height = 21/3}
appConfig.images.menu["digital_booklet"] = {path="images/menu/icon_digital_booklet@3x.png", width = 32/3, height = 37/3}
appConfig.images.menu["site"] = {path="images/menu/icon_site@3x.png", width = 35/3, height = 35/3}



-- SETTINGS
appConfig.images.settings = {}
appConfig.images.settings["switchBgOff"] = {path="images/settings/switch_bg_off@3x.png", width = 152/3, height = 71/3}
appConfig.images.settings["switchBgOn"] = {path="images/settings/switch_bg_on@3x.png", width = 152/3, height = 71/3}
appConfig.images.settings["switchBgCircle"] = {path="images/settings/swtich_circle@3x.png", width = 71/3, height = 71/3}



-- SCENE LOADING
appConfig.images.loading = {}
--appConfig.images.loading["background"] = {path="images/scene_loading/background.png"}
appConfig.images.loading["logo"] = {path="images/loading/appOficialMK.png", width=84, height=112}



-- SCENE FIRST
appConfig.images.first = {}
appConfig.images.first["iconExclusive"] = {path="images/scene_first/icon_exclusive_singer.png", width=54*1.1, height=32*1.1}
appConfig.images.first["logo"] = {path="images/scene_first/logo_mk.png", width=99/3*1.1, height=126/3*1.1}
appConfig.images.first["arrowLabel"] = {path="images/scene_first/icon_arrow_label_menu@3x.png", width=187/3, height=36/3}
appConfig.images.first["btMenu"] = {path="images/scene_first/bt_menu@3x.png", width=93, height=13}



-- SCENE AGENDA
appConfig.images.agenda = {}
appConfig.images.agenda["background"] = {path="images/agenda/background.png", width=display.contentWidth, height=display.contentHeight}



-- SCENE PROFILE
appConfig.images.profile = {}
appConfig.images.profile["background"] = appConfig.images.agenda["background"]
appConfig.images.profile["awardGold"] = {path="images/profile/award_gold@3x.png", width=74/3, height=74/3}
appConfig.images.profile["awardPlatinum"] = {path="images/profile/award_platinum@3x.png", width=74/3, height=74/3}
appConfig.images.profile["awardPlatinumDouble"] = {path="images/profile/award_platinum_double@3x.png", width=84/3, height=74/3}
appConfig.images.profile["btFacebook"] = {path="images/profile/bt_access_facebook@3x.png", width=131/3, height=37/3}
appConfig.images.profile["btTwitter"] = {path="images/profile/bt_access_twitter@3x.png", width=145/3, height=37/3}



-- SCENE DISCOGRAPHY
appConfig.images.discography = {}
appConfig.images.discography["btPlay"] = {path="images/icons/bt_play@3x.png", width=1.3*49/3, height=1.3*49/3}
appConfig.images.discography["btPlayOver"] = {path="images/icons/bt_play_over@3x.png", width=1.3*49/3, height=1.3*49/3}
appConfig.images.discography["btPause"] = {path="images/icons/bt_pause@3x.png", width=1.3*49/3, height=1.3*49/3}
appConfig.images.discography["btPauseOver"] = {path="images/icons/bt_pause_over@3x.png", width=1.3*49/3, height=1.3*49/3}



-- SCENE PREVIEW
appConfig.images.preview = {}
appConfig.images.preview["play"] = {path="images/icons/icon_play@3x.png", width=46/3, height=46/3}



-- SCENE PREVIEW PLAYER
appConfig.images.previewPlayer = {}
appConfig.images.previewPlayer["background"] = appConfig.images.agenda["background"]
appConfig.images.previewPlayer["play"] = {path="images/player/bt_play@3x.png", width=151/3, height=151/3}
appConfig.images.previewPlayer["pause"] = {path="images/player/bt_pause@3x.png", width=151/3, height=151/3}
appConfig.images.previewPlayer["forward"] = {path="images/player/bt_forward@3x.png", width=99/3, height=99/3}
appConfig.images.previewPlayer["reward"] = {path="images/player/bt_reward@3x.png", width=99/3, height=99/3}
appConfig.images.previewPlayer["forward"] = {path="images/player/bt_forward@3x.png", width=99/3, height=99/3}
appConfig.images.previewPlayer["shuffle"] = {path="images/player/bt_shuffle@3x.png", width=40/3, height=36/3}
appConfig.images.previewPlayer["repeat"] = {path="images/player/bt_repeat@3x.png", width=35/3, height=35/3}



-- SCENE DIRECT FROM ARTIST
appConfig.images.directFromArtist = {}
appConfig.images.directFromArtist["facebook"] = {path="images/direct_from_artist/logo_facebook@3x.png", width=72/3, height=72/3}
appConfig.images.directFromArtist["twitter"] = {path="images/direct_from_artist/logo_twitter@3x.png", width=72/3, height=72/3}
appConfig.images.directFromArtist["instagram"] = {path="images/direct_from_artist/logo_instagram@3x.png", width=72/3, height=72/3}




-- SCENE RADIOS
appConfig.images.radios = {}
appConfig.images.radios["btAppleMusic"] = {path="images/radios/bt_appleMusic.png", width=99, height=39}
appConfig.images.radios["btAppleMusicOver"] = {path="images/radios/bt_appleMusic_over.png", width=99, height=39}
appConfig.images.radios["btDeezer"] = {path="images/radios/bt_deezer.png", width=99, height=39}
appConfig.images.radios["btDeezerOver"] = {path="images/radios/bt_deezer_over.png", width=99, height=39}
appConfig.images.radios["btMusicKey"] = {path="images/radios/bt_musicKey.png", width=99, height=39}
appConfig.images.radios["btMusicKeyOver"] = {path="images/radios/bt_musicKey_over.png", width=99, height=39}
appConfig.images.radios["btRhapsody"] = {path="images/radios/bt_rhapsody.png", width=99, height=39}
appConfig.images.radios["btRhapsodyOver"] = {path="images/radios/bt_rhapsody_over.png", width=99, height=39}
appConfig.images.radios["btSpotify"] = {path="images/radios/bt_spotify.png", width=99, height=39}
appConfig.images.radios["btSpotifyOver"] = {path="images/radios/bt_spotify_over.png", width=99, height=39}
appConfig.images.radios["btRdio"] = {path="images/radios/bt_rdio.png", width=99, height=39}
appConfig.images.radios["btRdioOver"] = {path="images/radios/bt_rdio_over.png", width=99, height=39}


-- SCENE OTHER ARTISTS
appConfig.images.otherArtists = {}
appConfig.images.otherArtists["background"] = appConfig.images.agenda["background"]
appConfig.images.otherArtists["icon_download"] = {path="images/icons/icon_download@3x.png", width=29/3, height=29/3}



-- SCENE NO INTERNET
appConfig.images.noInternet = {}
appConfig.images.noInternet["cloud"] = {path="images/no_internet/cloud@3x.png", width=434/3, height=361/3}
appConfig.images.noInternet["refresh"] = {path="images/no_internet/icon_refresh@3x.png", width=34/3, height=34/3}


return appConfig

local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

local fetchQty = 10
local pageIndex = 0

-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
        

    ------------------------------------------------------------------------------------
    -- LOCAL VARIABLES
    ------------------------------------------------------------------------------------

    local sceneAssets = _G.IMAGES.otherArtists

    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}

    local topBar = require("module-topBar").new(sceneGroup, "Direto do Artista")
    sceneGroup:insert(topBar)
    
    _G.BACKGROUND.fill = {234/255, 236/255, 239/255}



    local margin={left=35/3, right=35/3}

    local frameWidth = SCREEN_W - (35/3)*2



    local groupContent = display.newGroup()
    groupContent.y = 0--120


    local scrollView = nil
    local scrollViewData = {}
    
    
    --------------------------------------------------
    -- REFRESHING FUNTIONS
    --------------------------------------------------


    local groupRefreshing
    local function showScrollViewRefreshing()

       if groupRefreshing then return end  -- if already being shown, don't show again

        groupRefreshing = require("module-frames").newRefreshingGroup()
        sceneGroup:insert(groupRefreshing)
        groupRefreshing.y = scrollView.y - scrollView.contentHeight*.5
        groupRefreshing.x = scrollView.x - scrollView.contentWidth*.5

        return groupRefreshing
            
    end

    local function removeScrollViewRefreshing()
        if groupRefreshing then
            display.remove(groupRefreshing)
            groupRefreshing = nil            
            timer.performWithDelay( 10, function()
                scrollView:scrollTo( "top", { 
                    time=800, 
                    --onComplete=function() print("finshied scrolling to top") end 
                    })
            end)
        end        
    end


    --------------------------------------------------
    -- FOOTER FUNTIONS
    --------------------------------------------------

    local function addScrollViewFooter(hasMoreToLoad)

        local view = scrollView:getView()

        local groupFooter = display.newGroup()
        groupFooter.y = (view.contentHeight < 0 and 0) or view.contentHeight
        scrollView:insert(groupFooter)
        scrollView.groupFooter = groupFooter


        if hasMoreToLoad then

            local btLoadMore = require("library-widgets").newButton{
                    top=14,
                    x = CENTER_X,
                    --width = 160,
                    height = 30,
                    backgroundColor={1,0,0,0},
                    backgroundOverColor={1,0,1,0},
                    label="CARREGAR MAIS", 
                    labelFontSize = 8,
                    labelFont = APP_CONFIG.fontMedium,
                    labelColor = {26/255,118/255,173/255},
                    labelOverColor = {26/255,118/255,173/255,.3},
                    
                    imageFile = APP_CONFIG.images.icons.plus.path,
                    imageWidth = APP_CONFIG.images.icons.plus.width,
                    imageHeight = APP_CONFIG.images.icons.plus.height,
                    imageColor = {26/255,118/255,173/255},
                    imageOverColor = {26/255,118/255,173/255,.3},
                    imagePosition="left",
                    imagePadding = {right = 4},
                    
                    onRelease = function(e) 
                        sceneGroup.loadMoreSpinner = AUX.showSpinner(e.target)
                        e.target.isVisible = false
                        sceneGroup.getContent(false)
                    end, 
            }
            groupFooter:insert(btLoadMore)

        else 
            
            local lbNoMoreToLoad = display.newText{text="NÃO HÁ MAIS POSTS", font=APP_CONFIG.fontMedium, fontSize=8}
            lbNoMoreToLoad.x = CENTER_X
            lbNoMoreToLoad.y = 29
            lbNoMoreToLoad:setTextColor( 26/255,118/255,173/255)
           
            groupFooter:insert(lbNoMoreToLoad)

        end

    end


    local function removeScrollViewFooter()
        display.remove(scrollView.groupFooter)        
    end


    ------------------------------------------------------------------------------------
    -- FUNCTION DEFINITIONS
    ------------------------------------------------------------------------------------


    local function addContentToScrollView(group)

        local view = scrollView:getView()
        
        local groupTop = (view.contentHeight <= 0 and 0) or (view.contentHeight + 14)
        group.x = CENTER_X - group.contentWidth*.5
        group.y = groupTop
        
        scrollView:insert(group)

    end


    




    -- get content to show
    sceneGroup.getContent = function(resetData)

        if resetData then            
            pageIndex = 0
        end

        local function onCallBack(returnData)

            display.remove(sceneGroup.loading)
            sceneGroup.loading = nil
            display.remove(sceneGroup.loadMoreSpinner)

            if resetData then
                display.remove(scrollView)
                sceneGroup.createScrollView()
                scrollViewData = {}
            end

            
            scrollView:setIsLocked( false )
            if returnData.result then
                
                local data = returnData.data
                local lineY = 0

                removeScrollViewRefreshing()

                removeScrollViewFooter()

                for i=1,#data do
                    scrollViewData[#scrollViewData + 1] = data[i]                
                    local frame = require("module-frames").directFromArtistEntry(data[i], scrollView)
                    addContentToScrollView(frame)                
                end
                
                addScrollViewFooter(#data > 0)                
            
                
            end
        end

        --SERVER.getSocialNetworksFeed(resetData, onCallBack)
        pageIndex = pageIndex + 1
        SERVER.getSocialNetworksFeed(fetchQty, pageIndex, onCallBack)
    end


    ------------------------------------------------------------------------------------
    -- RUN CODE
    ------------------------------------------------------------------------------------

    sceneGroup.createScrollView = function()

        -- ScrollView listener
        local function scrollListener( event )

            local phase = event.phase
            if ( phase == "began" ) then print( "Scroll view was touched" )
            --elseif ( phase == "moved" ) then 
            --elseif ( phase == "ended" ) then print( "Scroll view was released" )
            end

            -- In the event a scroll limit is reached...
            if ( event.limitReached ) then
                if ( event.direction == "up" ) then print( "Reached top limit" )
                elseif ( event.direction == "down" ) then print( "Reached bottom limit" )

                    if groupRefreshing or sceneGroup.loading then
                        print("there is already a refreshing in progress")
                        return
                    end
                    
                    local x, y = scrollView:getContentPosition()
                    
                    if y > 50 then
                        local function onScrollComplete()
                            print( "Scroll complete!" )
                            scrollView:setIsLocked( true )
                        end

                        local groupRefreshing = showScrollViewRefreshing()

                        scrollView:scrollToPosition{
                            y = groupRefreshing.contentHeight + 10,
                            time = 800,
                            onComplete = onScrollComplete
                        }

                        
                        timer.performWithDelay( 3000, function() sceneGroup.getContent(true) end )
                    end
                
                end
            end

            return true
        end

        local scrollViewH = SCREEN_H - 120
        local shouldDisableSrollView = false --( groupScrollViewContent.contentHeight < ( SCREEN_H - backgroundWhite.contentBounds.yMin))

          -- creates the scrollview
        scrollView = require("widget").newScrollView{
           x = CENTER_X,
           y = topBar.contentBounds.yMax + scrollViewH*.5,
           width = SCREEN_W,
           height = scrollViewH,    
           hideBackground = true,   
           horizontalScrollDisabled = true,
           verticalScrollDisabled = shouldDisableSrollView,
           bottomPadding = 20,
           topPadding = 10,
           listener = scrollListener,
           hideScrollBar = true,
        }  
        sceneGroup:insert(scrollView)
    

    end




    sceneGroup.createScrollView()
    sceneGroup.getContent()


    sceneGroup.loading = AUX.showLoadingAnimation(CENTER_X, CENTER_Y) 
    sceneGroup:insert(sceneGroup.loading)
    
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).        
        

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

       _G.rbBack.addBack()

       -- alterado por Bruna em 23/10/2015 pois estava marcando a opção errada
       _G.MENU.highlightMenuWithLabel("Direto do Artista")
        
       -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
       local eventFlurry = "view;Direto do Artista;Direto do Artista;"
       print(eventFlurry)
       _G.analytics.logEvent(eventFlurry)
       -- fim
        
    end

end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
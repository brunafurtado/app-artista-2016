local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view

    -- Initialize the scene here.
    -- Example: add display objects to "sceneGroup", add touch listeners, etc.
            

    local sceneAssets = _G.IMAGES.profile

    local profile = require("class-profile")
    
    
    local topBar = require("module-topBar").new(sceneGroup, "Perfil")
    sceneGroup:insert(topBar)
    topBar.background.alpha = 0

    local background = display.loadImage(sceneGroup, sceneAssets.background)
    background.x, background.y = CENTER_X, CENTER_Y
    sceneGroup:insert(topBar) -- bringing to front

    
    local backgroundSocialMediaH = 190 * _G.LAYOUT_RATIO
    local backgroundSocialMediaCenterY = topBar.lbSceneName.y + topBar.lbSceneName.contentHeight + backgroundSocialMediaH * .5 + 6 
    local backgroundSocialMedia = display.newRect(topBar, SCREEN_W*.5, backgroundSocialMediaCenterY ,SCREEN_W,backgroundSocialMediaH)
    backgroundSocialMedia:setFillColor( 0,0,0,.3)
    


    local backgroundWhiteTop = backgroundSocialMedia.contentBounds.yMax
    local backgroundWhiteH = SCREEN_H - backgroundWhiteTop    
    local backgroundWhite = display.newRect(sceneGroup, SCREEN_W*.5,backgroundWhiteTop + backgroundWhiteH*.5, SCREEN_W, backgroundWhiteH)
    backgroundWhite:setFillColor( 1,1,1)





    local function createSocialMediaGroup(socialMediaName)

        local group = display.newGroup()
        group.anchorChildren = true

        local lbTitle = display.newText{parent=group, text=socialMediaName, fontSize=10, font=APP_CONFIG.fontLight}
        lbTitle.anchorY = 0
        lbTitle.x = lbTitle.contentWidth*.5
        lbTitle.y = 0


        local lbCountString = AUX.comma_value(SERVER.getSocialMediaNumber(socialMediaName))
        local lbCount = display.newText{parent=group, text=lbCountString, fontSize=12, font=APP_CONFIG.fontLight}
        lbCount.anchorY = 0
        lbCount.x = lbCount.contentWidth*.5
        lbCount.y = lbTitle.y + lbTitle.contentHeight + 4       

        local imgSocialMedia = display.loadImage(group, sceneAssets["bt"..socialMediaName])
        imgSocialMedia.anchorY = 0
        imgSocialMedia.x = imgSocialMedia.contentWidth*.5
        imgSocialMedia.y = lbCount.y + lbTitle.contentHeight + 8

        -- centering everyone
        local centerX = math.max(lbCount.x,lbTitle.x,imgSocialMedia.x)
        lbTitle.x = centerX
        lbCount.x = centerX        
        imgSocialMedia.x = centerX

        -- adding invisible rectangle above to capture taps
        local invisibleFront = display.newRect(group.contentWidth*.5, group.contentHeight*.5, group.contentWidth, group.contentHeight)
        invisibleFront:setFillColor( 1,1,1,.4 )
        invisibleFront.isHitTestable = true
        invisibleFront.alpha = 0
        group:insert(invisibleFront)
        invisibleFront:addEventListener( "tap", function()

            imgSocialMedia.alpha = 0.7

            local url = "http://www.mkmusic.com.br"

            if socialMediaName == "Twitter" then
                url = "https://www.twitter.com/" .. APP_CONFIG.twitterHandler
            elseif socialMediaName == "Facebook" then
                url = "https://www.facebook.com/" .. APP_CONFIG.facebookPageID
            end
            timer.performWithDelay( 75, function()
                imgSocialMedia.alpha = 1
                system.openURL( url )
            end)
            

        end)


        return group
    end

    
    local lineX = topBar.photoBackground.contentBounds.xMax + (SCREEN_W - topBar.photoBackground.contentBounds.xMax)*.5
    local separatorLine = display.newLine(sceneGroup, lineX, backgroundSocialMedia.y - backgroundSocialMedia.contentHeight*.3, lineX, backgroundSocialMedia.y + backgroundSocialMedia.contentHeight*.3)
    separatorLine.strokeWidth = 1
    separatorLine:setStrokeColor( 64/255, 64/255, 68/255 )


    local fbCenterX = topBar.photoBackground.contentBounds.xMax + (lineX - topBar.photoBackground.contentBounds.xMax)*.5
    local twitterCenterX = lineX + (SCREEN_W - lineX)*.5

    local groupSocialMediaFB = createSocialMediaGroup("Facebook")
    groupSocialMediaFB.y = backgroundSocialMedia.y
    groupSocialMediaFB.x = fbCenterX
    sceneGroup:insert(groupSocialMediaFB)

    local groupSocialMediaTwitter = createSocialMediaGroup("Twitter")
    groupSocialMediaTwitter.y = backgroundSocialMedia.y
    groupSocialMediaTwitter.x = twitterCenterX
    sceneGroup:insert(groupSocialMediaTwitter)




    local groupScrollViewContent = display.newGroup() 

    -- Adicionado por Bruna em 23/10/2015 
    -- nome do app cujo item será compartilhado
    local shareTo = "Compartilhar"
    
    --local shareMessage = "PERFIL " .. PROFILE.name.first .. " " .. PROFILE.name.last .. ": " .. PROFILE.description
    local shareMessage = "Confira o Perfil de " .. PROFILE.name.first .. " " .. PROFILE.name.last .. " no site: " .. APP_CONFIG.artistSite .. "perfil2 ou veja novidades no site "
    
    local btShare = require("custom-widget").newShareButton{
        parent = groupScrollViewContent,
        right = SCREEN_W - 10,
        top = 10,
        shareImageFilename = function() return AUX.getFilenameFromURL(PROFILE.image.url) end,
        shareMessage = shareMessage,
        useBlackColor = true,
        flurry = "outClick;Perfil;Perfil;Compartilhar;" .. shareTo .. ";" .. PROFILE.name.first .. " " .. PROFILE.name.last .. ";",
    }


    local dataAwards = PROFILE.awards

    local totalNumAwards = (dataAwards["gold"] or 0) + (dataAwards["platinum"] or 0) + (dataAwards["doublePlatinum"] or 0)
    local lbNumberOfAwardsFontSize = 72
    if totalNumAwards >= 10 then lbNumberOfAwardsFontSize = 68 end
    if totalNumAwards >= 100 then lbNumberOfAwardsFontSize = 42 end


    local groupAwardsSection = display.newGroup()
    groupScrollViewContent:insert(groupAwardsSection)
    groupAwardsSection.y = btShare.y + btShare.contentHeight*.5 + 10

    local lbNumberOfAwards = display.newText{parent=groupAwardsSection, text=totalNumAwards, x = topBar.photo.x - 10, y=0, font=APP_CONFIG.fontMedium, fontSize=lbNumberOfAwardsFontSize, align="center", width=80}
    lbNumberOfAwards:setFillColor(133/255,155/255,144/255)
    lbNumberOfAwards.y = lbNumberOfAwards.contentHeight*.5


    local lbAwards = display.newText{parent=groupAwardsSection, text="Premiações", x = lbNumberOfAwards.x, y=lbNumberOfAwards.y + lbNumberOfAwards.contentHeight*.5, font=APP_CONFIG.fontMedium, fontSize=16}
    lbAwards.anchorY = 0
    lbAwards:setFillColor(133/255,155/255,144/255)
    

    local lineHorizontalMargin = 16
    lineX = lbAwards.x + lbAwards.contentWidth*.5 + lineHorizontalMargin    
    separatorLine = display.newLine(groupAwardsSection, lineX, lbNumberOfAwards.y - lbNumberOfAwards.contentHeight*.5, lineX, lbAwards.y + lbAwards.contentHeight)
    separatorLine.strokeWidth = 1
    separatorLine:setStrokeColor( 163/255, 163/255, 163/255 )

    local maxAward = math.max(dataAwards["gold"], dataAwards["platinum"], dataAwards["doublePlatinum"])
    local lbNumAwardsWidth = 15
    if maxAward >= 10 then lbNumAwardsWidth = 25 end
    if maxAward >= 20 then  lbNumAwardsWidth = 35 end


    local function createAwardsDiscsGroup(parent, numberOfAwards, awardType)

        local group = display.newGroup()

        local iconObj
        local color = {148/255, 180/255, 196/255} -- (133/255,155/255,144/255)
        if awardType == "Ouro" then
            iconObj = sceneAssets.awardGold
        elseif awardType == "Platina" then
            iconObj = sceneAssets.awardPlatinum
        elseif awardType == "Platina Duplo" then
            iconObj = sceneAssets.awardPlatinumDouble
        end

        local icon = display.loadImage(group, iconObj)
        --icon:scale(74*(320/1080)/icon.contentWidth, 74*(320/1080)/icon.contentWidth)
        --icon.x = icon.contentWidth*.5
        icon.x = sceneAssets.awardPlatinum.width - icon.contentWidth*.5
        icon.y = icon.contentHeight*.5
        icon:setFillColor(unpack(color))

                
        local lbNumAwards = display.newText{parent=group, text=numberOfAwards, x = topBar.photo.x - 4, y=80, font=APP_CONFIG.fontLight, fontSize=18, width=lbNumAwardsWidth, align="center"}
        lbNumAwards.anchorX = 0
        lbNumAwards:setFillColor(unpack(color))
        lbNumAwards.x = icon.x + icon.contentWidth*.5 + 3
        lbNumAwards.y = icon.y


        local lbAwardsString = "Discos de " .. awardType
        local lbAwards = display.newText{parent=group, text=lbAwardsString, x = topBar.photo.x - 4, y=80, font=APP_CONFIG.fontLight, fontSize=12}
        lbAwards.anchorX = 0
        lbAwards:setFillColor(unpack(color))
        lbAwards.x = lbNumAwards.x + lbNumAwards.contentWidth + 3
        lbAwards.y = icon.y
        
        if parent then
            parent:insert(group)
        end

        return group  -- top, left positioned

    end



    local groupAwardsDetails = display.newGroup()
    groupAwardsSection:insert(groupAwardsDetails)
    
    local awardType = "gold"
    if dataAwards[awardType] then
        local award = createAwardsDiscsGroup(groupAwardsDetails, dataAwards[awardType], "Ouro" )
        award.x = lineX + lineHorizontalMargin
        award.y = 0
    end
    
    awardType = "platinum"
    if dataAwards[awardType] then
        local award = createAwardsDiscsGroup(groupAwardsDetails, dataAwards[awardType], "Platina" )
        award.x = lineX + lineHorizontalMargin
        award.y = groupAwardsDetails.contentHeight + 4
    end
    
    awardType = "doublePlatinum"
    if dataAwards[awardType] then
        local award = createAwardsDiscsGroup(groupAwardsDetails, dataAwards[awardType], "Platina Duplo" )
        award.x = lineX + lineHorizontalMargin
        award.y = groupAwardsDetails.contentHeight + 4
    end
    

    -- vertically centering the groupAwardsDetails
    groupAwardsDetails.y = separatorLine.y + separatorLine.contentHeight*.5 - groupAwardsDetails.contentHeight*.5



    local dataProfileDescription = profile.description

    local lbProfileDescription = display.newText2{spaceBetweenLines = 5, x=0, y=0, text=dataProfileDescription, font=APP_CONFIG.fontLight, fontSize=14, width=SCREEN_W*0.9} 
    lbProfileDescription.y = groupAwardsSection.y + groupAwardsSection.contentHeight + 20
    lbProfileDescription.x = CENTER_X - lbProfileDescription.contentWidth*.5
    lbProfileDescription:setTextColor(0.4,0.4,0.4)
    groupScrollViewContent:insert(lbProfileDescription)
    
    local scrollViewY = backgroundWhite.y
    local shouldDisableSrollView = false --( groupScrollViewContent.contentHeight < ( SCREEN_H - backgroundWhite.contentBounds.yMin))

      -- creates the scrollview
    local scrollView = require("widget").newScrollView{
       x = CENTER_X,
       y = scrollViewY,
       width = SCREEN_W,
       height = backgroundWhite.contentHeight,    
       hideBackground = true,   
       hideScrollBar = true,
       horizontalScrollDisabled = true,
       verticalScrollDisabled = shouldDisableSrollView,
       bottomPadding = 60,
    }  
    sceneGroup:insert(scrollView)
    scrollView:insert(groupScrollViewContent)




      

    -- bringing topBar to front
    sceneGroup:insert(topBar.photo)


end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        _G.rbBack.addBack()

        _G.MENU.highlightMenuWithLabel("Perfil")
        
        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry
        local eventFlurry = "view;Perfil;Perfil;"
        print(eventFlurry)
        _G.analytics.logEvent(eventFlurry)
        -- fim

    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------


-- "scene:create()"
function scene:create( event )

    local sceneGroup = self.view


    local sceneAssets = _G.IMAGES.loading

    local groupCenter = display.newGroup()
    groupCenter.anchorChildren = true
    sceneGroup:insert(groupCenter)
    

    -- loading logo
    local logo = display.newImageRect(groupCenter, sceneAssets.logo.path,  sceneAssets.logo.width,  sceneAssets.logo.height)
    logo.x = logo.contentWidth*.5
    logo.y = logo.contentHeight*.5

    -- loading the spinner
    
    sceneGroup.loading = require("rb-aux").showLoadingAnimation(logo.x,0)
    sceneGroup.loading.y = logo.y + logo.contentHeight*.5 + sceneGroup.loading.contentHeight*.5 + 24
    groupCenter:insert(sceneGroup.loading)
    sceneGroup.loading.alpha = 0



    -- positioning the group
    groupCenter.x = CENTER_X
    groupCenter.y = CENTER_Y


   




    
    -------------------------------------------------------------
    -- ENTER THE APP/GAME CUSTOM INFORMATION BELOW

    local nextScene = "scene-first"                      -- the scene that will be loaded after the splashscreen
    local minTimeForSplahscreen = 800                    -- duration of the splashscreen (in ms)

    
    -- flurry (analytics) IDs
    local analyticsApple = APP_CONFIG["flurryAppleKey"]
    local analyticsGoogle = APP_CONFIG["flurryGoogleKey"]
    local analyticsAmazon = APP_CONFIG["flurryAmazonKey"]
    local analyticsWindows = APP_CONFIG["flurryWindowsKey"]
    
    
    --
    -------------------------------------------------------------
    
    local timeBeginLoading = system.getTimer()




    -- variables used by different functions
    local adsAppID
    local analyticsID


    -- require modules and create Global variable on the function below
    local loadingModules = function(onFinish)
        
        _G.AUX =     require("rb-aux")
        _G.rbBack =  require("rb-back")

        require("custom-display")
        require("custom-table")
        require("class-notifications")
        
        _G.SERVER =  require("server")        
        _G.PROFILE = require("class-profile")
        _G.PLAYER =  require("rb-audioPlayer")

        _G.TWITTER = require("module-twitter")
        

        _G.FEATURED_ALBUM = require("class-featuredAlbum")
        _G.FEATURED_ALBUM.download(function()

            _G.MENU  =   require("module-menu").new()

            onFinish()

        end)


        

    end

        
    -- set any device specific configuration (like analyticsID, adsID,..) in the function below
    local loadingDeviceSpecificConfig = function(onFinish)
        
                  
        if _G.DEVICE.isApple then

            analyticsID = analyticsApple
            _G.storeReviewURL = storeReviewURLApple
            
        elseif _G.DEVICE.isGoogle then

            analyticsID = analyticsGoogle
            _G.storeReviewURL = storeReviewURLGoogle

        elseif _G.DEVICE.isKindleFire then
            analyticsID = analyticsAmazon
            _G.storeReviewURL = storeReviewURLAmazon            

        else
            -- any other DEVICE (windows, html,..) will end up here
            
            
            
        end

        onFinish()
                        
    end
    
    
    
    
    -- load the analytics provider below
    local loadingAnalytics = function(onFinish)
        
        -- initialize analytics
        if not _G.DEVICE.isSimulator and analyticsID then
            _G.analytics = require( "analytics" )
            --analytics:setCurrentProvider("flurry")
            _G.analytics.init( analyticsID )
        -- incluido por Bruna em 19/10/2015 para ativar o logEvent no flurry    
        else
            _G.analytics = {}
            _G.analytics.logEvent = function() end
        -- fim
        end
        

        onFinish()
        
    end
    
    -- load all sounds on the function below
    local loadingSounds = function(onFinish)


        onFinish()
                    
    end

    

    -- pre-load any scene on the function below
    local loadingScenes = function(onFinish)

        -- pre-loading next scene
        composer.loadScene( nextScene, false )

        onFinish()        
    end
        
    


    
    
    local functionsToRun = {}
    
    -- comment or uncomment the functions below if you are using them
    functionsToRun[#functionsToRun+1] = loadingModules    
    --functionsToRun[#functionsToRun+1] = loadingSounds
    functionsToRun[#functionsToRun+1] = loadingDeviceSpecificConfig
    functionsToRun[#functionsToRun+1] = loadingAnalytics
    --functionsToRun[#functionsToRun+1] = loadingAds
    functionsToRun[#functionsToRun+1] = loadingScenes






    local loadFunction
    
    -- this loads all the functions above, making a delay between the loads to avoid any freeze for the app
    loadFunction = function(number)
                
        --print("loadFunction entered - number", number)
        if number > #functionsToRun then


            local timeToWaitBeforeGoingToNextScreenEndLoading = minTimeForSplahscreen - (system.getTimer() - timeBeginLoading )
                        
            if timeToWaitBeforeGoingToNextScreenEndLoading <= 0 then
                                                                        
                if _G.CANCEL_LOADING ~= true then   
                    composer.gotoScene( nextScene, { effect="slideLeft", time=400} )
                end

            else
               timer.performWithDelay(timeToWaitBeforeGoingToNextScreenEndLoading, function() loadFunction(number) end) 
            end
            
        else
            
            --print("calling Function ", number)
            functionsToRun[number](function()
                --print("finshed Function ", number)            
                timer.performWithDelay(10, function() loadFunction(number+1) end)
            end)
            
            

        end
        
    end
    sceneGroup.loadFunction = loadFunction
    
    
    
end


-- "scene:show()"
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then    
        -- Called when the scene is still off screen (but is about to come on screen).        

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.

        sceneGroup.loading.alpha = 1

        timer.performWithDelay(100, function() sceneGroup.loadFunction(1) end)
    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
        display.setStatusBar( display.TranslucentStatusBar )
        --_G.statusBarBackground.isVisible = true        

    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
local news = {}


	news.new = function(params)

		local obj = {}

		obj.date = params.date
		obj.image = params.image  -- image={url=, width, height=}
		obj.title = params.title
		obj.summary = params.summary
		obj.content = params.content
		obj.webURL = params.webURL
		

		return obj

	end


return news
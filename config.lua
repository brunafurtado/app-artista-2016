----------------------------------------------------------------------------------
-- Este arquivo NÃO deve ser modificado (a não ser que saiba o que está fazendo).
-- Para customizar o app para outro artista, modifique o arquivo appConfig.lua
----------------------------------------------------------------------------------


application = {
    launchPad = false,  -- disabling corona analytics
	content = {

		scale = "adaptive",
		fps = 60,
				
        imageSuffix = {
		    ["@2x"] = 1.5,
            ["@3x"] = 2.5,
		}
		
	},

    
    -- Push notifications

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
      
}
